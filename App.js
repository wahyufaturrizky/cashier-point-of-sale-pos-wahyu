import React, {Component} from 'react';
import {Text, View} from 'react-native';
import ScreenContainer from './src/ScreenContainer';
import Login from './src/ScreenContainer/View/Login';

export default class App extends Component {
  render() {
    return <ScreenContainer />;
  }
}

console.disableYellowBox = true;
