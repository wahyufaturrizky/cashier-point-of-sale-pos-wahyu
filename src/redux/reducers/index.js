import {combineReducers} from 'redux';
import foodAllMenuReducer from './transaction/foodAllMenuReducer';
import foodCategoryReducer from './transaction/foodCategoryReducer';
import rightFoodTransactionReducer from './transaction/rightFoodTransactionReducer';

export default combineReducers({
  foodAllMenu: foodAllMenuReducer,
  foodCategory: foodCategoryReducer,
  rightFoodTransaction: rightFoodTransactionReducer,
});
