export default (state = [], action) => {
  switch (action.type) {
    case 'GET_ALL_FOOD_MENU':
      return action.payload;
    default:
      return state;
  }
};
