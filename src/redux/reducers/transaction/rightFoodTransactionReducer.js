const initialStaterightFoodTransaction = {
  allTransactionMenu: null,
  subTotal: null,
  amountDiscount: null,
  dataUangTunai: null,
  testRedux: 'testRedux',
  tokenUser: '',
  all_data_id_order_printing: '',
  all_data_id_category: '',
  dataAllCustomer: '',
  all_data_discount: {},
};

export default (state = initialStaterightFoodTransaction, action) => {
  console.log('All Data Reducer = ', state);
  switch (action.type) {
    case 'ALL_TRANSACTION':
      console.log('action.payload allTransactionMenu = ', action.payload);
      return {
        ...state,
        allTransactionMenu: action.payload,
      };
    case 'ALL_SUB_TOTAL_TRANSACTION':
      return {
        ...state,
        subTotal: action.payload,
      };
    case 'ALL_SUB_TOTAL_DISCOUNT':
      return {
        ...state,
        amountDiscount: action.payload,
      };
    case 'DATA_TOKEN_USER':
      return {
        ...state,
        tokenUser: action.payload,
      };
    case 'DATA_ALL_CUSTOMER':
      console.log('action.payload dataAllCustomer = ', action.payload);
      return {
        ...state,
        dataAllCustomer: action.payload,
      };
    case 'DATA_ALL_UANG_TUNAI':
      console.log('action.payload dataUangTunai = ', action.payload);
      return {
        ...state,
        dataUangTunai: action.payload,
      };
    case 'DATA_ALL_DATA_DISCOUNT':
      console.log('action.payload all_data_discount = ', action.payload);
      return {
        ...state,
        all_data_discount: action.payload,
      };
    case 'ALL_DATA_ORDER_ID_PRINTING':
      console.log(
        'action.payload all_data_id_order_printing = ',
        action.payload,
      );
      return {
        ...state,
        all_data_id_order_printing: action.payload,
      };
    case 'ALL_DATA_ID_CATEGORY':
      console.log('action.payload all_data_id_category = ', action.payload);
      return {
        ...state,
        all_data_id_category: action.payload,
      };
    default:
      return state;
  }
};
