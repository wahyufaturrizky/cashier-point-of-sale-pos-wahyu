export default (state = [], action) => {
  switch (action.type) {
    case 'GET_FOOD_CATEGORY_BEVERAGE':
      return action.payload;
    case 'GET_FOOD_CATEGORY_MEALS':
      return action.payload;
    case 'GET_FOOD_CATEGORY_SNACK':
      return action.payload;
    default:
      return state;
  }
};
