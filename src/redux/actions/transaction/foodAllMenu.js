import Baresto from '../../../api/api';

export const getAllFoodMenu = () => async (dispatch) => {
  const response = await Baresto.get('/food_menu').catch((error) =>
    console.log('ini error', error),
  );
  // console.log('ini response All Menu', response);
  dispatch({
    type: 'GET_ALL_FOOD_MENU',
    payload: response.data.data.data,
  });
};
