import Baresto from '../../../api/api';

export const getAllFoodMenu = () => async (dispatch) => {
  const response = await Baresto.get('/food_menu');
  dispatch({type: 'GET_FOOD_MENU', payload: response.data});
};
export const getFoodCategoryBeverage = () => async (dispatch) => {
  const response = await Baresto.get('/food_menu/beverage');
  dispatch({type: 'GET_FOOD_CATEGORY_BEVERAGE', payload: response.data});
};
export const getFoodCategoryMeals = () => async (dispatch) => {
  const response = await Baresto.get('/food_category/meals');
  dispatch({type: 'GET_FOOD_CATEGORY_MEALS', payload: response.data});
};
export const getFoodCategorySnack = () => async (dispatch) => {
  const response = await Baresto.get('/food_category/snack');
  dispatch({type: 'GET_FOOD_CATEGORY_SNACK', payload: response.data});
};
