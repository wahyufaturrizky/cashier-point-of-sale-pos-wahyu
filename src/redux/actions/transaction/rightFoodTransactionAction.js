export const getAllDataTransaction = (dataAllTransaction) => (dispatch) => {
  return dispatch({type: 'ALL_TRANSACTION', payload: dataAllTransaction});
};
export const getAllDatasubTotal = (dataAllSubtotal) => (dispatch) => {
  console.log('dataAllSubtotal Action = ', dataAllSubtotal);
  return dispatch({
    type: 'ALL_SUB_TOTAL_TRANSACTION',
    payload: dataAllSubtotal,
  });
};
export const getAllDataDiscount = (dataAllDiscount) => (dispatch) => {
  return dispatch({type: 'ALL_SUB_TOTAL_DISCOUNT', payload: dataAllDiscount});
};
export const getAllTokenUser = (dataTokenUser) => (dispatch) => {
  console.log('dataTokenUser = ', dataTokenUser);
  return dispatch({type: 'DATA_TOKEN_USER', payload: dataTokenUser});
};
export const getAllDataCustomer = (getDataAllCustomer) => (dispatch) => {
  console.log('getDataAllCustomer Action = ', getDataAllCustomer);
  return dispatch({type: 'DATA_ALL_CUSTOMER', payload: getDataAllCustomer});
};
export const getAllDataUangTunai = (dataAllUangTunai) => (dispatch) => {
  console.log('dataAllUangTunai Action = ', dataAllUangTunai);
  return dispatch({type: 'DATA_ALL_UANG_TUNAI', payload: dataAllUangTunai});
};
export const getAllDataGlobalDiscount = (allDataDiscount) => (dispatch) => {
  console.log('allDataDiscount Action = ', allDataDiscount);
  return dispatch({type: 'DATA_ALL_DATA_DISCOUNT', payload: allDataDiscount});
};
export const getAllDataOrderIdPrinting = (allDataOrderIdPrinting) => (
  dispatch,
) => {
  console.log('allDataOrderIdPrinting Action = ', allDataOrderIdPrinting);
  return dispatch({
    type: 'ALL_DATA_ORDER_ID_PRINTING',
    payload: allDataOrderIdPrinting,
  });
};
export const getAllCategoryMenuId = (allDataIdCategory) => (dispatch) => {
  console.log('allDataIdCategory Action = ', allDataIdCategory);
  return dispatch({type: 'ALL_DATA_ID_CATEGORY', payload: allDataIdCategory});
};
