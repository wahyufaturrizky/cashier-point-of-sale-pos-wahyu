import axios from 'axios';

export default axios.create({
  baseURL: 'http://apimobile-devel.baresto.id/api',
  headers: {
    'Content-type': 'application/json',
  },
});
