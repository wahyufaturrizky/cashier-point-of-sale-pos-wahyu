import http from '../http-common';

class RunningOrderService {
  createRunningOrderData(
    order_type,
    floor_id,
    customer_id,
    table_id,
    food_menu,
    authtoken,
  ) {
    // console.log("RunningOrderService data = ", order_type, floor_id, table_id, food_menu, authtoken)
    return http.post(
      '/running_order/creation',
      {
        order_type: order_type,
        table_id: table_id,
        floor_id: floor_id,
        customer_id: customer_id,
        food_menu: food_menu,
      },
      {
        headers: {
          Authorization: `Bearer ${authtoken}`,
        },
      },
    );
  }
}

export default new RunningOrderService();
