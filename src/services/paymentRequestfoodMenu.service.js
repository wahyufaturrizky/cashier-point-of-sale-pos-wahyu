import http from '../http-common';
import {compose} from 'redux';

class PaymentGatewayRequest {
  getAllPaymentGatewayRequest() {
    return http.get('/payment_request/Hj25Da1rU9WJOYuJN3O4yMfG9Qgu8Z');
  }
}

export default new PaymentGatewayRequest();
