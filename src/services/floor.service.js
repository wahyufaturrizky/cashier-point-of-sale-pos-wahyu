import http from '../http-common';

class FloorService {
  getFloor(authtoken) {
    console.log('authtoken Floor = ', authtoken);
    return http.get('/floor', {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }
}

export default new FloorService();
