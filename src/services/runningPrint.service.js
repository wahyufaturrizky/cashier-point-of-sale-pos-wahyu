import http from '../http-common';

class RunningPrintService {
  createPrintKot(order_id, authtoken) {
    console.log('order_id = ', order_id, 'dan authtoken = ', authtoken);
    return http.get(`/print/kot/${order_id}`, {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }

  createPrintingInvoiceDuaDone(order_id, authtoken) {
    console.log('order_id, authtoken Done = ', order_id, authtoken);
    return http.get(`/print/invoice2/${order_id}`, {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }

  createPrintingInvoicePay(order_id, authtoken) {
    console.log('order_id, authtoken Pay = ', order_id, authtoken);
    return http.get(`/print/invoice/${order_id}`, {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }

  createPrintBill(hold_id, authtoken, discount_id) {
    console.log('order_id = ', hold_id, 'dan authtoken = ', authtoken);
    return http.post(
      '/sales/calculate',
      {
        hold_id: hold_id,
        discount_id: discount_id,
      },
      {
        headers: {
          Authorization: `Bearer ${authtoken}`,
        },
      },
    );
  }
}

export default new RunningPrintService();
