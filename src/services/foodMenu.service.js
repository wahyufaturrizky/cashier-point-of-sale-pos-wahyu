import http from '../http-common';
import {compose} from 'redux';

class FoodMenuService {
  getAll(authtoken) {
    console.log('userToken all food menu = ', authtoken);
    return http.get('/food_menu', {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }
  getFoodMenuSearch(dataSearchString, authtoken) {
    console.log('searchFoodMenu masuk ga = ', dataSearchString);
    return http.get(`/food_menu?search=${dataSearchString}`, {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }
  getCategory(authtoken) {
    console.log('authtoken getCategory masuk ga = ', authtoken);
    return http.get('/food_category', {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }
  getItemModal(uuidCategoryMenu, authtoken) {
    console.log(
      'cek nih uuidCategoryMenu ID betul ga = ',
      uuidCategoryMenu,
      'sama token nya = ',
      authtoken,
    );
    return http.get(`/food_category/${uuidCategoryMenu}`, {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }
  getDiscount(authtoken) {
    return http.get('./discount', {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }

  get(uuid, authtoken) {
    console.log('get(uuid, authtoken) = ', uuid, 'dan token = ', authtoken);
    return http.get(`food_menu/${uuid}`, {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }
}

export default new FoodMenuService();
