import http from '../http-common';

class TableService {
  getTable(authtoken) {
    console.log('authtoken Table = ', authtoken);
    return http.get('/table', {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }

  getTableByFloor(authtoken, idFloor) {
    console.log('authtoken Table = ', authtoken, 'dan uuid fllor = ', idFloor);
    return http.get(`/floor/${idFloor}`, {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }
}

export default new TableService();
