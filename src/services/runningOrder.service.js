import http from '../http-common';

class runningOderService {
  getAllRunningOderData(authtoken) {
    // console.log("authtoken RunningOderData = ", authtoken)
    return http.get('/running_order', {
      // headers: {
      //   Authorization: `Bearer ${authtoken}`,
      // },
    });
  }
  getRunningOderDataById(uuidRunningOrder, authtoken) {
    console.log(
      'uuidRunningOrder nyampe ga = ',
      uuidRunningOrder,
      'dan tokennya = ',
      authtoken,
    );
    return http.get(`/running_order/show/${uuidRunningOrder}`, {
      headers: {
        Authorization: `Bearer ${authtoken}`,
      },
    });
  }
}

export default new runningOderService();
