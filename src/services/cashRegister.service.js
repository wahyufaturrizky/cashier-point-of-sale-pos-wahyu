import http from '../http-common';

class CashRegisterService {
  getCashRegister(amount, authtoken) {
    console.log('authtoken service =', authtoken);
    return http.post(
      '/sales',
      {
        amount: amount,
      },
      {
        headers: {
          Authorization: `Bearer ${authtoken}`,
        },
      },
    );
  }
}
export default new CashRegisterService();
