import http from '../http-common';

class LoginService {
  getLogin(email, password) {
    return http.post('/login', {
      email: email,
      password: password,
    });
  }
}
export default new LoginService();
