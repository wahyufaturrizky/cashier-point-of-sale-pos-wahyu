import http from '../http-common';

class CustomerService {
  createCustomer(name, phone, email, address, authtoken) {
    console.log(
      'createCustomer data = ',
      name,
      phone,
      email,
      address,
      authtoken,
    );
    return http.post(
      '/customer',
      {
        name: name,
        phone: phone,
        email: email,
        address: address,
      },
      {
        headers: {
          Authorization: `Bearer ${authtoken}`,
        },
      },
    );
  }
}

export default new CustomerService();
