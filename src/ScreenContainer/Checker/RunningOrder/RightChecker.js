import React, {Component} from 'react';
import {Text, View, Image, ScrollView, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import OrderList from './OrderList';

export default class RightChecker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: '10:00',
    };
  }
  render() {
    return (
      <View>
        <View
          style={{
            height: '11%',
            backgroundColor: '#FFFFFF',
            marginBottom: '1.25%',
            justifyContent: 'center',
            paddingHorizontal: 23,
          }}>
          <View
            style={{
              height: '90%',
              borderRadius: 4,
              borderWidth: 0.5,
              borderColor: '#C5C5C5',
              paddingHorizontal: '3.14%',
              paddingVertical: 12,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View>
              <Text style={{fontSize: RFValue(7), color: '#878787'}}>
                KITCHEN STATUS
              </Text>
              {this.props.itemSelected == undefined ? (
                <Text style={{fontSize: RFValue(13), color: '#878787'}}>
                  NO ORDER SELECTED
                </Text>
              ) : (
                <Text style={{fontSize: RFValue(13), color: '#878787'}}>
                  No. 101 - TABLE 5
                </Text>
              )}
            </View>
            {this.props.itemSelected == undefined ? (
              <View
                style={{
                  width: '23.5%',
                  height: undefined,
                  aspectRatio: 3,
                  borderRadius: 3.5,
                  backgroundColor: '#878787',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  paddingHorizontal: '2%',
                }}>
                <Image
                  source={require('../../../assets/General/RunningOrder/clock.png')}
                  resizeMode="contain"
                  style={{width: undefined, height: '60%', aspectRatio: 1}}
                />
                <Text style={{fontSize: RFValue(10), color: '#FFFFFF'}}>
                  00:00
                </Text>
              </View>
            ) : (
              <View
                style={{
                  width: '23.5%',
                  height: undefined,
                  aspectRatio: 3,
                  borderRadius: 3.5,
                  backgroundColor: '#FEBF11',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  paddingHorizontal: '2%',
                }}>
                <Image
                  source={require('../../../assets/General/RunningOrder/clock.png')}
                  resizeMode="contain"
                  style={{width: undefined, height: '60%', aspectRatio: 1}}
                />
                <Text style={{fontSize: RFValue(10), color: '#FFFFFF'}}>
                  {this.state.time}
                </Text>
              </View>
            )}
          </View>
        </View>
        <View
          style={{
            height: '87.75%',
            backgroundColor: 'white',
            justifyContent: 'space-around',
            alignItems: 'center',
            paddingBottom: '3.4%',
            paddingHorizontal: 23,
          }}>
          <View
            style={{height: '85%', backgroundColor: 'white', width: '100%'}}>
            {this.props.itemSelected == undefined ? (
              <View
                style={{
                  height: '100%',
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text>No Transaction</Text>
              </View>
            ) : (
              <ScrollView>
                <OrderList />
              </ScrollView>
            )}
          </View>
          <View
            style={{height: '10%', width: '100%', justifyContent: 'center'}}>
            <TouchableOpacity
              style={{
                backgroundColor: '#FEBF11',
                width: '100%',
                height: undefined,
                aspectRatio: 8,
                borderRadius: RFValue(2),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: 'white', fontSize: 24}}>SERVE</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
