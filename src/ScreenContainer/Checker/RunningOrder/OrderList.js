import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import OrderItem from './OrderItem';

export default class OrderList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: false,
    };
  }
  render() {
    return (
      <View style={{width: '100%', alignItems: 'center', padding: RFValue(5)}}>
        <View
          style={{
            width: '100%',
            height: undefined,
            aspectRatio: 9,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{
              height: '90%',
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
              backgroundColor: 'white',
              borderWidth: 2,
              borderColor: '#DADADA',
            }}
            onPress={() => this.setState({selected: !this.state.selected})}>
            <View
              style={{
                height: '100%',
                width: undefined,
                aspectRatio: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View style={{height: '50%', width: '50%'}}>
                {this.state.selected ? (
                  <Image
                    source={require('../../../assets/General/checkTrue.png')}
                    style={{height: '100%', width: '100%', aspectRatio: 1}}
                    resizeMode="contain"
                  />
                ) : (
                  <Image
                    source={require('../../../assets/General/checkFalse.png')}
                    style={{height: '100%', width: '100%', aspectRatio: 1}}
                    resizeMode="contain"
                  />
                )}
              </View>
            </View>
            <View
              style={{
                height: '100%',
                width: undefined,
                flexDirection: 'row',
                alignItems: 'center',
                aspectRatio: 10,
              }}>
              <Text style={{fontSize: RFValue(8)}}>Select All</Text>
            </View>
          </TouchableOpacity>
        </View>
        <OrderItem
          item="Nasi Goreng"
          quantity={2}
          selected={this.state.selected}
        />
        <OrderItem
          item="Mie Goreng"
          quantity={1}
          selected={this.state.selected}
        />
      </View>
    );
  }
}
