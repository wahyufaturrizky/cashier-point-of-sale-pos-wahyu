import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class RunningOrderList extends Component {
  render() {
    return (
      <View
        style={{
          width: '100%',
          height: undefined,
          aspectRatio: 8,
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          style={{
            width: '100%',
            height: '75%',
            backgroundColor: 'white',
            borderColor: '#DADADA',
            borderWidth: 2,
            paddingHorizontal: '5%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
          onPress={() => this.props.selectItem(2)}>
          {this.props.paidStatus ? (
            <Text style={{fontSize: RFValue(11), color: '#33C15D'}}>PAID</Text>
          ) : (
            <Text style={{fontSize: RFValue(11), color: '#D0021B'}}>
              UNPAID
            </Text>
          )}
          <Text style={{fontSize: RFValue(10), color: '#6A6A6A'}}>
            {this.props.table}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
