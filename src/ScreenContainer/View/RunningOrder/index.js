/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import CashierNavigator from '../../Cashier/CashierNavigator';
import LeftCashier from '../../Cashier/RunningOrder/LeftCashier';
import RightCashier from '../../Cashier/RunningOrder/RightCashier';
import LeftChecker from '../../Checker/RunningOrder/LeftChecker';
import RightChecker from '../../Checker/RunningOrder/RightChecker';
import runningOderService from '../../../services/runningOrder.service';
import {connect} from 'react-redux';

class RunningOrder extends Component {
  constructor(props) {
    super(props);
    this.retrieveRunningDataById = this.retrieveRunningDataById.bind(this);
    this.state = {
      itemSelected: '',
      role: 'cashier',
      testCoba: 'testCoba',
      listAllDataHotDetail: [],
    };
  }

  retrieveRunningDataById() {
    console.log('this.state.itemSelected = ', this.state.itemSelected);
    runningOderService
      .getRunningOderDataById(this.state.itemSelected, this.props.userToken)
      .then((response) => {
        console.log('response getRunningOderDataById = ', response);
        this.setState({
          listAllDataHotDetail: response.data.data.hold_detail,
        });
      })
      .catch((e) => {
        console.log('Error Cek Semua data running by Id = ', e);
      });
  }

  rightComponent = (role) => {
    switch (role) {
      case 'cashier':
        return (
          <RightCashier
            testCoba={this.state.testCoba}
            itemSelected={this.state.itemSelected}
            listAllDataHotDetail={this.state.listAllDataHotDetail}
            navigation={this.props.navigation}
          />
        );
      case 'checker':
        return <RightChecker />;
    }
  };
  leftComponent = (role) => {
    switch (role) {
      case 'cashier':
        return <LeftCashier selectItem={this.selectItem} />;
      case 'checker':
        return <LeftChecker selectItem={this.selectItem} />;
    }
  };

  selectItem = (uuidRunningOrder) => {
    this.setState({itemSelected: uuidRunningOrder}, () =>
      this.retrieveRunningDataById(),
    );

    // this.child.current.retrieveRunningDataById(uuidRunningOrder)
  };

  render() {
    return (
      <View style={{flexDirection: 'row', height: '100%'}}>
        <View style={{flex: 85}}>
          {this.state.role === 'cashier' ? (
            <CashierNavigator
              runningOrderStatus={true}
              navigation={this.props.navigation}
            />
          ) : (
            <CashierNavigator
              runningOrderStatus={true}
              navigation={this.props.navigation}
            />
          )}
        </View>
        <View
          style={{
            flex: 642,
            backgroundColor: '#F7F7F7',
            borderWidth: 0.5,
            paddingHorizontal: '1%',
          }}>
          <ScrollView
            style={{width: '100%', height: '90%', marginVertical: '2%'}}>
            {this.leftComponent(this.state.role)}
          </ScrollView>
        </View>
        <View style={{flex: 555, backgroundColor: '#F7F7F7'}}>
          {this.rightComponent(this.state.role)}
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userToken: state.rightFoodTransaction.tokenUser,
    testRedux: state.rightFoodTransaction.testRedux,
  };
};

export default connect(mapStateToProps)(RunningOrder);
