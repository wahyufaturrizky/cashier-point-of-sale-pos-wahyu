/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class ModalFooter extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'flex-end',
          paddingHorizontal: '2%',
          backgroundColor: 'white',
        }}>
        {this.props.close ? (
          <TouchableOpacity
            onPress={this.props.close}
            style={{
              width: '50%',
              height: 80,
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                backgroundColor: '#6F6F6F',
                borderRadius: RFValue(5),
                marginRight: 12,
                marginBottom: 8,
                shadowColor: '#DF2428',
                shadowOffset: {width: 0, height: 2},
                shadowOpacity: 100,
                shadowRadius: 2.62,
                elevation: 3,
              }}>
              <View
                style={{
                  backgroundColor: '#6F6F6F',
                  paddingHorizontal: 10,
                  paddingVertical: 2,
                  justifyContent: 'center',
                  marginBottom: 6,
                  marginRight: 6,
                  borderRadius: 10,
                  flex: 1,
                }}>
                <Text
                  style={{
                    fontSize: RFValue(15),
                    textAlign: 'center',
                    color: '#FFFFFF',
                  }}>
                  {this.props.leftText}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        ) : (
          <View
            style={{
              width: undefined,
              height: '100%',
              aspectRatio: 1.6,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          />
        )}
        <Text style={{fontSize: RFValue(18), alignSelf: 'center'}}>
          {this.props.title}
        </Text>
        {this.props.save ? (
          <TouchableOpacity
            onPress={this.props.save}
            style={{
              width: '50%',
              height: 80,
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                backgroundColor: '#FEBF11',
                borderRadius: RFValue(5),
                marginLeft: 12,
                marginBottom: 8,
                shadowColor: '#DF2428',
                shadowOffset: {width: 0, height: 2},
                shadowOpacity: 100,
                shadowRadius: 2.62,
                elevation: 3,
              }}>
              <View
                style={{
                  backgroundColor: '#FEBF11',
                  paddingHorizontal: 10,
                  paddingVertical: 2,
                  justifyContent: 'center',
                  marginBottom: 6,
                  marginRight: 6,
                  borderRadius: 10,
                  flex: 1,
                }}>
                <Text
                  style={{
                    fontSize: RFValue(15),
                    textAlign: 'center',
                    color: '#FFFFFF',
                  }}>
                  {this.props.rightText}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        ) : (
          <View
            style={{
              width: undefined,
              height: '100%',
              aspectRatio: 1.6,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          />
        )}
      </View>
    );
  }
}
