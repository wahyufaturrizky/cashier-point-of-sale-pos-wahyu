/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Modal, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class DeleteModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
    };
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
  }
  render() {
    return (
      <Modal visible={this.state.visible} transparent={true}>
        <View
          style={{
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#000000aa',
          }}>
          <View
            style={{
              height: '30%',
              width: '30%',
              backgroundColor: 'white',
              alignItems: 'center',
              borderRadius: RFValue(2),
            }}>
            <View style={{height: '90%', width: '100%'}}>
              <View
                style={{
                  height: '100%',
                  width: '100%',
                  alignItems: 'center',
                  marginVertical: RFValue(15),
                }}>
                <View
                  style={{
                    height: '15%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={{fontSize: RFValue(15)}}>Delete Order ?</Text>
                </View>
                <View
                  style={{
                    height: '60%',
                    width: '75%',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                  }}>
                  {/* Options */}
                  <TouchableOpacity
                    style={{
                      width: '49.5%',
                      height: '50%',
                      backgroundColor: '#878787',
                      borderRadius: 6.6,
                      flexDirection: 'row',
                      paddingHorizontal: '3%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onPress={this.props.deleteModalSwitch}>
                    <Text style={{color: 'white', fontSize: RFValue(8)}}>
                      Cancel
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: '49.5%',
                      height: '50%',
                      backgroundColor: '#FEBF11',
                      borderRadius: 6.6,
                      flexDirection: 'row',
                      paddingHorizontal: '3%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onPress={this.props.deleteModalSwitch}>
                    <Text style={{color: 'white', fontSize: RFValue(8)}}>
                      Delete
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
