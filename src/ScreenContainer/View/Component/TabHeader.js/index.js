/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class TabHeader extends Component {
  render() {
    return (
      <View
        style={{
          flexDirection: 'row',
          flex: 1,
          borderBottomWidth: 0.5,
          paddingHorizontal: '2%',
          borderColor: '#C5C5C5',
          justifyContent: 'space-between',
        }}>
        <TouchableOpacity
          style={{
            width: '9%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around',
          }}
          onPress={() => this.props.navigation.goBack()}>
          <Image
            source={require('../../../../assets/General/back.png')}
            resizeMode="contain"
            style={{width: undefined, height: '75%', aspectRatio: 1}}
          />
          <Text style={{fontSize: RFValue(10)}}>Back</Text>
        </TouchableOpacity>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={{fontSize: RFValue(18)}}>{this.props.title}</Text>
        </View>
        <View style={{width: '9%'}} />
      </View>
    );
  }
}
