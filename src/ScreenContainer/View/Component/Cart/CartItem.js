/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import NumberFormat from 'react-number-format';

export default class CartItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enabled: false,
    };
  }
  render() {
    return (
      <View style={{width: '100%', height: undefined, aspectRatio: 10}}>
        {this.props.isRunningOrder ? (
          <TouchableOpacity
            onPress={() => this.setState({enabled: !this.state.enabled})}
            style={{height: '90%', width: '100%'}}>
            {this.state.enabled ? (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  borderColor: '#FD734C',
                  borderRadius: RFValue(1),
                  borderWidth: 2,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <View
                      style={{
                        height: '100%',
                        width: undefined,
                        aspectRatio: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text style={{fontSize: RFValue(9), color: '#6A6A6A'}}>
                        {this.props.quantity}
                      </Text>
                    </View>
                    <View style={{justifyContent: 'center'}}>
                      <Text style={{fontSize: RFValue(9), color: '#6A6A6A'}}>
                        {this.props.foodName}
                      </Text>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    height: '100%',
                    width: undefined,
                    aspectRatio: 4,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                    paddingRight: '2%',
                  }}>
                  {this.props.isRunningOrder ? (
                    <Text style={{fontSize: RFValue(9), color: '#6A6A6A'}}>
                      {this.props.cookStatus}
                    </Text>
                  ) : (
                    <NumberFormat
                      value={this.props.price}
                      displayType={'text'}
                      thousandSeparator={true}
                      prefix={'Rp'}
                      renderText={(value) => (
                        <Text style={{fontSize: RFValue(9), color: '#6A6A6A'}}>
                          {value}
                        </Text>
                      )}
                    />
                  )}
                </View>
              </View>
            ) : (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  borderColor: '#EAEAEA',
                  borderRadius: RFValue(1),
                  borderWidth: 2,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <View
                      style={{
                        height: '100%',
                        width: undefined,
                        aspectRatio: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text style={{fontSize: RFValue(9), color: '#6A6A6A'}}>
                        {this.props.quantity}
                      </Text>
                    </View>
                    <View style={{justifyContent: 'center'}}>
                      <Text style={{fontSize: RFValue(9), color: '#6A6A6A'}}>
                        {this.props.foodName}
                      </Text>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    height: '100%',
                    width: undefined,
                    aspectRatio: 4,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                    paddingRight: '2%',
                  }}>
                  {this.props.isRunningOrder ? (
                    <Text style={{fontSize: RFValue(9), color: '#6A6A6A'}}>
                      {this.props.cookStatus}
                    </Text>
                  ) : (
                    <NumberFormat
                      value={this.props.price}
                      displayType={'text'}
                      thousandSeparator={true}
                      prefix={'Rp'}
                      renderText={(value) => (
                        <Text style={{fontSize: RFValue(9), color: '#6A6A6A'}}>
                          {value}
                        </Text>
                      )}
                    />
                  )}
                </View>
              </View>
            )}
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={{
              height: '90%',
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              borderWidth: 2,
              borderColor: '#EAEAEA',
              borderRadius: RFValue(1),
            }}
            onPress={this.props.press}
            onLongPress={this.props.longPress}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    height: '100%',
                    width: undefined,
                    aspectRatio: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{fontSize: RFValue(9), color: '#6A6A6A'}}>
                    {this.props.quantity}
                  </Text>
                </View>
                {this.props.modifier == null &&
                this.props.discountItem == null ? (
                  <View style={{justifyContent: 'center'}}>
                    <Text style={{fontSize: RFValue(9), color: '#6A6A6A'}}>
                      {this.props.foodName}
                    </Text>
                  </View>
                ) : (
                  <View style={{justifyContent: 'center'}}>
                    <Text style={{fontSize: RFValue(9), color: '#6A6A6A'}}>
                      {this.props.foodName}
                    </Text>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={{fontSize: RFValue(7), color: '#CBCBCB'}}>
                        {this.props.modifier}
                      </Text>
                      <Text style={{fontSize: RFValue(7), color: '#CBCBCB'}}>
                        {this.props.discountItem}
                      </Text>
                    </View>
                  </View>
                )}
              </View>
            </View>
            <View
              style={{
                height: '100%',
                width: undefined,
                aspectRatio: 4,
                justifyContent: 'center',
                alignItems: 'flex-end',
                paddingRight: '2%',
              }}>
              <NumberFormat
                value={this.props.price}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp'}
                renderText={(value) => (
                  <Text style={{fontSize: RFValue(9), color: '#6A6A6A'}}>
                    {value}
                  </Text>
                )}
              />
            </View>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}
