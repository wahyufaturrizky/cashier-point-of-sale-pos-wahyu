/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View} from 'react-native';
import CartItem from './CartItem';
import AddItemModal from '../../../Cashier/Transaction/AddItemModal';
import DeleteModal from '../DeleteModal';
import {RFValue} from 'react-native-responsive-fontsize';

export default class Cart extends Component {
  constructor(props) {
    super(props);

    this.setActiveFoodMenu = this.setActiveFoodMenu.bind(this);

    this.state = {
      addItemModalVisible: false,
      deleteModalVisible: false,
      cartItem: [],
      currentItem: null,
      currentIndex: -1,
    };
  }

  setActiveFoodMenu(foodMenu, index) {
    this.setState({
      currentItem: foodMenu,
      currentIndex: index,
    });
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
  }
  addItemModalSwitch = (foodName) => {
    this.state.addItemModalVisible
      ? this.setState({addItemModalVisible: false, foodItem: null})
      : this.setState({addItemModalVisible: true, foodItem: foodName});
  };
  deleteModalSwitch = () => {
    this.state.deleteModalVisible
      ? this.setState({deleteModalVisible: false})
      : this.setState({deleteModalVisible: true});
  };
  render() {
    const {currentItem} = this.state;
    return (
      <View style={{alignItems: 'center'}}>
        {currentItem ? (
          <CartItem
            quantity={currentItem.qty}
            foodName={currentItem.menu_name}
            price={currentItem.sale_price}
            // modifier="Telur"
            // discountItem="(Discount Pelajar 20%)"
            // price={30000}
            // isRunningOrder={this.props.isRunningOrder}
            // cookStatus="Done Cooking"
            // isPayment={this.props.isPayment}
            // press={this.addItemModalSwitch}
            // longPress={this.deleteModalSwitch}
          />
        ) : (
          <Text>No Transaction</Text>
        )}

        <AddItemModal
          visible={this.state.addItemModalVisible}
          addItemModalSwitch={this.addItemModalSwitch}
        />
        <DeleteModal
          visible={this.state.deleteModalVisible}
          deleteModalSwitch={this.deleteModalSwitch}
        />
      </View>
    );
  }
}
