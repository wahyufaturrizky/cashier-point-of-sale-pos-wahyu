import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import NumberFormat from 'react-number-format';
import {RFValue} from 'react-native-responsive-fontsize';

export default class InvoiceList extends Component {
  render() {
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          height: RFValue(22),
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingHorizontal: 28,
          borderTopWidth: 1,
          borderBottomWidth: 1,
          borderColor: '#DADADA',
        }}
        onPress={this.props.deleteMenuModalSwitch}>
        <Text
          style={{color: '#CBCBCB', fontWeight: 'bold', fontSize: RFValue(7)}}>
          {this.props.quantity}
        </Text>
        <Text
          style={{color: '#CBCBCB', fontWeight: 'bold', fontSize: RFValue(7)}}>
          {this.props.title}
        </Text>
        <NumberFormat
          value={this.props.amount}
          displayType={'text'}
          thousandSeparator={true}
          prefix={'Rp. '}
          renderText={(value) => (
            <Text style={{color: '#6A6A6A', fontSize: RFValue(7)}}>
              {value}
            </Text>
          )}
        />
      </TouchableOpacity>
    );
  }
}
