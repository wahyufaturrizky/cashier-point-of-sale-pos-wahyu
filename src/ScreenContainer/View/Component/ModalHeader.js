/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class ModalHeader extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'flex-end',
          paddingHorizontal: '2%',
          backgroundColor: 'white',
        }}>
        {this.props.close ? (
          <TouchableOpacity
            onPress={this.props.close}
            style={{
              width: undefined,
              height: '100%',
              aspectRatio: 1.6,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Image
              source={require('../../../assets/General/back.png')}
              resizeMode="contain"
              style={{width: undefined, height: '75%', aspectRatio: 1}}
            />
            <Text style={{fontSize: RFValue(12)}}>Back</Text>
          </TouchableOpacity>
        ) : (
          <View
            style={{
              width: undefined,
              height: '100%',
              aspectRatio: 1.6,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          />
        )}
        <Text style={{fontSize: RFValue(18), alignSelf: 'center'}}>
          {this.props.title}
        </Text>
        {this.props.save ? (
          <TouchableOpacity
            onPress={this.props.save}
            style={{
              width: undefined,
              height: '100%',
              aspectRatio: 1.6,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: RFValue(12)}}>Save</Text>
            <Image
              source={require('../../../assets/General/correct.png')}
              resizeMode="contain"
              style={{width: undefined, height: '75%', aspectRatio: 1}}
            />
          </TouchableOpacity>
        ) : (
          <View
            style={{
              width: undefined,
              height: '100%',
              aspectRatio: 1.6,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          />
        )}
      </View>
    );
  }
}
