/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import LoginService from '../../services/login.service';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getAllTokenUser} from '../../redux/actions/transaction/rightFoodTransactionAction';

class Login extends Component {
  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.state = {
      role: 'cashier',
      //register: '',
      email: '',
      password: '',
      loginStatus: '',
      auth_token: '',
    };
  }

  componentDidMount() {
    this.login();
  }

  login() {
    LoginService.getLogin(this.state.email, this.state.password)
      .then((response) => {
        this.setState(
          {
            loginStatus: response.data.message,
            auth_token: response.data.data.access_token,
          },
          () => this.props.getAllTokenUser(response.data.data.access_token),
        );
        console.log('response token pas lo masuk = ', this.state.auth_token);

        if (this.state.loginStatus === 'LoginSuccess') {
          this.roleNavigate(this.state.role);
        }
      })
      .catch((e) => {
        console.log('error', e);
      });
  }
  onChangeEmail(text) {
    const email = text;
    console.log('email', email);
    this.setState({
      email: email,
    });
  }
  onChangePassword(text) {
    const password = text;
    console.log('password', password);
    this.setState({
      password: password,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
            paddingHorizontal: RFValue(150),
          }}>
          <View style={{flex: 5}} />
          <View style={{flex: 2, alignItems: 'center', width: '100%'}}>
            <Text style={{fontSize: 20}}>LOG IN USER</Text>
          </View>

          <View
            style={{
              flex: 2,
              shadowColor: '#707070',
              borderBottomColor: '#FEBF11',
              borderBottomWidth: 1,
              justifyContent: 'center',
              height: 49.48,
              width: '100%',
            }}>
            <TextInput
              placeholder="Email Address"
              style={{
                fontSize: 12,
                paddingVertical: 0,
                color: 'black',
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
                alignSelf: 'center',
              }}
              placeholderTextColor={'#C6CED9'}
              onChangeText={this.onChangeEmail}
              keyboardType="email-address"
            />
          </View>
          <View
            style={{
              flex: 2,
              shadowColor: '#707070',
              borderBottomColor: '#FEBF11',
              borderBottomWidth: 1,
              justifyContent: 'center',
              height: 49.48,
              width: '100%',
            }}>
            <TextInput
              placeholder="Password"
              style={{
                fontSize: 12,
                paddingVertical: 0,
                color: 'black',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              placeholderTextColor={'#C6CED9'}
              onChangeText={this.onChangePassword}
              secureTextEntry={true}
            />
          </View>
          <TouchableOpacity
            style={{
              width: '100%',
              height: undefined,
              aspectRatio: 11,
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              marginTop: RFValue(10),
              borderRadius: RFValue(3),
              backgroundColor: '#FEBF11',
            }}
            onPress={this.login}>
            <Text style={{color: 'white'}}>START</Text>
          </TouchableOpacity>
          <View style={{flex: 5}} />
        </View>
      </View>
    );
  }
  roleNavigate = (role) => {
    switch (role) {
      case 'cashier':
        this.props.navigation.navigate('CashierStack');
        break;
      case 'checker':
        this.props.navigation.navigate('CheckerStack');
        break;
      case 'kitchen':
        this.props.navigation.navigate('KitchenStack');
        break;
      case 'waiter':
        this.props.navigation.navigate('WaiterBottomTab');
        break;
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  button: {
    backgroundColor: '#1c313a',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
  },
  imglogo: {
    width: 300,
    height: 100,
    alignItems: 'center',
    backgroundColor: '#EEEEEE',
    justifyContent: 'center',
    marginBottom: 15,
  },
});

const mapStateToProps = (state) => {
  return {
    testRedux: state.rightFoodTransaction.testRedux,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({getAllTokenUser}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
