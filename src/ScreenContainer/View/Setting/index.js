/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import CashierNavigator from '../../Cashier/CashierNavigator';
import LogOutModal from './LogOutModal';
// import KitchenCheckerNavigator from "../../../components/molecules/KitchenCheckerNavigator"

import {RFValue} from 'react-native-responsive-fontsize';

export default class Setting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role: 'cashier',
      logOutModalVisible: false,
      islogOut: false,
    };
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          flexDirection: 'row',
          backgroundColor: '#F7F7F7',
        }}>
        <View style={{flex: 85}}>
          {this.state.role == 'cashier' ? (
            <CashierNavigator
              settingStatus={true}
              navigation={this.props.navigation}
            />
          ) : (
            {
              /*<KitchenCheckerNavigator
              settingStatus={true}
              navigation={this.props.navigation}
            />*/
            }
          )}
        </View>

        <View style={{flex: 1195}}>
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              paddingVertical: 25,
              backgroundColor: 'white',
              borderBottomWidth: 0.5,
              borderColor: '#C5C5C5',
            }}>
            <Text style={{fontSize: RFValue(18)}}>SETTINGS</Text>
          </View>
          <View style={{}}>
            <View
              style={{
                height: '15%',
                width: '100%',
                marginVertical: '.1%',
                backgroundColor: 'white',
                marginBottom: 0.5,
                alignItems: 'center',
                borderBottomColor: '#C5C5C5',
                borderBottomWidth: 0.5,
              }}>
              <TouchableOpacity
                style={{
                  height: '100%',
                  width: '100%',
                  paddingHorizontal: 100,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
                onPress={() => this.props.navigation.navigate('Profile')}>
                <Text style={{fontSize: RFValue(15)}}>Edit Profile</Text>
                <Image
                  source={require('../../../assets/General/arrow.png')}
                  style={{
                    width: undefined,
                    height: '25%',
                    aspectRatio: 0.8,
                    transform: [{rotate: '180deg'}],
                  }}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: '15%',
                width: '100%',
                marginVertical: '.1%',
                backgroundColor: 'white',
                marginBottom: 0.5,
                alignItems: 'center',
                borderBottomColor: '#C5C5C5',
                borderBottomWidth: 0.5,
              }}>
              <TouchableOpacity
                style={{
                  height: '100%',
                  width: '100%',
                  paddingHorizontal: 100,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
                onPress={() =>
                  this.props.navigation.navigate('TransactionHistory')
                }>
                <Text style={{fontSize: RFValue(15)}}>Transaction History</Text>
                <Image
                  source={require('../../../assets/General/arrow.png')}
                  style={{
                    width: undefined,
                    height: '25%',
                    aspectRatio: 0.8,
                    transform: [{rotate: '180deg'}],
                  }}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: '15%',
                width: '100%',
                marginVertical: '.1%',
                backgroundColor: 'white',
                marginBottom: 0.5,
                alignItems: 'center',
                borderBottomColor: '#C5C5C5',
                borderBottomWidth: 0.5,
              }}>
              <TouchableOpacity
                style={{
                  height: '100%',
                  width: '100%',
                  paddingHorizontal: 100,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
                onPress={() => this.props.navigation.navigate('ClosingCash')}>
                <Text style={{fontSize: RFValue(15)}}>
                  Closing Cash Register
                </Text>
                <Image
                  source={require('../../../assets/General/arrow.png')}
                  style={{
                    width: undefined,
                    height: '25%',
                    aspectRatio: 0.8,
                    transform: [{rotate: '180deg'}],
                  }}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: '15%',
                width: '100%',
                marginVertical: '.1%',
                backgroundColor: 'white',
                marginBottom: 0.5,
                alignItems: 'center',
                borderBottomColor: '#C5C5C5',
                borderBottomWidth: 0.5,
              }}>
              <TouchableOpacity
                style={{
                  height: '100%',
                  width: '100%',
                  paddingHorizontal: 100,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
                onPress={() => this.props.navigation.navigate('ShiftHistory')}>
                <Text style={{fontSize: RFValue(15)}}>Shifting History</Text>
                <Image
                  source={require('../../../assets/General/arrow.png')}
                  style={{
                    width: undefined,
                    height: '25%',
                    aspectRatio: 0.8,
                    transform: [{rotate: '180deg'}],
                  }}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: '15%',
                width: '100%',
                marginVertical: '.1%',
                backgroundColor: 'white',
                marginBottom: 0.5,
                alignItems: 'center',
                borderBottomColor: '#C5C5C5',
                borderBottomWidth: 0.5,
              }}>
              <TouchableOpacity
                style={{
                  height: '100%',
                  width: '100%',
                  paddingHorizontal: 100,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
                onPress={() => this.props.navigation.navigate('Printer')}>
                <Text style={{fontSize: RFValue(15)}}>Printer Setting</Text>
                <Image
                  source={require('../../../assets/General/arrow.png')}
                  style={{
                    width: undefined,
                    height: '25%',
                    aspectRatio: 0.8,
                    transform: [{rotate: '180deg'}],
                  }}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: '15%',
                width: '100%',
                marginVertical: '.1%',
                backgroundColor: 'white',
                marginBottom: 0.5,
                alignItems: 'center',
                borderBottomColor: '#C5C5C5',
                borderBottomWidth: 0.5,
              }}>
              <TouchableOpacity
                style={{
                  height: '100%',
                  width: '100%',
                  paddingHorizontal: 100,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
                onPress={this.logOutModalSwitch}>
                <Text style={{fontSize: RFValue(15), color: 'red'}}>
                  Log Out
                </Text>
                <Image
                  source={require('../../../assets/General/arrow.png')}
                  style={{
                    width: undefined,
                    height: '25%',
                    aspectRatio: 0.8,
                    transform: [{rotate: '180deg'}],
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <LogOutModal
          visible={this.state.logOutModalVisible}
          logOutModalSwitch={this.logOutModalSwitch}
          togglelogOut={this.togglelogOut}
          closeAndlogOut={this.closeAndlogOut}
        />
      </View>
    );
  }

  logOutModalSwitch = () => {
    this.state.logOutModalVisible
      ? this.setState({logOutModalVisible: false})
      : this.setState({logOutModalVisible: true});
  };

  togglelogOut = () => {
    this.state.islogOut
      ? this.setState({islogOut: false})
      : this.setState({islogOut: true});
  };

  closeAndlogOut = () => {
    this.props.navigation.navigate('Login');
  };
}
