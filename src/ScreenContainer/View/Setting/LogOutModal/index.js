/* eslint-disable radix */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {
  Text,
  View,
  Modal,
  TextInput,
  ScrollView,
  StyleSheet,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import ModalFooter from '../../../View/Component/ModalFooter';
import ModalHeader from '../../../View/Component/ModalHeader';
import TableService from '../../../../services/table.service';
// import DineInModal from '../DineInModal';

export default class LogOutModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
  }

  render() {
    const {tables} = this.state;
    return (
      <View>
        <Modal visible={this.state.visible} transparent={true}>
          <View
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#000000aa',
            }}>
            <View
              style={{
                height: '50%',
                width: '50%',
                backgroundColor: 'white',
                alignItems: 'center',
                borderRadius: RFValue(2),
                paddingVertical: '10%',
              }}>
              <View
                style={{
                  height: '10%',
                  width: '100%',
                  shadowOpacity: 1,
                  backgroundColor: 'white',
                  elevation: 1,
                }}>
                <ModalHeader
                  title="LOG OUT?"
                  // close={this.props.logOutModalSwitch}
                  // save={this.props.closeAndlogOut}
                />
              </View>
              <ModalFooter
                // title="LOG OUT?"
                rightText="Keep Going"
                leftText="Yes"
                close={this.props.closeAndlogOut}
                save={this.props.logOutModalSwitch}
              />
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputStyle: {
    height: RFValue(35),
    width: '100%',
    marginTop: RFValue(10),
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderColor: '#C6CED9',
    borderRadius: 3,
    paddingHorizontal: '2%',
    paddingVertical: 0,
    fontSize: RFValue(9),
  },
});
