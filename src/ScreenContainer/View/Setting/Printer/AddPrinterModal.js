import React, {Component} from 'react';
import {
  Text,
  View,
  Modal,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class AddPrinterModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
      quantity: 0,
      takeaway: false,
      modalType: 'main',
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
  }
  render() {
    return (
      <Modal visible={this.state.visible} transparent={true}>
        <View
          style={{
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#000000aa',
          }}>
          {this.chooseModal()}
        </View>
      </Modal>
    );
  }
  chooseModal = () => {
    switch (this.state.modalType) {
      case 'main':
        return (
          <View
            style={{
              height: '40%',
              width: '80%',
              backgroundColor: 'white',
              alignItems: 'center',
              borderRadius: RFValue(2),
            }}>
            <View
              style={{
                height: '25%',
                width: '100%',
                borderBottomWidth: 0.5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: RFValue(12)}}>Select Type</Text>
            </View>
            <TouchableOpacity
              style={{
                height: '25%',
                width: '100%',
                flexDirection: 'row',
                borderBottomWidth: 0.5,
                paddingHorizontal: RFValue(30),
                alignItems: 'center',
              }}>
              <Image />
              <Text style={{fontSize: RFValue(12)}}>Wi-Fi / LAN</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                height: '25%',
                width: '100%',
                flexDirection: 'row',
                borderBottomWidth: 0.5,
                paddingHorizontal: RFValue(30),
                alignItems: 'center',
              }}>
              <Image />
              <Text style={{fontSize: RFValue(12)}}>Bluetooth</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                height: '25%',
                width: '100%',
                flexDirection: 'row',
                paddingHorizontal: RFValue(30),
                alignItems: 'center',
              }}
              onPress={() => this.setState({modalType: 'input IP address'})}>
              <Image />
              <Text style={{fontSize: RFValue(12)}}>IP Address</Text>
            </TouchableOpacity>
          </View>
        );
      case 'input IP address':
        return (
          <View
            style={{
              height: RFValue(60),
              width: '80%',
              backgroundColor: 'white',
              alignItems: 'center',
              borderRadius: RFValue(2),
            }}>
            <View
              style={{
                height: '50%',
                width: '100%',
                borderBottomWidth: 0.5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: RFValue(12)}}>Select Type</Text>
            </View>
            <View
              style={{
                height: '50%',
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
              }}>
              <TextInput
                style={{
                  height: '80%',
                  width: '75%',
                  borderRadius: RFValue(2),
                  borderWidth: 2,
                  paddingHorizontal: RFValue(20),
                  paddingVertical: 0,
                  borderColor: '#DADADA',
                }}
                onChangeText={(value) =>
                  this.props.handleChange(value, 'ip_address')
                }
                placeholder="IP Address"
              />
              <TouchableOpacity
                style={{
                  height: '80%',
                  width: '20%',
                  backgroundColor: '#FEBF11',
                  borderRadius: RFValue(2),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => this.setState({modalType: 'loading'})}>
                <Text style={{fontSize: RFValue(9), color: 'white'}}>
                  Input
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        );
      case 'loading':
        return (
          <View
            style={{
              height: '60%',
              width: undefined,
              aspectRatio: 1.5,
              backgroundColor: 'white',
              alignItems: 'center',
              borderRadius: RFValue(2),
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: RFValue(12)}}>Searching for printer…</Text>
            {this.props.printerConnectedSwitch()}
            {this.setState({modalType: 'main'})}
          </View>
        );
    }
  };
}
