import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {ScrollView} from 'react-native-gesture-handler';

export default class AvailablePrinter extends Component {
  render() {
    return (
      <ScrollView>
        <View
          style={{
            height: RFValue(30),
            widht: '100%',
            justifyContent: 'center',
            alignItems: 'flex-end',
          }}>
          <TouchableOpacity
            style={{
              height: '100%',
              width: undefined,
              aspectRatio: 3,
              borderRadius: RFValue(2),
              backgroundColor: '#FEBF11',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={this.props.addPrinterModalSwitch}>
            <Text style={{fontSize: RFValue(9), color: 'white'}}>
              Add Printer
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{width: '100%', flexWrap: 'wrap', flexDirection: 'row'}}>
          {/* Connected Printer */}
          <TouchableOpacity
            style={{
              height: undefined,
              width: '20%',
              marginHorizontal: '2%',
              marginBottom: RFValue(10),
              aspectRatio: 0.8,
              backgroundColor: 'white',
              justifyContent: 'space-around',
              alignItems: 'center',
              borderRadius: RFValue(5),
              shadowColor: '#C5C5C5',
              elevation: 2,
            }}>
            <View
              style={{
                width: '100%',
                height: undefined,
                aspectRatio: 1,
                marginTop: '5%',
                borderBottomWidth: 0.5,
                borderColor: '#C5C5C5',
                justifyContent: 'space-around',
                alignItems: 'center',
              }}
              onPress={this.props.testPrint}>
              <Image
                source={require('../../../../assets/General/Setting/print.png')}
                resizeMode="contain"
                style={{width: undefined, height: '40%', aspectRatio: 1}}
              />
              <View style={{alignItems: 'center'}}>
                <Text style={{fontSize: RFValue(10)}}>Printer Name</Text>
                <Text style={{fontSize: RFValue(9)}}>IP Address</Text>
              </View>
            </View>
            <Text style={{fontSize: RFValue(9), color: '#FEBF11'}}>
              Test Print
            </Text>
          </TouchableOpacity>
          {/* Disconnected Printer */}
          <TouchableOpacity
            style={{
              height: undefined,
              width: '20%',
              marginHorizontal: '2%',
              marginBottom: RFValue(10),
              aspectRatio: 0.8,
              backgroundColor: 'white',
              justifyContent: 'space-around',
              alignItems: 'center',
              borderRadius: RFValue(5),
              shadowColor: '#C5C5C5',
              elevation: 2,
            }}>
            <View
              style={{
                width: '100%',
                height: undefined,
                aspectRatio: 1,
                marginTop: '5%',
                borderBottomWidth: 0.5,
                borderColor: '#C5C5C5',
                justifyContent: 'space-around',
                alignItems: 'center',
              }}>
              <Image
                source={require('../../../../assets/General/Setting/printGrey.png')}
                resizeMode="contain"
                style={{width: undefined, height: '40%', aspectRatio: 1}}
              />
              <View style={{alignItems: 'center'}}>
                <Text style={{fontSize: RFValue(10), color: '#C5C5C5'}}>
                  Printer Name
                </Text>
                <Text style={{fontSize: RFValue(9), color: '#C5C5C5'}}>
                  IP Address
                </Text>
              </View>
            </View>
            <Text style={{fontSize: RFValue(9)}}>Connect</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
