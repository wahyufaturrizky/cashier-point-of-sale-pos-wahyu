/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput, ScrollView} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import KitchenNavigator from '../KitchenNavigator';
import Header from './Header/Header';
import OrderList from './OrderListMenu/OrderListMenu';
import OrderDetailsBody from './OrderDetails/OrderDetailsBody';

export default class KitchenRunningOrder extends Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        {/*<OrderDetailsBody />
          <KitchenNavigator
            navigation={this.props.navigation}
            RunningOrderStatus={true}
          />
        */}
        <View>
          <KitchenNavigator
            navigation={this.props.navigation}
            RunningOrderStatus={true}
          />
        </View>
        <View>
          <Header />
          <View style={styles.view} />
          <ScrollView
            style={{backgroundColor: '#ffffff', height: '20%'}}
            horizontal>
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
            <OrderList noOrder="No. 107" noTable="Table 7" />
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexWrap: 'wrap',
  },
  searchWrap: {
    width: '100%',
    height: '10%',
    backgroundColor: '#FFFFFF',
    shadowOpacity: 0.4,
    shadowRadius: 3,
    elevation: 3,
  },
  search: {
    backgroundColor: '#F2F4F4',
    width: '60%',
    height: 45,
    marginTop: '1%',
    marginBottom: '0.1%',
    marginLeft: '5%',
    paddingLeft: '5%',
    borderRadius: 5,
  },
  view: {
    backgroundColor: '#D5D8DC',
    height: '70%',
  },
});
