import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class Header extends Component {
  render() {
    return (
      <View style={styles.searchWrap}>
        <TextInput placeholder="search" style={styles.search} />
        <TouchableOpacity style={styles.searchButtonStyle}>
          <Text>icon search</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.notifStyle}>
          <Image
            source={require('../../../../assets/Waiter/BottomTabIcon/ActiveNotification.png')}
            style={styles.imageStyle}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  searchWrap: {
    flexDirection: 'row',
    width: '100%',
    height: '10%',
    backgroundColor: '#FFFFFF',
    shadowOpacity: 0.4,
    shadowRadius: 3,
    elevation: 3,
  },
  search: {
    backgroundColor: '#F2F4F4',
    width: '60%',
    height: RFValue(25),
    marginTop: '1%',
    marginBottom: '0.1%',
    marginLeft: '5%',
    paddingLeft: '5%',
    borderRadius: 5,
  },
  searchButtonStyle: {
    backgroundColor: '#F2F4F4',
    width: RFValue(35),
    height: RFValue(27),
    marginTop: '1%',
    marginBottom: '0.1%',
    marginLeft: RFValue(5),
    borderTopRightRadius: RFValue(3),
    borderBottomRightRadius: RFValue(3),
  },
  notifStyle: {
    //backgroundColor: '#F2F4F4',
    width: RFValue(35),
    height: RFValue(27),
    marginTop: '1%',
    marginBottom: '0.1%',
    marginLeft: RFValue(35),
    borderTopRightRadius: RFValue(3),
    borderBottomRightRadius: RFValue(3),
  },
  imageStyle: {
    width: RFValue(30),
    height: RFValue(30),
  },
});
