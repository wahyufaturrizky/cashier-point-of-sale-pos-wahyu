import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class OrderDetailsOptions extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.imageContainer}>
          <View style={styles.imageWrapper}>
            <Image
              source={require('../../../../assets/Kitchen/BUTTON_COOK.png')}
              style={styles.imageStyle}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.imageContainer}>
          <View style={styles.imageWrapper}>
            <Image
              source={require('../../../../assets/Kitchen/Button_Done.png')}
              style={styles.imageStyle}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.imageContainer}>
          <View style={styles.imageWrapper}>
            <Image
              source={require('../../../../assets/Kitchen/Button_Serve.png')}
              style={styles.imageStyle}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.imageContainer}>
          <View style={styles.imageWrapper2}>
            <Image
              source={require('../../../../assets/Kitchen/Icon_Waste_Copy.png')}
              style={styles.imageStyle}
            />
            <Text style={styles.textStyle}>Waste</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginLeft: RFValue(30),
    marginRight: RFValue(41),
  },
  imageContainer: {
    backgroundColor: '#ffffff',
    width: RFValue(100),
    height: RFValue(60),
    alignItems: 'center',
    borderRadius: RFValue(3),
    borderColor: '#C5C5C5',
    elevation: RFValue(1.5),
    marginLeft: RFValue(10),
    marginRight: RFValue(10),
  },
  imageWrapper: {
    marginTop: RFValue(8),
  },
  imageWrapper2: {
    flexDirection: 'row',
    marginTop: RFValue(8),
  },
  imageStyle: {
    width: RFValue(45),
    height: RFValue(45),
  },
  textStyle: {
    //marginTop: RFValue(10),
    marginLeft: RFValue(3),
    alignSelf: 'center',
  },
});
