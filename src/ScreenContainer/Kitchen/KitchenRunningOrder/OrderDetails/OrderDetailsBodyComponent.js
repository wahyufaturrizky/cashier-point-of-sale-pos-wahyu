import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class OrderDetailsBodyComponent extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.numberWrapper}>
          <Text style={styles.textNumberStyle}>1</Text>
        </View>
        <View style={styles.orderWrapper}>
          <Text style={styles.textOrder1Style}>Nasi Goreng</Text>
          <Text style={styles.textOrder2Style}>Telur</Text>
        </View>
        <View style={styles.statusWrapper}>
          <Text style={styles.textStatusStyle}>Not Ready</Text>
        </View>
        <View style={styles.timeWrapper}>
          <View>
            <Image
              source={require('../../../../assets/Kitchen/Icon_Clock.png')}
              style={styles.imageStyle}
            />
          </View>
          <Text style={styles.textTimeStyle}>00:00</Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#C5C5C5',
    borderWidth: RFValue(0.5),
    borderRadius: RFValue(3),
    backgroundColor: '#ffffff',
    marginLeft: RFValue(41),
    marginRight: RFValue(41),
  },
  numberWrapper: {
    //backgroundColor: 'red',
    marginLeft: RFValue(20),
  },
  orderWrapper: {
    //backgroundColor: 'yellow',
    marginLeft: RFValue(20),
  },
  statusWrapper: {
    //backgroundColor: 'green',
    marginLeft: RFValue(340),
  },
  timeWrapper: {
    flexDirection: 'row',
    marginLeft: RFValue(10),
    //backgroundColor: 'blue',
    alignItems: 'center',
  },
  imageStyle: {
    width: RFValue(20),
    height: RFValue(20),
  },
  textNumberStyle: {
    fontSize: RFValue(15),
  },
  textOrder1Style: {
    fontSize: RFValue(13),
  },
  textOrder2Style: {
    fontSize: RFValue(8),
  },
  textStatusStyle: {
    fontSize: RFValue(13),
  },
  textTimeStyle: {
    fontSize: RFValue(10),
    marginLeft: RFValue(5),
  },
});
