import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class OderDetailsHeader extends Component {
  render() {
    return (
      <View>
        <View style={styles.container}>
          <View style={styles.statusStyle}>
            <Text style={styles.text1Style}>KITCHEN STATUS</Text>
            <Text style={styles.text2Style}>No. 101 - TABLE 5</Text>
          </View>
          <View style={styles.timeWraperContainer}>
            <View>
              <Image
                source={require('../../../../assets/General/RunningOrder/clock.png')}
                style={styles.imageStyle}
              />
            </View>
            <View style={styles.timeTextWrapper}>
              <Text style={styles.timeTextStyle}>00:00</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    //flex: 1,
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    borderColor: '#C5C5C5',
    borderWidth: RFValue(0.4),
    borderRadius: RFValue(3),
    height: RFValue(45),
    marginLeft: RFValue(41),
    marginRight: RFValue(41),
  },
  statusStyle: {
    marginLeft: RFValue(5),
    marginVertical: RFValue(4),
  },
  text1Style: {
    color: '#C5C5C5',
    fontSize: RFValue(10),
  },
  text2Style: {
    color: '#C5C5C5',
    fontSize: RFValue(15),
  },
  timeWraperContainer: {
    backgroundColor: '#C5C5C5',
    //flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginVertical: RFValue(8),
    marginRight: RFValue(5),
    marginLeft: RFValue(400),
    width: RFValue(50),
    borderRadius: RFValue(3),
  },
  imageStyle: {
    width: RFValue(12),
    height: RFValue(12),
    //alignSelf: 'flex-end',
  },
  timeTextWrapper: {
    marginLeft: RFValue(3),
  },
  timeTextStyle: {
    color: '#ffffff',
    fontSize: RFValue(12),
  },
});
