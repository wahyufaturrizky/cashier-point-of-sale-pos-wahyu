import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, ScrollView} from 'react-native';
import {RFValue, RFPercentage} from 'react-native-responsive-fontsize';

export default class DataOrder extends Component {
  render() {
    return (
      <View style={styles.dataContainer}>
        <View style={styles.imageWrapper}>
          <Image
            source={require('../../../../assets/Waiter/BottomTabIcon/ActiveMenu.png')}
            resizeMode="contain"
            style={styles.imageStyle}
          />
        </View>
        <View style={styles.textWrapper}>
          <Text style={styles.text1}>{this.props.order}</Text>
          <Text style={styles.text2}>{this.props.itemOrder}</Text>
        </View>
        <View style={styles.timeWrapper}>
          <Text style={styles.timeText}>{this.props.time}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dataContainer: {
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    width: '100%',
    height: RFValue(50),
    marginTop: RFValue(13),
    paddingHorizontal: RFValue(16),
  },
  imageWrapper: {
    marginTop: RFValue(5),
    // marginBottom: RFValue(10),
    //backgroundColor: 'red',
  },
  imageStyle: {
    height: RFValue(40),
    width: RFValue(50),
    //paddingBottom: RFValue(10),
  },
  dateWrapper: {
    marginTop: RFValue(10),
  },
  textWrapper: {
    paddingVertical: RFValue(14),
    paddingLeft: RFValue(20),
  },
  text1: {
    fontSize: RFValue(10),
  },
  text2: {
    color: '#BFC9CA',
  },
  timeWrapper: {
    paddingTop: RFValue(21),
    //justifyContent: 'flex-end',
    //flexDirection: 'row',
    //backgroundColor: 'red',
    //alignSelf: 'center',
    flex: 1,
  },
  timeText: {
    alignSelf: 'flex-end',
  },
});
