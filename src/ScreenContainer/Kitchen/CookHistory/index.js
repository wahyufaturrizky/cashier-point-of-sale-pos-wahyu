import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, ScrollView} from 'react-native';
import {RFValue, RFPercentage} from 'react-native-responsive-fontsize';

import LeftCookHistory from './leftCookHistory/leftCookHistory';
import RightCookHistory from './rightCookHistory/body';
import KitchenNavigator from '../KitchenNavigator';

export default class CookHistory extends Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.navigatorContainer}>
          <KitchenNavigator
            navigation={this.props.navigation}
            CookHistoryStatus={true}
          />
        </View>
        <View style={styles.leftCookContainer}>
          <ScrollView>
            <LeftCookHistory />
          </ScrollView>
        </View>
        <View style={styles.rightCookContainer}>
          <RightCookHistory />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    //flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: '#f5f5f5',
    paddingHorizontal: RFValue(22),
  },
  navigatorContainer: {
    width: RFPercentage(10),
    marginLeft: RFValue(-22),
    //height: RFPercentage(100),
  },
  leftCookContainer: {
    width: RFPercentage(50),
    height: RFPercentage(100),
    marginLeft: RFValue(-41),
  },
  rightCookContainer: {
    width: RFPercentage(50),
    height: RFPercentage(100),
  },
});
