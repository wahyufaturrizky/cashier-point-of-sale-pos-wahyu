import React, {Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {RFValue, RFPercentage} from 'react-native-responsive-fontsize';

import Header from './header';
import DataDetails from './dataDetails';
import OrderDetails from './orderDetails';

export default class Body extends Component {
  render() {
    return (
      <View style={styles.bodyContainer}>
        <Header />
        <View style={styles.bodyWrapper}>
          <View style={styles.transactionWrapper}>
            <Text style={styles.transactionText}>TRANSACTION DETAILS</Text>
          </View>
          <View>
            <DataDetails
              img={require('../../../../assets/General/Sidebar/IconRunningOrderSelected.png')}
              text1="Bil No."
              text2="101"
            />
            <DataDetails
              img={require('../../../../assets/Waiter/Setting/person.png')}
              text1="Customer Name"
              text2="Muti"
            />
            <DataDetails
              img={require('../../../../assets/Cashier/Activity/clock.png')}
              text1="Time of Purchase"
              text2="March, 9th2020 at 02:02 PM"
            />
          </View>
          <View style={styles.OrderTypeWrapper}>
            <Text style={styles.textOrderType}>DINE IN</Text>
          </View>
          <View>
            <OrderDetails
              textN="1"
              textOr="Nasi Goreng"
              textOr2="telur"
              status="SUCCEED"
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bodyContainer: {
    flex: 1,
  },
  bodyWrapper: {
    flex: 1,
    backgroundColor: '#ffffff',
    height: RFPercentage(100),
    width: RFPercentage(65),
  },
  transactionWrapper: {
    marginTop: RFValue(10),
    marginLeft: RFValue(13),
  },
  transactionText: {
    fontSize: RFValue(10),
  },
  imageWrapper: {
    //backgroundColor: 'red',
    justifyContent: 'center',
  },
  textBackWrapper: {
    justifyContent: 'center',
  },
  textOrderDetailsWrapper: {
    flex: 1,
    justifyContent: 'center',
  },
  textBack: {
    fontSize: RFValue(9),
    paddingLeft: RFValue(10),
  },
  textOderDetails: {
    alignSelf: 'flex-end',
    paddingRight: RFPercentage(8),
    fontSize: RFValue(12),
  },
  OrderTypeWrapper: {
    marginLeft: RFValue(-90),
    marginTop: RFValue(30),
    alignItems: 'center',
  },
  textOrderType: {
    fontSize: RFValue(10),
  },
});
