import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, ScrollView} from 'react-native';
import {RFValue, RFPercentage} from 'react-native-responsive-fontsize';

import LeftWaste from '../Waste/leftWaste/leftWaste';
import RightWaste from '../Waste/rightWaste/body';
import KitchenNavigator from '../KitchenNavigator';

export default class CookHistory extends Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.navigatorContainer}>
          <KitchenNavigator
            navigation={this.props.navigation}
            WasteStatus={true}
          />
        </View>
        <View>
          <ScrollView style={styles.leftCookContainer}>
            <LeftWaste />
          </ScrollView>
        </View>
        <View style={styles.rightCookContainer}>
          <RightWaste />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexWrap: 'wrap',
    backgroundColor: '#f5f5f5',
    paddingHorizontal: RFValue(22),
  },
  navigatorContainer: {
    width: RFPercentage(10),
    marginLeft: RFValue(-22),
    //height: RFPercentage(100),
  },
  leftCookContainer: {
    height: RFPercentage(100),
    width: RFPercentage(50),
    marginLeft: RFValue(-41),
  },
  rightCookContainer: {
    width: RFPercentage(50),
    height: RFPercentage(100),
  },
});
