import React, {Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {RFValue, RFPercentage} from 'react-native-responsive-fontsize';

export default class DataDetails extends Component {
  render() {
    return (
      <View style={styles.dataContainer}>
        <View style={styles.textNWrapper}>
          <Text style={styles.text1}>{this.props.textN}</Text>
        </View>
        <View style={styles.text1Wrapper}>
          <Text style={styles.text2}>{this.props.textOr}</Text>
          <Text style={styles.text3}>{this.props.textOr2}</Text>
        </View>
        <View style={styles.text2Wrapper}>
          <Text style={styles.text4}>{this.props.status}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dataContainer: {
    //flex: 1,
    flexDirection: 'row',
    //backgroundColor: 'red',
    justifyContent: 'center',
    marginLeft: RFValue(50),
    marginTop: RFValue(8),
  },
  textNWrapper: {
    //backgroundColor: 'red',
    width: RFValue(20),
    height: RFValue(20),
    marginLeft: RFValue(-30),
    justifyContent: 'center',
    alignItems: 'center',
  },
  text1Wrapper: {
    //backgroundColor: 'green',
    marginLeft: RFValue(10),
    //justifyContent: 'center',
  },
  text2Wrapper: {
    flex: 1,
    justifyContent: 'center',
  },
  text1: {
    fontSize: RFValue(9),
  },
  text2: {
    alignSelf: 'flex-start',
    paddingRight: RFPercentage(8),
    fontSize: RFValue(10),
    //backgroundColor: 'yellow',
  },
  text4: {
    alignSelf: 'flex-end',
    paddingRight: RFPercentage(20),
    fontSize: RFValue(9),
    color: '#1ADD3B',
  },
});
