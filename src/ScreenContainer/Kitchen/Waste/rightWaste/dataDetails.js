import React, {Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {RFValue, RFPercentage} from 'react-native-responsive-fontsize';

export default class DataDetails extends Component {
  render() {
    return (
      <View style={styles.dataContainer}>
        <View style={styles.imageWrapper}>
          <Image source={this.props.img} style={styles.imageStyle} />
        </View>
        <View style={styles.text1Wrapper}>
          <Text style={styles.text1}>{this.props.text1}</Text>
        </View>
        <View style={styles.text2Wrapper}>
          <Text style={styles.text2}>{this.props.text2}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dataContainer: {
    //flex: 1,
    flexDirection: 'row',
    //backgroundColor: 'red',
    justifyContent: 'center',
    marginLeft: RFValue(50),
    marginTop: RFValue(8),
  },
  imageWrapper: {
    //backgroundColor: 'red',
    width: RFValue(20),
    height: RFValue(20),
    marginLeft: RFValue(-26),
  },
  imageStyle: {
    flex: 1,
    height: undefined,
    width: undefined,
    resizeMode: 'contain',
  },
  text1Wrapper: {
    //backgroundColor: 'red',
    marginLeft: RFValue(10),
    justifyContent: 'center',
  },
  text2Wrapper: {
    flex: 1,
  },
  text1: {
    fontSize: RFValue(9),
  },
  text2: {
    alignSelf: 'flex-end',
    paddingRight: RFPercentage(20),
    fontSize: RFValue(9),
  },
});
