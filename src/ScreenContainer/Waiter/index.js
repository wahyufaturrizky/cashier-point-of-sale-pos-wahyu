import TableSelection from './TableSelection';
import Menu from './Menu';
import OrderConfirmation from './OrderConfirmation';
import Notification from './Notification';
import WaiterSetting from './WaiterSetting';
import WaiterEditProfile from './WaiterSetting/WaiterEditProfile';

export {
  TableSelection,
  Menu,
  OrderConfirmation,
  Notification,
  WaiterSetting,
  WaiterEditProfile,
};
