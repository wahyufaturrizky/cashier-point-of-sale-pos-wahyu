import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class OrderButton extends Component {
  render() {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          height: RFValue(40),
          backgroundColor: '#FEBF11',
          bottom: '3%',
          borderRadius: RFValue(4),
          width: '95%',
          paddingHorizontal: '5%',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View>
          <Text style={{color: 'white', fontSize: RFValue(13)}}>
            2 Items | Rp 60.000
          </Text>
          <Text
            style={{left: RFValue(8), color: 'white', fontSize: RFValue(9)}}>
            Spaghetti Carbonara (x2)
          </Text>
        </View>
        <View>
          <Image
            source={require('../../../assets/Waiter/Home/shopping.png')}
            resizeMode="contain"
            style={{width: undefined, height: '50%', aspectRatio: 1}}
          />
        </View>
      </TouchableOpacity>
    );
  }
}
