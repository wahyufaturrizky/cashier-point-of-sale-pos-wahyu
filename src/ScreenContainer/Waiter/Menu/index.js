import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  ScrollView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Modal,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import WaiterHeader from '../WaiterHeader';
import OrderButton from './OrderButton';
import ListMenu from './ListMenu';
import Banner from './Banner';

export default class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalNotesVisible: false,
      modalCartVisible: true,
    };
  }
  _add = () => {
    return console.log('Add button clicked');
  };
  _modalVisible = () => {
    this.setState({
      modalNotesVisible: !this.state.modalNotesVisible,
    });
    return console.log('modal switched');
  };
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#F7F7F7'}}>
        <View
          style={{
            height: '10%',
            marginBottom: '3%',
            paddingBottom: '1%',
            backgroundColor: 'white',
            shadowOpacity: 1,
            elevation: 1,
          }}>
          <WaiterHeader
            title="Menu"
            back="TableSelection"
            next="OrderConfirmation"
            navigation={this.props.navigation}
          />
        </View>
        <ScrollView style={{flex: 1}}>
          <View style={{backgroundColor: 'white', paddingBottom: RFValue(150)}}>
            <View style={styles.textSearchWrap}>
              <TextInput placeholder="search" style={styles.textSearch} />
            </View>
            <View style={styles.banner}>
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal
                style={{height: '15%', padding: RFValue(10)}}>
                <Banner />
                <Banner />
                <Banner />
                <Banner />
                <Banner />
                <Banner />
                <Banner />
              </ScrollView>
            </View>
            <View style={{flex: 1, width: '100%', justifyContent: 'center'}}>
              <ListMenu />
              <ListMenu />
              <ListMenu />
              <ListMenu />
              <ListMenu />
              <ListMenu />
              <ListMenu />
              <ListMenu />
              <ListMenu />
              <ListMenu />
              <View style={{height: 100}} />
            </View>
          </View>
        </ScrollView>
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          {this.state.modalCartVisible ? <OrderButton /> : <View />}
          <Modal
            style={{height: '20%', justifyContent: 'flex-end'}}
            visible={this.state.modalNotesVisible}
            transparent={true}>
            <View
              style={{
                position: 'absolute',
                bottom: '8%',
                height: '50%',
                width: '100%',
                backgroundColor: 'white',
                padding: '5%',
              }}>
              <Text>Add Notes</Text>
              <View
                style={{
                  height: '60%',
                  width: '100%',
                  marginVertical: '5%',
                  borderBottomWidth: 0.5,
                  borderTopWidth: 0.5,
                }}>
                <TextInput
                  style={{width: '100%'}}
                  placeholder="Example: No Spices"
                  multiline={true}
                />
              </View>
              <TouchableOpacity
                style={{
                  width: '30%',
                  alignSelf: 'flex-end',
                  height: undefined,
                  aspectRatio: 4,
                  borderRadius: RFValue(3),
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#878787',
                }}>
                <Text style={{color: 'white', fontSize: RFValue(15)}}>Add</Text>
              </TouchableOpacity>
            </View>
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  textSearchWrap: {
    //position: 'relative',
    //flex: 1,
    marginBottom: 4,
  },
  textSearch: {
    borderRadius: 3,
    height: 40,
    fontSize: 13,
    paddingLeft: 40,
    paddingTop: 10,
    backgroundColor: 'white',
    marginTop: 16,
    marginHorizontal: 9,
    shadowOpacity: 0.8,
    elevation: 2,
  },
  banner: {
    marginBottom: 10,
  },
});
