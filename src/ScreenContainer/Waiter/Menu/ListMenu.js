/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {Text, View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class ListMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 0,
    };
  }

  _increase = () => {
    this.setState({
      quantity: parseInt(this.state.quantity + 1),
    });
  };

  _decrease = () => {
    this.setState({
      quantity: parseInt(this.state.quantity - 1),
    });
  };

  render() {
    return (
      <View style={styles.imageMenuWrap}>
        <View style={styles.imageMenu} />
        <View style={styles.imageTextAdd}>
          <View style={{width: '70%', justifyContent: 'center'}}>
            <Text style={{fontSize: RFValue(15)}}>Spaghetti Carbonara</Text>
            <Text style={{fontSize: RFValue(10), color: '#878787'}}>
              A pasta that mixed with tomato sauce, meatballs and cheese
            </Text>
          </View>
          <View
            style={{
              width: '30%',
              height: '100%',
              alignItems: 'flex-end',
              justifyContent: 'space-around',
            }}>
            <View
              style={{
                height: '30%',
                width: '100%',
                marginBottom: '30%',
                alignItems: 'flex-end',
              }}>
              <Text style={{fontSize: RFValue(12)}}>Rp 60.000</Text>
            </View>
            {this.state.quantity == 0 ? (
              <View
                style={{
                  height: '50%',
                  flexDirection: 'row',
                  width: undefined,
                  aspectRatio: 5.5,
                  justifyContent: 'flex-end',
                  alignItems: 'flex-end',
                }}>
                <TouchableOpacity onPress={() => this._increase()}>
                  <Image
                    style={{height: '100%', width: undefined, aspectRatio: 4}}
                    source={require('../../../assets/Waiter/Home/add.png')}
                  />
                </TouchableOpacity>
              </View>
            ) : (
              <View
                style={{
                  height: '50%',
                  flexDirection: 'row',
                  width: undefined,
                  aspectRatio: 5.5,
                  justifyContent: 'space-between',
                  alignItems: 'flex-end',
                }}>
                <TouchableOpacity
                  style={{
                    height: '100%',
                    width: undefined,
                    justifyContent: 'center',
                    alignItems: 'center',
                    aspectRatio: 1,
                    backgroundColor: 'white',
                    borderRadius: RFValue(5),
                    borderWidth: 1,
                    borderColor: '#D8D8D8',
                  }}>
                  <Image
                    source={require('../../../assets/Waiter/Home/edit.png')}
                    style={{height: '80%', width: undefined, aspectRatio: 1}}
                  />
                </TouchableOpacity>
                <View
                  style={{
                    height: '100%',
                    width: undefined,
                    aspectRatio: 4.3,
                    borderRadius: RFValue(3),
                    borderWidth: 1,
                    borderColor: '#D8D8D8',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <TouchableOpacity
                    onPress={() => this._decrease()}
                    style={{
                      height: '100%',
                      width: '30%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{fontSize: RFValue(20), color: '#F4282D'}}>
                      -
                    </Text>
                  </TouchableOpacity>
                  <View
                    style={{
                      height: '100%',
                      width: '40%',
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRightWidth: 1,
                      borderLeftWidth: 1,
                      borderColor: '#D8D8D8',
                    }}>
                    <Text style={{fontSize: RFValue(12)}}>
                      {this.state.quantity}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => this._increase()}
                    style={{
                      height: '100%',
                      width: '30%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{fontSize: RFValue(20), color: '#33C15D'}}>
                      +
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            )}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageMenu: {
    width: undefined,
    height: '100%',
    aspectRatio: 1,
    borderRadius: 6,
    backgroundColor: '#FEBF11',
  },
  imageTextAdd: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    width: '100%',
    padding: 11,
    height: '100%',
  },
  imageMenuWrap: {
    flexDirection: 'row',
    paddingLeft: 12,
    alignItems: 'center',
    marginVertical: 5,
    height: RFValue(60),
    marginVertical: RFValue(10),
  },
  imageMenuContainer: {
    backgroundColor: '#f6f6f6',
  },
});
