import React, {Component} from 'react';
import {Image, View, StyleSheet, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class Banner extends Component {
  render() {
    return (
      <TouchableOpacity style={styles.imageWrap}>
        <Image
          source={require('../../../assets/Waiter/Home/banner.jpg')}
          style={styles.imageBanner}
          resizeMode="contain"
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  imageWrap: {
    width: undefined,
    height: '100%',
    aspectRatio: 1.7,
    padding: 9,
  },
  imageBanner: {
    width: undefined,
    height: undefined,
    flex: 1,
    resizeMode: 'cover',
    borderRadius: 8,
  },
});
