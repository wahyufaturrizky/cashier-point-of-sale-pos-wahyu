import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {CheckBox} from 'react-native-elements';

export default class NotificationItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: false,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.checkedAll && !this.props.checkedAll) {
      this.setState({
        item: nextProps.checkedAll,
      });
    } else {
      this.setState({
        item: nextProps.checkedAll,
      });
    }
  }
  render() {
    return (
      <View
        style={{
          width: '100%',
          height: undefined,
          paddingHorizontal: '2%',
          aspectRatio: 10,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          backgroundColor: 'white',
          marginBottom: RFValue(5),
          borderWidth: 0.5,
          borderColor: '#DADADA',
        }}>
        {console.log(this.state.item)}
        <View
          style={{
            height: '100%',
            width: '10%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <CheckBox
            containerStyle={{
              height: '100%',
              width: undefined,
              aspectRatio: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            checkedIcon={
              <Image
                resizeMode="contain"
                style={{
                  width: undefined,
                  height: '100%',
                  aspectRatio: 1,
                  alignSelf: 'center',
                }}
                source={require('../../../assets/General/checkTrue.png')}
              />
            }
            uncheckedIcon={
              <Image
                resizeMode="contain"
                style={{
                  width: undefined,
                  height: '100%',
                  aspectRatio: 1,
                  alignSelf: 'center',
                }}
                source={require('../../../assets/General/checkFalse.png')}
              />
            }
            checked={this.state.item}
            onPress={() => this.setState({item: !this.state.item})}
          />
        </View>
        <View style={{height: '100%', width: '25%', justifyContent: 'center'}}>
          <Text style={{fontSize: RFValue(14), color: '#6A6A6A'}}>
            TAKEAWAY
          </Text>
        </View>
        <View style={{height: '100%', width: '40%'}}>
          <Text style={{fontSize: RFValue(14), color: '#6A6A6A'}}>
            Nasi Goreng
          </Text>
          <Text style={{fontSize: RFValue(10), color: '#CBCBCB'}}>
            Customer: Bilal
          </Text>
        </View>
        <View
          style={{
            height: '100%',
            width: '25%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{
              height: '75%',
              width: undefined,
              aspectRatio: 3,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#878787',
            }}>
            <Text style={{color: 'white', fontSize: RFValue(14)}}>Collect</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
