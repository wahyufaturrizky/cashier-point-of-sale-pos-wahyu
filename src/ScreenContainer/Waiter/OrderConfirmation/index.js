import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import WaiterHeader from '../WaiterHeader';
import ListMenu from '../Menu/ListMenu';

export default class OrderConfirmation extends Component {
  render() {
    return (
      <View style={{flex: 1, justifyContent: 'space-between'}}>
        <View
          style={{
            height: '10%',
            marginBottom: '3%',
            paddingBottom: '1%',
            backgroundColor: 'white',
            shadowOpacity: 1,
            elevation: 1,
          }}>
          <WaiterHeader
            title="Menu"
            back="Menu"
            navigation={this.props.navigation}
          />
        </View>
        <View style={{height: '90%'}}>
          <View
            style={{
              height: '22%',
              width: '100%',
              justifyContent: 'flex-start',
              backgroundColor: 'white',
              padding: '5%',
            }}>
            <View
              style={{
                flexDirection: 'row',
                height: '60%',
                marginBottom: RFValue(10),
                width: '100%',
              }}>
              <View style={{height: '100%', width: undefined, aspectRatio: 1}}>
                <Image
                  style={{height: '100%', width: '100%'}}
                  resizeMode="contain"
                  source={require('../../../assets/Waiter/Home/circle.png')}
                />
              </View>
              <View style={{marginLeft: '2%'}}>
                <Text style={{fontSize: RFValue(13)}}>Order Confirmation</Text>
                <Text style={{fontSize: RFValue(27)}}>Table 5</Text>
              </View>
            </View>
            <View
              style={{
                height: '40%',
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                borderRadius: 3,
                borderWidth: 0.5,
                borderColor: '#C5C5C5',
                backgroundColor: '#F7F7F7',
              }}>
              <View
                style={{
                  height: '80%',
                  width: undefined,
                  aspectRatio: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingHorizontal: RFValue(5),
                  borderRightWidth: 1,
                  borderColor: '#C5C5C5',
                }}>
                <Image
                  source={require('../../../assets/Waiter/Home/person.png')}
                  style={{height: '60%', width: undefined, aspectRatio: 1}}
                />
              </View>
              <TextInput
                style={{
                  paddingVertical: 0,
                  paddingHorizontal: '5%',
                  fontSize: RFValue(12),
                }}
                placeholder="Customer Name"
              />
            </View>
          </View>
          <View
            style={{height: '72%', backgroundColor: 'white', marginTop: '3%'}}>
            <ScrollView style={{padding: RFValue(20)}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingBottom: '3%',
                  borderBottomWidth: 0.5,
                  borderColor: '#D8D8D8',
                }}>
                <View style={{height: '100%'}}>
                  <Text style={{fontSize: RFValue(16)}}>Order List</Text>
                </View>
                <TouchableOpacity
                  style={{
                    width: '30%',
                    height: undefined,
                    aspectRatio: 4.8,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    backgroundColor: '#FE724C',
                    borderRadius: RFValue(2),
                  }}>
                  <View
                    style={{
                      height: '100%',
                      width: undefined,
                      aspectRatio: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRightWidth: 1,
                      borderColor: 'white',
                    }}>
                    <Text style={{color: 'white'}}>+</Text>
                  </View>
                  <View
                    style={{
                      height: '100%',
                      width: undefined,
                      aspectRatio: 3.8,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{color: 'white', fontSize: RFValue(12)}}>
                      Add More
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <ListMenu />
              <ListMenu />
              <ListMenu />
              <View
                style={{
                  height: undefined,
                  width: '100%',
                  marginBottom: RFValue(100),
                  aspectRatio: 5,
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  style={{
                    width: '75%',
                    height: undefined,
                    aspectRatio: 7,
                    backgroundColor: '#FEBF11',
                    borderRadius: RFValue(5),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{color: 'white', fontSize: RFValue(16)}}>
                    Place Order
                  </Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}
