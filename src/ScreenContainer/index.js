import React from 'react';
import {Image} from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import {Login, RunningOrder, Setting, Profile, Printer} from './View';

import {
  Register,
  Transaction,
  Discount,
  KitchenStatus,
  Payment,
  SplitBill,
  SplitPayment,
  Activity,
  TransactionHistory,
  ClosingCash,
  ShiftHistory,
  PaymentSuccess,
} from './Cashier';

import {
  TableSelection,
  Menu,
  OrderConfirmation,
  Notification,
  WaiterSetting,
  WaiterEditProfile,
} from './Waiter';

import {KitchenRunningOrder, CookHistory, Waste} from './Kitchen';

const LoginStack = createStackNavigator(
  {
    Login,
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
  },
);

const CashierStack = createStackNavigator(
  {
    Register, //All Done
    Discount,
    Transaction,
    KitchenStatus,
    Payment,
    PaymentSuccess,
    SplitBill,
    SplitPayment,
    Activity,
    TransactionHistory, //Nearly Done minus list
    ClosingCash, //All Done minus list
    ShiftHistory, //All Done minus list
    Setting,
    Profile, //All Done
    Printer, //All Done
  },
  {
    initialRouteName: 'Register',
    headerMode: 'none',
  },
);

const CheckerStack = createStackNavigator(
  {
    RunningOrder,
    Setting,
  },
  {
    initialRouteName: 'RunningOrder',
    headerMode: 'none',
  },
);

const WaiterHomeStack = createStackNavigator(
  {
    TableSelection,
    Menu,
    OrderConfirmation,
  },
  {
    headerMode: 'none',
    config: {
      restSpeedThreshold: 100000,
    },
  },
);

const WaiterSettingStack = createStackNavigator(
  {
    WaiterSetting,
    WaiterEditProfile,
  },
  {
    headerMode: 'none',
  },
);

const WaiterBottomTab = createBottomTabNavigator(
  {
    WaiterSettingStack: {
      screen: WaiterSettingStack,
      navigationOptions: {
        tabBarIcon: ({focused}) =>
          focused ? (
            <Image
              source={require('../assets/Waiter/BottomTabIcon/ActiveSetting.png')}
              resizeMode="contain"
              style={{width: undefined, height: '80%', aspectRatio: 1}}
            />
          ) : (
            <Image
              source={require('../assets/Waiter/BottomTabIcon/InactiveSetting.png')}
              resizeMode="contain"
              style={{width: undefined, height: '80%', aspectRatio: 1}}
            />
          ),
      },
    },
    WaiterHomeStack: {
      screen: WaiterHomeStack,
      navigationOptions: {
        tabBarIcon: ({focused}) =>
          focused ? (
            <Image
              source={require('../assets/Waiter/BottomTabIcon/ActiveMenu.png')}
              resizeMode="contain"
              style={{width: undefined, height: '80%', aspectRatio: 1}}
            />
          ) : (
            <Image
              source={require('../assets/Waiter/BottomTabIcon/InactiveMenu.png')}
              resizeMode="contain"
              style={{width: undefined, height: '80%', aspectRatio: 1}}
            />
          ),
      },
    },
    Notification: {
      screen: Notification,
      navigationOptions: {
        tabBarIcon: ({focused}) =>
          focused ? (
            <Image
              source={require('../assets/Waiter/BottomTabIcon/ActiveNotification.png')}
              resizeMode="contain"
              style={{width: undefined, height: '80%', aspectRatio: 1}}
            />
          ) : (
            <Image
              source={require('../assets/Waiter/BottomTabIcon/InactiveNotification.png')}
              resizeMode="contain"
              style={{width: undefined, height: '80%', aspectRatio: 1}}
            />
          ),
      },
    },
  },
  {
    initialRouteName: 'WaiterHomeStack',
    tabBarOptions: {
      showLabel: false,
    },
  },
);

const KitchenStack = createStackNavigator(
  {
    KitchenRunningOrder,
    CookHistory,
    Waste,
  },
  {
    initialRouteName: 'KitchenRunningOrder',
    headerMode: 'none',
  },
);

const Routes = createSwitchNavigator(
  {
    LoginStack,
    CashierStack,
    CheckerStack,
    KitchenStack,
    WaiterBottomTab,
  },
  {
    initialRouteName: 'LoginStack',
    animationEnabled: false,
  },
);

export default createAppContainer(Routes);
