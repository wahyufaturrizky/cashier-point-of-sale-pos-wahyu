import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import NumberFormat from 'react-number-format';

export default class ItemSplit extends Component {
  render() {
    return (
      <TouchableOpacity
        style={{
          width: '100%',
          height: undefined,
          aspectRatio: 13,
          borderBottomWidth: 1,
          borderColor: '#EAEAEA',
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingHorizontal: '5%',
        }}>
        <View style={{justifyContent: 'center'}}>
          <Text style={{fontSize: RFValue(9)}}>{this.props.foodName}</Text>
          <Text style={{fontSize: RFValue(7), color: '#CBCBCB'}}>
            {this.props.modifier}
          </Text>
        </View>
        <View style={{justifyContent: 'center'}}>
          <NumberFormat
            value={this.props.price}
            displayType={'text'}
            thousandSeparator={true}
            prefix={'Rp. '}
            renderText={(value) => (
              <Text style={{fontSize: RFValue(8)}}>{value}</Text>
            )}
          />
        </View>
      </TouchableOpacity>
    );
  }
}
