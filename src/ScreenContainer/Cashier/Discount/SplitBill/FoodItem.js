/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import NumberFormat from 'react-number-format';

export default class FoodItem extends Component {
  render() {
    return (
      <View
        style={{
          width: '100%',
          height: undefined,
          aspectRatio: 8,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          style={{
            height: '80%',
            width: '100%',
            flexDirection: 'row',
            flexDirection: 'row',
            paddingHorizontal: RFValue(10),
            justifyContent: 'space-between',
            backgroundColor: 'white',
            elevation: 3,
          }}>
          <View
            style={{
              height: '100%',
              width: undefined,
              aspectRatio: 6,
              flexDirection: 'row',
            }}>
            <View style={{justifyContent: 'center'}}>
              <Text style={{fontSize: RFValue(9)}}>{this.props.foodName}</Text>
              <Text style={{fontSize: RFValue(7), color: '#CBCBCB'}}>
                {this.props.modifier}
              </Text>
            </View>
          </View>
          <View
            style={{
              height: '100%',
              width: undefined,
              aspectRatio: 3,
              justifyContent: 'center',
              alignItems: 'flex-end',
            }}>
            <NumberFormat
              value={this.props.price}
              displayType={'text'}
              thousandSeparator={true}
              prefix={'Rp. '}
              renderText={(value) => (
                <Text style={{fontSize: RFValue(8)}}>{value}</Text>
              )}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
