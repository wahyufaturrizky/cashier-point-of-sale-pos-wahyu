/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from 'react-native';
import RightTransactionHeader from './RightTransactionHeader';
import {RFValue} from 'react-native-responsive-fontsize';

import InvoiceList from '../../View/Component/InvoiceList';
import InvoiceBill from '../../View/Component/InvoiceBill';
import InvoiceListTotal from '../../View/Component/InvoiceListTotal';
import Cart from '../../View/Component/Cart';
import {connect} from 'react-redux';
import DiscountSelected from './DiscountSelected';

class RightTransaction extends Component {
  render() {
    return (
      <View style={{width: undefined, height: undefined}}>
        <View
          style={{
            paddingHorizontal: '2%',
            height: '13.8%',
            backgroundColor: 'white',
          }}>
          <RightTransactionHeader
            customerDetailDineInModalSwitch={
              this.props.customerDetailDineInModalSwitch
            }
            isCustomerDetailDineIn={this.props.isCustomerDetailDineIn}
            takeawayModalSwitch={this.props.takeawayModalSwitch}
            isTakeaway={this.props.isTakeaway}
            deliveryModalSwitch={this.props.deliveryModalSwitch}
            isDelivery={this.props.isDelivery}
          />
        </View>

        <View
          style={{
            height: '86.14%',
            marginTop: '2.526%',
            backgroundColor: 'white',
          }}>
          <View style={{padding: RFValue(10), height: '53.492%'}}>
            <ScrollView>
              {/* <Cart addItemModal={this.addItemModalSwitch} /> */}
              {/* {this.props.allTransactionMenu.foodMenuData && this.props.allTransactionMenu.foodMenuData.map((allTransactionMenus, index) => (
                <InvoiceList 
                  quantity={allTransactionMenus.quantity_food}
                  title={allTransactionMenus.food_name} 
                  amount={allTransactionMenus.price_food} 
                  
                  />
              ))} */}
            </ScrollView>
          </View>
          <View
            style={{
              backgroundColor: 'white',
              justifyContent: 'space-between',
              height: '25.854%',
              paddingHorizontal: 15,
              paddingBottom: 5,
            }}>
            {/* <InvoiceListTotal title="Sub Total" amount={this.props.subTotal} /> */}
            {/* <InvoiceListTotal title="Tax / Service" amount={0} /> */}
            {/* <InvoiceListTotal title="Discount" amount={this.props.amountDiscount} /> */}
            {/* <InvoiceListTotal
              title="Discount"
              amount={this.props.all_data_discount.price_discount}
            /> */}
            {console.log('all_data_discount = ', this.props.all_data_discount)}
            {this.props.all_data_discount.price_discount === undefined ? (
              <View
                style={{
                  height: '100%',
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text>No Discount Selected</Text>
              </View>
            ) : (
              <DiscountSelected
                // amount={this.props.all_data_discount.price_discount}
                discountName={this.props.all_data_discount.discount_name}
              />
            )}
            {/* <InvoiceBill title="Total" amount={this.props.subTotal} /> */}
          </View>

          <View
            style={{
              marginTop: '2.377%',
              backgroundColor: 'white',
              paddingHorizontal: 15,
              alignContent: 'center',
              height: '14.562%',
              marginBottom: '3.715%',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                height: '100%',
              }}>
              <TouchableOpacity
                style={{width: '100%'}}
                onPress={() => this.props.navigation.navigate('Transaction')}>
                <View
                  style={{
                    flexDirection: 'row',
                    flex: 1,
                    backgroundColor: '#FEBF11',
                    borderRadius: RFValue(5),
                    marginRight: 2,
                    marginBottom: 8,
                    shadowColor: '#DF2428',
                    shadowOffset: {width: 0, height: 2},
                    shadowOpacity: 100,
                    shadowRadius: 2.62,
                    elevation: 3,
                  }}>
                  <View
                    style={{
                      backgroundColor: '#FEBF11',
                      paddingHorizontal: 10,
                      paddingVertical: 2,
                      justifyContent: 'center',
                      marginBottom: 6,
                      marginRight: 6,
                      borderRadius: 10,
                      flex: 1,
                    }}>
                    <Text
                      style={{
                        fontSize: RFValue(15),
                        textAlign: 'center',
                        color: '#FFFFFF',
                      }}>
                      Back
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    allTransactionMenu: state.rightFoodTransaction.allTransactionMenu,
    all_data_discount: state.rightFoodTransaction.all_data_discount,
    subTotal: state.rightFoodTransaction.subTotal,
    amountDiscount: state.rightFoodTransaction.amountDiscount,
  };
};

export default connect(mapStateToProps)(RightTransaction);
