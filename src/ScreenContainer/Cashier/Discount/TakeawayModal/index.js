import React, {Component} from 'react';
import {
  Text,
  View,
  Modal,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import ModalHeader from '../../../View/Component/ModalHeader';

export default class TakeawayModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
    };
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
  }
  render() {
    return (
      <Modal visible={this.state.visible} transparent={true}>
        <View
          style={{
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#000000aa',
          }}>
          <View
            style={{
              height: '90%',
              width: '90%',
              backgroundColor: 'white',
              alignItems: 'center',
              borderRadius: RFValue(2),
            }}>
            <View
              style={{
                height: '10%',
                width: '100%',
                shadowOpacity: 1,
                backgroundColor: 'white',
                elevation: 1,
              }}>
              <ModalHeader
                title="CUSTOMER DETAIL - TAKEAWAY"
                close={this.props.takeawayModalSwitch}
                // save={this.props.closeAndTakeaway}
              />
            </View>

            <View style={{height: '90%', width: '100%'}}>
              <View
                style={{
                  width: '100%',
                  alignItems: 'center',
                  marginVertical: RFValue(15),
                }}>
                <View
                  style={{
                    width: '70%',
                    // alignItems: 'center'
                  }}>
                  <Text style={{fontSize: RFValue(13)}}>Customer Data</Text>
                  <TextInput
                    style={styles.inputStyle}
                    placeholder="Input Name"
                  />
                  <TextInput style={styles.inputStyle} placeholder="Address" />
                  <TextInput
                    style={styles.inputStyle}
                    placeholder="Phone Number"
                  />
                  <TextInput style={styles.inputStyle} placeholder="Email" />
                </View>

                <TouchableOpacity
                  style={{
                    width: '70%',
                    height: undefined,
                    aspectRatio: 11,
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                    marginVertical: RFValue(20),
                    borderRadius: RFValue(3),
                    backgroundColor: '#FEBF11',
                  }}
                  onPress={this.props.closeAndTakeaway}>
                  <Text style={{color: 'white'}}>Save</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  inputStyle: {
    height: RFValue(35),
    width: '100%',
    marginTop: RFValue(10),
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderColor: '#C6CED9',
    borderRadius: 3,
    paddingHorizontal: '2%',
    paddingVertical: 0,
    fontSize: RFValue(9),
  },
});
