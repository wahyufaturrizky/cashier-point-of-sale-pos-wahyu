/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {Text, View, TextInput, TouchableOpacity} from 'react-native';
import CashierNavigator from '../../Cashier/CashierNavigator';
import LeftTransaction from './LeftTransaction';
import RightTransaction from './RightTransaction';
import CustomerDetailDineInModal from './CustomerDetailDineInModal';
import DineInModal from './DineInModal';
import TakeawayModal from './TakeawayModal';
import DeliveryModal from './DeliveryModal';
import CategoryModal from './CategoryModal';
import AddItemModal from './AddItemModal';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getAllDataGlobalDiscount} from '../../../redux/actions/transaction/rightFoodTransactionAction';
import RunningPrintService from '../../../services/runningPrint.service';

class Discount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customerDetailDineInModalVisible: false,
      dineInModalVisible: false,
      takeawayModalVisible: false,
      deliveryModalVisible: false,
      addItemModalVisible: false,
      isCustomerDetailDineIn: false,
      isTakeaway: false,
      isDelivery: false,
      isDineIn: false,
      categoryModalVisible: false,
      foodItem: null,
      discount_name: '',
      discount_id: null,
      price_discount: null,
    };
  }

  getAllDataDiscount = (discountName, discountId, discountPrice) => {
    console.log(
      'discountName, discountId, discountPrice = ',
      discountName,
      discountId,
      discountPrice,
    );
    console;
    // this.setState({
    //   discount_name: discountName,
    //   discount_id: discountId,
    //   price_discount: discountPrice,
    // });

    const allDataDiscount = {
      discount_name: discountName,
      discount_id: discountId,
      price_discount: discountPrice,
    };
    this.props.getAllDataGlobalDiscount(allDataDiscount);
  };

  render() {
    return (
      <View style={{flex: 1, flexDirection: 'row'}}>
        <View
          style={{
            flex: 85,
            borderColor: '#DADADA',
            shadowOpacity: 0.5,
            shadowRadius: 5,
            borderRight: 1,
          }}>
          <CashierNavigator
            discountStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View style={{flex: 640}}>
          <LeftTransaction
            categoryModalSwitch={this.categoryModalSwitch}
            addItemModalSwitch={this.addItemModalSwitch}
            getAllDataDiscount={this.getAllDataDiscount}
          />
        </View>
        <View style={{flex: 555}}>
          <RightTransaction
            navigation={this.props.navigation}
            customerDetailDineInModalSwitch={
              this.customerDetailDineInModalSwitch
            }
            isCustomerDetailDineIn={this.state.isCustomerDetailDineIn}
            takeawayModalSwitch={this.takeawayModalSwitch}
            isTakeaway={this.state.isTakeaway}
            deliveryModalSwitch={this.deliveryModalSwitch}
            isDelivery={this.state.isDelivery}
          />
        </View>
        <CustomerDetailDineInModal
          visible={this.state.customerDetailDineInModalVisible}
          customerDetailDineInModalSwitch={this.customerDetailDineInModalSwitch}
          toggleCustomerDetailDineIn={this.toggleCustomerDetailDineIn}
          closeAndCustomerDetailDineIn={this.closeAndCustomerDetailDineIn}
          dineInModalSwitch={this.dineInModalSwitch}
        />
        {/* [START DINE IN MODAL] */}
        <DineInModal
          visible={this.state.dineInModalVisible}
          dineInModalSwitch={this.dineInModalSwitch}
          toggleDineIn={this.toggleDineIn}
          closeAndDineIn={this.closeAndDineIn}
        />
        {/* [END DINE IN MODAL] */}
        <TakeawayModal
          visible={this.state.takeawayModalVisible}
          takeawayModalSwitch={this.takeawayModalSwitch}
          toggleTakeaway={this.toggleTakeaway}
          closeAndTakeaway={this.closeAndTakeaway}
        />
        <DeliveryModal
          visible={this.state.deliveryModalVisible}
          deliveryModalSwitch={this.deliveryModalSwitch}
          toggleDelivery={this.toggleDelivery}
          closeAndDelivery={this.closeAndDelivery}
        />
        <CategoryModal
          visible={this.state.categoryModalVisible}
          categoryModalSwitch={this.categoryModalSwitch}
          navigation={this.props.navigation}
        />
        <AddItemModal
          visible={this.state.addItemModalVisible}
          addItemModalSwitch={this.addItemModalSwitch}
          foodItem={this.state.foodItem}
        />
      </View>
    );
  }
  customerDetailDineInModalSwitch = () => {
    this.state.customerDetailDineInModalVisible
      ? this.setState({customerDetailDineInModalVisible: false})
      : this.setState({customerDetailDineInModalVisible: true});
  };
  toggleCustomerDetailDineIn = () => {
    this.state.isCustomerDetailDineIn
      ? this.setState({isCustomerDetailDineIn: false})
      : this.setState({isCustomerDetailDineIn: true});
  };
  closeAndCustomerDetailDineIn = () => {
    this.customerDetailDineInModalSwitch();
    this.toggleCustomerDetailDineIn();
    this.state.isCustomerDetailDineIn = !this.state.isTakeaway
      ? null
      : this.toggleTakeaway();
    this.state.isCustomerDetailDineIn = !this.state.isDelivery
      ? null
      : this.toggleDelivery();
    this.state.isCustomerDetailDineIn = !this.state.isDineIn
      ? null
      : this.toggleDineIn();
  };

  takeawayModalSwitch = () => {
    this.state.takeawayModalVisible
      ? this.setState({takeawayModalVisible: false})
      : this.setState({takeawayModalVisible: true});
  };
  toggleTakeaway = () => {
    this.state.isTakeaway
      ? this.setState({isTakeaway: false})
      : this.setState({isTakeaway: true});
  };
  closeAndTakeaway = () => {
    this.takeawayModalSwitch();
    this.toggleTakeaway();
    this.state.isTakeaway = !this.state.isCustomerDetailDineIn
      ? null
      : this.toggleCustomerDetailDineIn();
    this.state.isTakeaway = !this.state.isDelivery
      ? null
      : this.toggleDelivery();
  };
  deliveryModalSwitch = () => {
    this.state.deliveryModalVisible
      ? this.setState({deliveryModalVisible: false})
      : this.setState({deliveryModalVisible: true});
  };
  toggleDelivery = () => {
    this.state.isDelivery
      ? this.setState({isDelivery: false})
      : this.setState({isDelivery: true});
  };
  closeAndDelivery = () => {
    // console.log('Close and delivery');
    this.deliveryModalSwitch();
    this.toggleDelivery();
    this.state.isDelivery = !this.state.isCustomerDetailDineIn
      ? null
      : this.toggleCustomerDetailDineIn();
    this.state.isDelivery = !this.state.isTakeaway
      ? null
      : this.toggleTakeaway();
  };

  categoryModalSwitch = () => {
    this.state.categoryModalVisible
      ? this.setState({categoryModalVisible: false})
      : this.setState({categoryModalVisible: true});
  };

  addItemModalSwitch = (uuid) => {
    // console.log('doodname',uuid)
    this.state.addItemModalVisible
      ? this.setState({addItemModalVisible: false, foodItem: null})
      : this.setState({addItemModalVisible: true, foodItem: uuid});
  };

  // [START] Fungsi untuk DINE IN Modal

  dineInModalSwitch = () => {
    this.state.dineInModalVisible
      ? this.setState({dineInModalVisible: false})
      : this.setState({dineInModalVisible: true}) &&
        this.state.customerDetailDineInModalVisible
      ? this.setState({customerDetailDineInModalVisible: true})
      : this.setState({customerDetailDineInModalVisible: false});
  };

  toggleDineIn = () => {
    this.state.isDineIn
      ? this.setState({isDineIn: false})
      : this.setState({isDineIn: true});
  };

  closeAndDineIn = () => {
    // console.log('Close and DineIn');
    this.dineInModalSwitch();
    this.toggleDineIn();
    this.state.isDineIn = !this.state.isTakeaway ? null : this.toggleTakeaway();
    this.state.isDineIn = !this.state.isDelivery ? null : this.toggleDelivery();
    this.state.isDineIn = !this.state.isCustomerDetailDineIn
      ? null
      : this.toggleCustomerDetailDineIn();
  };

  // [END] Fungsi untuk DINE IN Modal
}

const mapStateToProps = (state) => {
  return {
    testRedux: state.rightFoodTransaction.testRedux,
    all_data_id_order_printing:
      state.rightFoodTransaction.all_data_id_order_printing,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getAllDataGlobalDiscount,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(Discount);
