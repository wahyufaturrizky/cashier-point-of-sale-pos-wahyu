/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import NumberFormat from 'react-number-format';

export default class DiscountItem extends Component {
  render() {
    return (
      <View
        style={{
          width: '25%',
          height: undefined,
          aspectRatio: 2.1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          style={{
            height: '100%',
            width: '100%',
            paddingVertical: RFValue(8),
            backgroundColor: 'white',
            alignItems: 'center',
            borderRadius: RFValue(3),
            justifyContent: 'center',
            borderColor: '#8B929D',
            borderWidth: RFValue(1),
            // elevation: 3,
          }}
          onPress={() =>
            this.props.pressActionDiscount(
              this.props.discountName,
              this.props.discountid,
              this.props.discountPrice,
            )
          }>
          <View style={{height: '20%', justifyContent: 'center'}}>
            <Text
              style={{
                fontSize: RFValue(8),
                textAlign: 'center',
                fontWeight: '700',
                color: '#4A4A4A',
              }}>
              {this.props.discountName}
            </Text>
          </View>

          {/* [Start Gambar Menu] */}
          {/* <View
            style={{width: '100%', height: '55%', backgroundColor: 'white'}}>
            <Image
              source={this.props.photo}
              resizeMode="cover"
              style={{width: '100%', height: '100%'}}
            />
          </View> */}
          {/* [End Gambar Menu] */}

          {/* [Start harga Menu] */}
          <View
            style={{
              height: '20%',
              justifyContent: 'center',
              marginTop: RFValue(10),
            }}>
            <NumberFormat
              value={this.props.discountPrice}
              displayType={'text'}
              thousandSeparator={true}
              prefix={'Rp '}
              renderText={(value) => (
                <Text style={{fontSize: RFValue(7), color: '#4A4A4A'}}>
                  {value}
                </Text>
              )}
            />
          </View>
          {/* [End harga Menu] */}

          {/* [Start unique Key] */}
          {/* <View>
            <Text>{this.props.uuid}</Text>
          </View> */}
          {/* [End unique Key] */}
        </TouchableOpacity>
      </View>
    );
  }
}
