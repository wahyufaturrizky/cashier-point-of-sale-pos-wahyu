/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import NumberFormat from 'react-number-format';

export default class MenuItem extends Component {
  render() {
    return (
      <View
        style={{
          width: '33.33%',
          height: undefined,
          aspectRatio: 1.1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          style={{
            height: '90%',
            width: '90%',
            paddingVertical: 24,
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderRadius: RFValue(3),
            elevation: 3,
          }}
          onPress={() => this.props.pressAction(this.props.foodName)}>
          <View style={{height: '20%', justifyContent: 'center'}}>
            <Text
              style={{
                fontSize: RFValue(8),
                textAlign: 'center',
                fontWeight: '700',
                color: '#4A4A4A',
              }}>
              {this.props.foodName}
            </Text>
          </View>

          {/* [Start Gambar Menu] */}
          {/* <View
            style={{width: '100%', height: '55%', backgroundColor: 'white'}}>
            <Image
              source={this.props.photo}
              resizeMode="cover"
              style={{width: '100%', height: '100%'}}
            />
          </View> */}
          {/* [End Gambar Menu] */}

          {/* [Start harga Menu] */}
          <View style={{height: '20%', justifyContent: 'center'}}>
            <NumberFormat
              value={this.props.price}
              displayType={'text'}
              thousandSeparator={true}
              prefix={'Rp '}
              renderText={(value) => (
                <Text style={{fontSize: RFValue(7), color: '#4A4A4A'}}>
                  {value}
                </Text>
              )}
            />
          </View>
          {/* [End harga Menu] */}

          {/* [Start Stock barang] */}
          <View style={{height: '20%', justifyContent: 'center'}}>
            <Text
              style={{
                fontSize: RFValue(8),
                textAlign: 'center',
                color: '#36B64D',
              }}>
              58 portion available
            </Text>
          </View>
          {/* [End Stock barang] */}

          {/* [Start unique Key] */}
          {/* <View>
            <Text>{this.props.uuid}</Text>
          </View> */}
          {/* [End unique Key] */}
        </TouchableOpacity>
      </View>
    );
  }
}
