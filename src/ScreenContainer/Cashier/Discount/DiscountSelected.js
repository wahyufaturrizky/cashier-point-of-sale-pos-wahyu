/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class DiscountSelected extends Component {
  render() {
    return (
      <View
        style={{
          width: '100%',
          height: undefined,
          aspectRatio: 8,
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          style={{
            width: '100%',
            height: '75%',
            backgroundColor: 'white',
            borderColor: '#DADADA',
            borderWidth: 2,
            paddingHorizontal: '5%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
          onPress={() => this.props.selectItem()}>
          {/* {this.props.statusHotDetail === 'Live' ? (
            <Text style={{fontSize: RFValue(11), color: '#33C15D'}}>PAID</Text>
          ) : (
            <Text style={{fontSize: RFValue(11), color:   '#D0021B'}}>
              UNPAID
            </Text>
          )}
          <Text style={{fontSize: RFValue(10), color: '#6A6A6A'}}>
            {this.props.qtyHotDetail}
          </Text>
          <View>
            <Text style={{fontSize: RFValue(10), color: '#6A6A6A'}}>
              {this.props.namaHotDetail}
            </Text>
            <Text style={{fontSize: RFValue(10), color: '#6A6A6A'}}>
              {this.props.sale_price}
            </Text>
          </View> */}
          <Text style={{fontSize: RFValue(10), color: '#6A6A6A'}}>
            {this.props.discountName}
          </Text>

          {/* <View>
            <Text style={{fontSize: RFValue(10), color: '#6A6A6A'}}>
              {this.props.namaHotDetail}
            </Text>
            <Text style={{fontSize: RFValue(6), color: '#6A6A6A'}}>
              {this.props.priceHotDetail}
            </Text>
          </View>

          <Text style={{fontSize: RFValue(10), color: '#6A6A6A'}}>
            {this.props.statusHotDetail === 'Live' ? 'Cooking' : 'Not Yet'}
          </Text> */}
        </TouchableOpacity>
      </View>
    );
  }
}
