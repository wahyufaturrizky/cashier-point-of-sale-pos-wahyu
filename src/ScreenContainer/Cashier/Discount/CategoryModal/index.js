/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Modal, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import ModalHeader from '../../../View/Component/ModalHeader';
import FoodMenuService from '../../../../services/foodMenu.service';

export default class CategoryModal extends Component {
  constructor(props) {
    super(props);
    this.retrieveCategory = this.retrieveCategory.bind(this);
    this.state = {
      visible: props.visible,
      categories: [],
    };
  }

  async componentDidMount() {
    this.retrieveCategory();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
  }

  retrieveCategory() {
    FoodMenuService.getCategory()
      .then((response) => {
        this.setState({
          categories: response.data.data,
        });
        // console.log('response ini semua kategory menu = ', response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const {categories} = this.state;
    return (
      <Modal visible={this.state.visible} transparent={true}>
        <View
          style={{
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#000000aa',
          }}>
          <View
            style={{
              height: '90%',
              width: '90%',
              backgroundColor: 'white',
              alignItems: 'center',
              borderRadius: RFValue(2),
            }}>
            <View
              style={{
                height: '10%',
                width: '100%',
                shadowOpacity: 1,
                backgroundColor: 'white',
                elevation: 1,
              }}>
              <ModalHeader
                title="CATEGORY"
                close={this.props.categoryModalSwitch}
                save={this.props.categoryModalSwitch}
              />
            </View>

            <View style={{height: '90%', width: '100%'}}>
              <View
                style={{
                  height: '100%',
                  width: '100%',
                  alignItems: 'center',
                  marginVertical: RFValue(15),
                }}>
                <View
                  style={{
                    height: '15%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={{fontSize: RFValue(15)}}>SELECT ORDER TYPE</Text>
                </View>
                <View
                  style={{
                    height: '60%',
                    width: '75%',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                  }}>
                  {/* categories.map((category,index)=>()) */}
                  <TouchableOpacity
                    style={{
                      width: '25%',
                      height: undefined,
                      aspectRatio: 1,
                      borderRadius: 10,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                      shadowOpacity: 1,
                      elevation: 3,
                    }}>
                    <Image
                      source={require('../../../../assets/General/mealIcon.png')}
                      resizeMode="contain"
                      style={{
                        width: '50%',
                        height: undefined,
                        aspectRatio: 1,
                        marginBottom: '10%',
                      }}
                    />
                    <Text style={{fontSize: RFValue(15)}}>MEAL</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: '25%',
                      height: undefined,
                      aspectRatio: 1,
                      borderRadius: 10,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                      shadowOpacity: 1,
                      elevation: 3,
                    }}>
                    <Image
                      source={require('../../../../assets/General/mealIcon.png')}
                      resizeMode="contain"
                      style={{
                        width: '50%',
                        height: undefined,
                        aspectRatio: 1,
                        marginBottom: '10%',
                      }}
                    />
                    <Text style={{fontSize: RFValue(15)}}>BEVERAGE</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: '25%',
                      height: undefined,
                      aspectRatio: 1,
                      borderRadius: 10,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                      shadowOpacity: 1,
                      elevation: 3,
                    }}>
                    <Image
                      source={require('../../../../assets/General/mealIcon.png')}
                      resizeMode="contain"
                      style={{
                        width: '50%',
                        height: undefined,
                        aspectRatio: 1,
                        marginBottom: '10%',
                      }}
                    />
                    <Text style={{fontSize: RFValue(15)}}>SNACK</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
