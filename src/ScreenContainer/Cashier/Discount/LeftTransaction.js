/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {connect} from 'react-redux';
//import {getAllFoodMenu} from '../../../redux/actions/transaction/foodAllMenu';
import DiscountItem from './DiscountItem';
import AddItemModal from '../Transaction/AddItemModal/';
import FoodMenuService from '../../../services/foodMenu.service';
import setActiveFoodMenu from '../../View/Component/Cart/index';
import {Link} from 'react-router-dom';

class LeftTransaction extends Component {
  // componentDidMount() {
  //   this.props.getAllFoodMenu();
  // }
  // renderList() {
  //   return this.props.foodAllMenu.map(allFood => {
  //     return (
  //       <MenuItem
  //         pressAction={this.props.addItemModalSwitch}
  //         foodName={allFood.name}
  //         price={allFood.sale_price}
  //         image={allFood.photo}
  //       />
  //     );
  //   });
  // }
  constructor(props) {
    super(props);
    this.retrieveDiscountMenus = this.retrieveDiscountMenus.bind(this);
    this.retrieveItemModal = this.retrieveItemModal.bind(this);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);

    this.state = {
      visible: false,
      discountMenus: [],
      discountMenuMessage: '',
      foodsMenuFilter: [],
      currentFoodMenu: null,
      currentIndex: -1,
      searchFoodMenu: '',
    };
  }

  async componentDidMount() {
    this.retrieveDiscountMenus();
    this.props.userToken;
  }

  onChangeSearchTitle(text) {
    const searchFoodMenu = text;
    this.setState({
      searchFoodMenu: searchFoodMenu,
    });
  }

  retrieveDiscountMenus() {
    FoodMenuService.getDiscount(this.props.userToken)
      .then((response) => {
        this.setState({
          discountMenus: response.data.data,
          discountMenus: response.data.data,
        });
        // console.log('respone discountMenus', response.data.data.food_discount);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  // For Get All Category Menu
  retrieveItemModal(uuid) {
    FoodMenuService.getItemModal(uuid).then((response) => response.data);
  }

  render() {
    const {discountMenus, currentIndex, foodsMenuFilter} = this.state;
    // foodMenus.map((foodMenu, index) => (
    //   this.props.navigation.navigate("Second", {
    //     uuid: foodMenu.uuid
    //   });
    // );
    return (
      <View style={{flex: 1, backgroundColor: 'white', flexDirection: 'row'}}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#F7F7F7',
            paddingHorizontal: '3.9%',
          }}>
          <View
            style={{
              flexDirection: 'row',
              paddingBottom: 5,
              marginTop: 19,
              height: RFValue(40),
            }}>
            <View style={{width: '100%', marginRight: '3.2%'}}>
              <View
                style={{
                  backgroundColor: 'white',
                  flex: 1,
                  justifyContent: 'center',
                  borderRadius: 5,
                  shadowOpacity: 0.5,
                  shadowRadius: 5,
                  elevation: 3,
                }}>
                <TextInput
                  placeholder="Search..."
                  defaultValue={this.state.searchFoodMenu}
                  style={{fontSize: RFValue(10), justifyContent: 'center'}}
                  inputContainerStyle={{height: 30}}
                  onChangeText={this.onChangeSearchTitle}
                />
              </View>
            </View>
            {/* [START] Button Category Menu */}
            {/* <View
              style={{
                width: '33.5%',
                height: '100%',
                justifyContent: 'center',
                alignContent: 'center',
              }}>
              <TouchableOpacity
                style={{
                  height: '100%',
                  width: '100%',
                  backgroundColor: '#FEBF11',
                  shadowColor: '#000',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  borderRadius: RFValue(3),
                  elevation: 3,
                }}
                onPress={this.props.categoryModalSwitch}>
                <Text
                  style={{
                    alignSelf: 'center',
                    fontSize: RFValue(12),
                    color: '#FFF',
                  }}>
                  Category
                </Text>
              </TouchableOpacity>
            </View> */}
            {/* [END] Button Category Menu */}
          </View>
          <ScrollView>
            <View
              style={{
                width: '100%',
                marginVertical: '2%',
                flexDirection: 'row',
                flexWrap: 'wrap',
              }}>
              {console.log('discountMenus = ', discountMenus)}
              {discountMenus.map((discountMenu, index) => (
                <DiscountItem
                  // onPress = {(event) => {
                  //   {console.log('FOODmENU', foodMenu)}
                  //   uuid=foodMenu.UUID
                  //   {console.log('uuid',uuid)}
                  //   const {navigate} = this.props.navigation;
                  //   navigate('UUID',  {uuid: uuid});
                  // }}
                  className={
                    'list-group-item' + (index === currentIndex ? 'active' : '')
                  }
                  pressActionDiscount={this.props.getAllDataDiscount}
                  discountName={discountMenu.discount_name}
                  discountid={discountMenu.id}
                  discountPrice={discountMenu.amount}
                  onClick={() =>
                    this.props.setActiveFoodMenu(discountMenu, index)
                  }
                  key={index}
                />
              ))}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userToken: state.rightFoodTransaction.tokenUser,
  };
};

export default connect(mapStateToProps)(LeftTransaction);
