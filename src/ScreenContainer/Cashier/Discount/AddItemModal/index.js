/* eslint-disable radix */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  Modal,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import ModalHeader from '../../../View/Component/ModalHeader';
import Modifier from './Modifier';
import DiscountItem from './DiscountItem';

export default class AddItemModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
      uuid: props.uuid,
      quantity: 0,
      takeaway: false,
      modifier: '',
      discount: null,
    };
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
  }
  increment = () => {
    this.setState({
      quantity: parseInt(this.state.quantity + 1),
    });
  };
  decrement = () => {
    this.state.quantity !== 0
      ? this.setState({
          quantity: parseInt(this.state.quantity - 1),
        })
      : null;
  };
  // onModifierSelected(modifier) {
  //   const modifier = modifier;
  //   this.setState({
  //     modifier: modifier,
  //   });
  // }
  // onDiscountSelected(discount) {
  //   const discount = discount;
  //   this.setState({
  //     discount: discount,
  //   });
  // }
  render() {
    return (
      <Modal visible={this.state.visible} transparent={true}>
        <View
          style={{
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#000000aa',
          }}>
          <View
            style={{
              height: '90%',
              width: '90%',
              backgroundColor: 'white',
              alignItems: 'center',
              borderRadius: RFValue(2),
            }}>
            <View
              style={{
                height: '10%',
                width: '100%',
                shadowOpacity: 1,
                backgroundColor: 'white',
                elevation: 1,
              }}>
              <ModalHeader
                title="ADD ITEM"
                close={this.props.addItemModalSwitch}
                save={this.props.addItemModalSwitch}
              />
              {console.log(this.props.save)}
            </View>
            <ScrollView style={{width: '100%', backgroundColor: '#F5F5F5'}}>
              <View
                style={{
                  height: RFValue(40),
                  marginVertical: RFValue(10),
                  paddingHorizontal: '10%',
                  width: '100%',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  backgroundColor: 'white',
                }}>
                <View>
                  <Text>{this.props.foodItem}</Text>
                </View>
                <View
                  style={{
                    height: '40%',
                    width: undefined,
                    aspectRatio: 5,
                    flexDirection: 'row',
                    borderRadius: RFValue(3),
                    backgroundColor: 'white',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    elevation: 3,
                  }}>
                  <TouchableOpacity
                    style={{
                      height: '100%',
                      width: undefined,
                      aspectRatio: 1.5,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRightWidth: 0.5,
                    }}
                    onPress={this.decrement}>
                    <Text
                      style={{
                        color: '#F4282D',
                        fontSize: RFValue(12),
                        fontWeight: 'bold',
                      }}>
                      -
                    </Text>
                  </TouchableOpacity>
                  <View
                    style={{
                      height: '100%',
                      width: undefined,
                      aspectRatio: 2,
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: 'white',
                    }}>
                    <Text style={{fontSize: RFValue(9)}}>
                      {this.state.quantity}
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={{
                      height: '100%',
                      width: undefined,
                      aspectRatio: 1.5,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderLeftWidth: 0.5,
                    }}
                    onPress={this.increment}>
                    <Text
                      style={{
                        color: '#33C15D',
                        fontSize: RFValue(12),
                        fontWeight: 'bold',
                      }}>
                      +
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{width: '100%', backgroundColor: 'white'}}>
                <Modifier />
                <DiscountItem />
                <View
                  style={{
                    width: '100%',
                    borderTopWidth: 0.5,
                    borderColor: '#D8D8D8',
                    alignItems: 'center',
                  }}>
                  <View style={{width: '80%', paddingHorizontal: '3%'}}>
                    <View
                      style={{
                        height: RFValue(25),
                        flexDirection: 'row',
                        alignItems: 'center',
                        borderBottomWidth: 1,
                        borderColor: '#D8D8D8',
                      }}>
                      <Image
                        source={require('../../../../assets/General/edit.png')}
                        resizeMode="contain"
                        style={{
                          height: '50%',
                          width: undefined,
                          aspectRatio: 1,
                        }}
                      />
                      <Text
                        style={{
                          marginLeft: RFValue(10),
                          fontSize: RFValue(12),
                        }}>
                        Add Notes
                      </Text>
                    </View>
                    <View
                      style={{
                        height: RFValue(100),
                        borderBottomWidth: 1,
                        borderColor: '#D8D8D8',
                      }}>
                      <TextInput
                        placeholder="Notes"
                        style={{fontSize: RFValue(12)}}
                        multiline={true}
                      />
                    </View>
                    <View
                      style={{
                        height: RFValue(50),
                        marginTop: RFValue(10),
                        width: '100%',
                        alignItems: 'flex-end',
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({takeaway: !this.state.takeaway})
                        }>
                        {this.state.takeaway ? (
                          <View
                            style={{
                              height: '75%',
                              width: undefined,
                              aspectRatio: 2.5,
                              backgroundColor: '#FEBF11',
                              elevation: 3,
                              alignItems: 'center',
                              justifyContent: 'center',
                            }}>
                            <Text
                              style={{fontSize: RFValue(10), color: 'white'}}>
                              Takeaway
                            </Text>
                          </View>
                        ) : (
                          <View
                            style={{
                              height: '75%',
                              width: undefined,
                              aspectRatio: 2.5,
                              backgroundColor: 'white',
                              elevation: 3,
                              alignItems: 'center',
                              justifyContent: 'center',
                            }}>
                            <Text style={{fontSize: RFValue(10)}}>
                              Takeaway
                            </Text>
                          </View>
                        )}
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}
