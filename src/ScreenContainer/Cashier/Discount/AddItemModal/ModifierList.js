/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import NumberFormat from 'react-number-format';

export default class ModifierList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: false,
    };
  }
  render() {
    return (
      <View
        style={{
          width: '25%',
          height: undefined,
          aspectRatio: 2.5,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          style={{height: '90%', width: '90%'}}
          onPress={() => this.setState({selected: !this.state.selected})}>
          {this.state.selected ? (
            <View
              style={{
                height: '90%',
                backgroundColor: '#FEBF11',
                borderRadius: RFValue(3),
                justifyContent: 'center',
                alignItems: 'center',
                elevation: 3,
              }}>
              <Text style={{fontSize: RFValue(8), color: 'white'}}>
                {this.props.name}
              </Text>
              <NumberFormat
                value={this.props.value}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp. '}
                renderText={(value) => (
                  <Text style={{color: 'white', fontSize: RFValue(7)}}>
                    {value}
                  </Text>
                )}
              />
            </View>
          ) : (
            <View
              style={{
                height: '90%',
                backgroundColor: 'white',
                borderRadius: RFValue(3),
                justifyContent: 'center',
                alignItems: 'center',
                elevation: 3,
              }}>
              <Text style={{fontSize: RFValue(8)}}>{this.props.name}</Text>
              <NumberFormat
                value={this.props.value}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp. '}
                renderText={(value) => (
                  <Text style={{fontSize: RFValue(7), color: '#FEBF11'}}>
                    {value}
                  </Text>
                )}
              />
            </View>
          )}
        </TouchableOpacity>
      </View>
    );
  }
}
