/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import DiscountItemList from './DiscountItemList';

export default class DiscountTime extends Component {
  render() {
    return (
      <View
        style={{
          width: '100%',
          alignItems: 'center',
          marginBottom: RFValue(20),
          paddingHorizontal: '10%',
        }}>
        <Text style={{fontSize: RFValue(12), marginBottom: RFValue(5)}}>
          {' '}
          SELECT DISCOUNT{' '}
        </Text>
        <View
          style={{
            width: '100%',
            backgroundColor: 'white',
            flexDirection: 'row',
            flexWrap: 'wrap',
          }}>
          <DiscountItemList name="Disc Pelajar" value={30} />
          <DiscountItemList name="Disc Mahasiswa" value={20} />
          <DiscountItemList name="Disc Pelajar" value={30} />
          <DiscountItemList name="Disc Mahasiswa" value={20} />
        </View>
      </View>
    );
  }
}
