/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import ModifierList from './ModifierList';
import FoodMenuService from '../../../../services/foodMenu.service';
export default class Modifier extends Component {
  constructor(props) {
    super(props);
    //this.retrieveFoodMenus = this.retrieveFoodMenus.bind(this);
    this.retrieveItemModal = this.retrieveItemModal.bind(this);
    //this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);

    this.state = {
      visible: false,
      foodMenus: [],
      foodModifiers: [],
      currentFoodMenu: null,
      currentIndex: -1,
      searchFoodMenu: '',
    };
  }

  componentDidMount() {
    this.retrieveItemModal();
  }

  retrieveItemModal(uuid) {
    FoodMenuService.getItemModal(uuid)
      .then((response) => {
        this.setState({
          foodMenus: response.data.data,
        });
        console.log(
          'ItemModalfdhsgeighhdkjghkjdhfsfkljljdaklkhklkl',
          response.data.data,
        );
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const {foodMenus, foodModifiers} = this.state;
    {
      console.log('FOODmENU', foodMenus);
    }
    return (
      <View
        style={{
          width: '100%',
          alignItems: 'center',
          marginBottom: RFValue(20),
          paddingHorizontal: '10%',
        }}>
        <Text style={{fontSize: RFValue(12), marginBottom: RFValue(5)}}>
          {' '}
          SELECT MODIFIER{' '}
        </Text>
        <View
          style={{
            width: '100%',
            backgroundColor: 'white',
            flexDirection: 'row',
            flexWrap: 'wrap',
          }}>
          {console.log('Halo', foodMenus)}
          {foodMenus.map((foodMenu, index) => (
            <ModifierList
              name={foodMenu.modifier.name}
              value={foodMenu.modifier.price}
            />
          ))}
        </View>
      </View>
    );
  }
}
