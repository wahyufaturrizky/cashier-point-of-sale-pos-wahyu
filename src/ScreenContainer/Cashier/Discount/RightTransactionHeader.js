/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Button,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class RightTransactionHeader extends Component {
  render() {
    return (
      <View
        style={{
          height: '80%',
          flexDirection: 'row',
          justifyContent: 'space-between',
          top: '1%',
        }}>
        <TouchableOpacity
          style={{width: '32%'}}
          onPress={this.props.customerDetailDineInModalSwitch}>
          {this.props.isCustomerDetailDineIn ? (
            <View style={styles.buttonStyleActiveDineIn}>
              {/* <View style={styles.imageStyle}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/Cashier/Transaction/DineInActive.png')}
                  resizeMode="contain"
                />
              </View> */}
              <Text style={styles.textStyleActiveDineIn}>DINE IN</Text>
            </View>
          ) : (
            <View style={styles.buttonStyleInactiveDineIn}>
              {/* <View style={styles.imageStyle}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/Cashier/Transaction/DineInInactive.png')}
                  resizeMode="contain"
                />
              </View> */}
              <Text style={styles.textStyleInactiveDineIn}>DINE IN</Text>
            </View>
          )}
        </TouchableOpacity>

        <TouchableOpacity
          style={{width: '32%'}}
          onPress={this.props.takeawayModalSwitch}>
          {this.props.isTakeaway ? (
            <View style={styles.buttonStyleActiveTakeAway}>
              {/* <View style={styles.imageStyle}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/Cashier/Transaction/TakeawayActive.png')}
                  resizeMode="contain"
                />
              </View> */}
              <Text style={styles.textStyleActiveTakeAway}>TAKE AWAY</Text>
            </View>
          ) : (
            <View style={styles.buttonStyleInactiveTakeAway}>
              {/* <View style={styles.imageStyle}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/Cashier/Transaction/TakeawayInactive.png')}
                  resizeMode="contain"
                />
              </View> */}
              <Text style={styles.textStyleInactiveTakeAway}>TAKE AWAY</Text>
            </View>
          )}
        </TouchableOpacity>

        <TouchableOpacity
          style={{width: '32%'}}
          onPress={this.props.deliveryModalSwitch}>
          {this.props.isDelivery ? (
            <View style={styles.buttonStyleActiveDelivery}>
              {/* <View style={styles.imageStyle}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/Cashier/Transaction/DeliveryActive.png')}
                  resizeMode="contain"
                />
              </View> */}
              <Text style={styles.textStyleActiveDelivery}>DELIVERY</Text>
            </View>
          ) : (
            <View style={styles.buttonStyleInactiveDelivery}>
              {/* <View style={styles.imageStyle}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/Cashier/Transaction/DeliveryInactive.png')}
                  resizeMode="contain"
                />
              </View> */}
              <Text style={styles.textStyleInactiveDelivery}>DELIVERY</Text>
            </View>
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  // buttonStyleInactive: {
  //   height: '100%',
  //   paddingHorizontal: '5%',
  //   alignItems: 'center',
  //   flexDirection: 'row',
  //   borderRadius: 5,
  //   backgroundColor: 'white',
  //   borderColor: '#DADADA',
  //   shadowOpacity: 0.5,
  //   shadowRadius: 5,
  //   elevation: 5,
  //   backgroundColor: 'white',
  // },
  // buttonStyleActive: {
  //   height: '100%',
  //   paddingHorizontal: '5%',
  //   alignItems: 'center',
  //   flexDirection: 'row',
  //   borderRadius: 5,
  //   backgroundColor: 'white',
  //   borderColor: '#DADADA',
  //   shadowOpacity: 0.5,
  //   shadowRadius: 5,
  //   elevation: 5,
  //   backgroundColor: '#FE724C',
  // },
  buttonStyleInactiveDineIn: {
    height: '100%',
    paddingHorizontal: '5%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5,
    backgroundColor: '#EB5757',
  },
  buttonStyleActiveDineIn: {
    height: '100%',
    paddingHorizontal: '5%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    borderColor: '#EB5757',
    borderWidth: 2,
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5,
  },
  buttonStyleInactiveTakeAway: {
    height: '100%',
    paddingHorizontal: '5%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5,
    backgroundColor: '#6D91F0',
  },
  buttonStyleActiveTakeAway: {
    height: '100%',
    paddingHorizontal: '5%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    borderColor: '#6D91F0',
    borderWidth: 2,
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5,
  },
  buttonStyleInactiveDelivery: {
    height: '100%',
    paddingHorizontal: '5%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5,
    backgroundColor: '#33C15D',
  },
  buttonStyleActiveDelivery: {
    height: '100%',
    paddingHorizontal: '5%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    borderColor: '#33C15D',
    borderWidth: 2,
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5,
  },
  imageStyle: {
    width: '30%',
    height: undefined,
    aspectRatio: 1,
    marginHorizontal: '5%',
  },
  icon: {
    width: '100%',
    height: undefined,
    aspectRatio: 1,
  },
  textStyleInactiveDineIn: {
    fontSize: RFValue(8),
    color: 'white',
  },
  textStyleActiveDineIn: {
    fontSize: RFValue(8),
    color: '#EB5757',
  },
  textStyleInactiveTakeAway: {
    fontSize: RFValue(8),
    color: 'white',
  },
  textStyleActiveTakeAway: {
    fontSize: RFValue(8),
    color: '#6D91F0',
  },
  textStyleInactiveDelivery: {
    fontSize: RFValue(8),
    color: 'white',
  },
  textStyleActiveDelivery: {
    fontSize: RFValue(8),
    color: '#33C15D',
  },
  // textStyleActive: {
  //   fontSize: RFValue(8),
  //   color: 'white',
  // },
  // textStyleInactive: {
  //   fontSize: RFValue(8),
  // },
});
