/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import CashierNavigator from '../../CashierNavigator';
import NumberFormat from 'react-number-format';
import {RFValue} from 'react-native-responsive-fontsize';

import CashierMiniHeader from '../../CashierMiniHeader';

export default class SplitPayment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentMethodSelect: '',
      payAmount: 0,
      totalAmount: 100000,
    };
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          flexDirection: 'row',
          backgroundColor: '#F7F7F7',
        }}>
        <View style={{flex: 85}}>
          <CashierNavigator
            cartStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        {console.log(this.state.payAmount)}
        <View style={{flex: 1195, flexDirection: 'row'}}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'space-around',
            }}>
            <View
              style={{
                width: '100%',
                height: undefined,
                aspectRatio: 2,
                alignItems: 'center',
                justifyContent: 'space-around',
              }}>
              <View
                style={{
                  width: '60%',
                  height: undefined,
                  aspectRatio: 2,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: RFValue(8), color: '#C5C5C5'}}>
                  TOTAL TAGIHAN
                </Text>
                <NumberFormat
                  value={this.state.totalAmount}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp. '}
                  renderText={(value) => (
                    <Text style={{color: '#6A6A6A', fontSize: RFValue(20)}}>
                      {value}
                    </Text>
                  )}
                />
              </View>
              <View
                style={{
                  width: '60%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text style={{color: '#FD734C', fontSize: RFValue(10)}}>
                  Amount Left
                </Text>
                <NumberFormat
                  value={this.state.totalAmount - this.state.payAmount}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp. '}
                  renderText={(value) => (
                    <Text
                      style={{
                        color: '#6A6A6A',
                        fontSize: RFValue(10),
                        color: '#FD734C',
                      }}>
                      {value}
                    </Text>
                  )}
                />
              </View>
              <View
                style={{
                  width: '60%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text style={{fontSize: RFValue(10)}}>Pay Amount</Text>
                <NumberFormat
                  value={this.state.payAmount}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp. '}
                  renderText={(value) => (
                    <Text style={{color: '#6A6A6A', fontSize: RFValue(10)}}>
                      {value}
                    </Text>
                  )}
                />
              </View>
            </View>
            <View
              style={{
                shadowColor: '#707070',
                alignItems: 'center',
                justifyContent: 'center',
                width: '60%',
                height: undefined,
                aspectRatio: 8,
                backgroundColor: 'white',
              }}>
              <TextInput
                placeholder="Input Amount"
                style={{
                  fontSize: RFValue(9),
                  color: 'black',
                  alignItems: 'center',
                  paddingVertical: 0,
                }}
                keyboardType="number-pad"
                onChangeText={(input) => this.setState({payAmount: input})}
              />
            </View>
          </View>
          <View style={{flex: 1, backgroundColor: 'white'}}>
            <View
              style={{
                flex: 65,
                paddingVertical: 10,
                borderBottomWidth: 0.5,
                borderColor: '#C5C5C5',
              }}>
              <CashierMiniHeader
                title="PAYMENT METHOD"
                navigation={this.props.navigation}
              />
            </View>
            <View style={{flex: 735}}>
              <ScrollView style={{flex: 1}}>
                <View
                  style={{
                    paddingHorizontal: '10%',
                    marginTop: RFValue(40),
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.state.paymentMethodSelect == 'cash'
                        ? this.setState({paymentMethodSelect: ''})
                        : this.setState({paymentMethodSelect: 'cash'});
                    }}>
                    {this.state.paymentMethodSelect == 'cash' ? (
                      <View
                        style={{
                          width: '100%',
                          height: undefined,
                          aspectRatio: 7.35,
                          borderRadius: RFValue(2),
                          backgroundColor: 'white',
                          elevation: 2,
                          shadowOpacity: 1,
                          justifyContent: 'center',
                          borderWidth: 3,
                          borderColor: '#FD734C',
                          alignItems: 'center',
                        }}>
                        <Text>CASH</Text>
                      </View>
                    ) : (
                      <View
                        style={{
                          width: '100%',
                          height: undefined,
                          aspectRatio: 7.35,
                          borderRadius: RFValue(2),
                          backgroundColor: 'white',
                          elevation: 2,
                          shadowOpacity: 1,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text>CASH</Text>
                      </View>
                    )}
                  </TouchableOpacity>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      marginTop: RFValue(8),
                      justifyContent: 'space-between',
                      flexWrap: 'wrap',
                    }}>
                    <TouchableOpacity
                      style={{
                        width: '30%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      onPress={() => {
                        this.state.paymentMethodSelect == 'ovo'
                          ? this.setState({paymentMethodSelect: ''})
                          : this.setState({paymentMethodSelect: 'ovo'});
                      }}>
                      {this.state.paymentMethodSelect == 'ovo' ? (
                        <Image
                          source={require('../../../../assets/Cashier/Transaction/Payment/ovo.png')}
                          resizeMode="contain"
                          style={{
                            width: '100%',
                            height: undefined,
                            aspectRatio: 2.45,
                            borderWidth: 3,
                            borderColor: '#FD734C',
                          }}
                        />
                      ) : (
                        <Image
                          source={require('../../../../assets/Cashier/Transaction/Payment/ovo.png')}
                          resizeMode="contain"
                          style={{
                            width: '100%',
                            height: undefined,
                            aspectRatio: 2.45,
                          }}
                        />
                      )}
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        width: '30%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      onPress={() => {
                        this.state.paymentMethodSelect == 'link aja'
                          ? this.setState({paymentMethodSelect: ''})
                          : this.setState({paymentMethodSelect: 'link aja'});
                      }}>
                      {this.state.paymentMethodSelect == 'link aja' ? (
                        <Image
                          source={require('../../../../assets/Cashier/Transaction/Payment/linkaja.png')}
                          resizeMode="contain"
                          style={{
                            width: '100%',
                            height: undefined,
                            aspectRatio: 2.45,
                            borderWidth: 3,
                            borderColor: '#FD734C',
                          }}
                        />
                      ) : (
                        <Image
                          source={require('../../../../assets/Cashier/Transaction/Payment/linkaja.png')}
                          resizeMode="contain"
                          style={{
                            width: '100%',
                            height: undefined,
                            aspectRatio: 2.45,
                          }}
                        />
                      )}
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        width: '30%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      onPress={() => {
                        this.state.paymentMethodSelect == 'dana'
                          ? this.setState({paymentMethodSelect: ''})
                          : this.setState({paymentMethodSelect: 'dana'});
                      }}>
                      {this.state.paymentMethodSelect == 'dana' ? (
                        <Image
                          source={require('../../../../assets/Cashier/Transaction/Payment/dana.png')}
                          resizeMode="contain"
                          style={{
                            width: '100%',
                            height: undefined,
                            aspectRatio: 2.45,
                            borderWidth: 3,
                            borderColor: '#FD734C',
                          }}
                        />
                      ) : (
                        <Image
                          source={require('../../../../assets/Cashier/Transaction/Payment/dana.png')}
                          resizeMode="contain"
                          style={{
                            width: '100%',
                            height: undefined,
                            aspectRatio: 2.45,
                          }}
                        />
                      )}
                    </TouchableOpacity>
                  </View>
                </View>
                <View
                  style={{
                    width: '100%',
                    alignItems: 'center',
                    marginTop: RFValue(100),
                    paddingHorizontal: '10%',
                    paddingBottom: RFValue(50),
                  }}>
                  {this.payButton()}
                </View>
              </ScrollView>
            </View>
          </View>
        </View>
      </View>
    );
  }
  payButton = () => {
    switch (
      this.state.payAmount > 0 &&
      this.state.totalAmount >= 0 &&
      this.state.paymentMethodSelect.trim() != ''
    ) {
      case true:
        switch (parseInt(this.state.totalAmount - this.state.payAmount) == 0) {
          case true:
            return (
              <TouchableOpacity onPress={() => this.navigateToHome()}>
                <View
                  style={{
                    width: '100%',
                    height: undefined,
                    aspectRatio: 10,
                    borderRadius: RFValue(2),
                    backgroundColor: '#FEBF11',
                    elevation: 2,
                    shadowOpacity: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{color: 'white', fontSize: RFValue(10)}}>
                    PAY
                  </Text>
                </View>
              </TouchableOpacity>
            );
          case false:
            return (
              <TouchableOpacity onPress={this.payPartial}>
                <View
                  style={{
                    width: '100%',
                    height: undefined,
                    aspectRatio: 10,
                    borderRadius: RFValue(2),
                    backgroundColor: '#FEBF11',
                    elevation: 2,
                    shadowOpacity: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{color: 'white', fontSize: RFValue(10)}}>
                    PAY
                  </Text>
                </View>
              </TouchableOpacity>
            );
        }
      case false:
        return <View />;
    }
  };
  navigateToHome = () => {
    this.props.navigation.navigate('Transaction');
  };
  payPartial = () => {
    this.setState({totalAmount: this.state.totalAmount - this.state.payAmount});
  };
}
