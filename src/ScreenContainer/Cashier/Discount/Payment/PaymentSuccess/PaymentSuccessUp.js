import React, {Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

class PaymentSuccessUp extends Component {
  render() {
    return (
      <View>
        <View style={styles.imageWrapper}>
          <Image
            source={require('../../../../../assets/Cashier/Transaction/Payment/Succeed.png')}
          />
        </View>
        <View style={styles.text1Wrapper}>
          <Text style={styles.text1}>Transaki Berhasil</Text>
        </View>
        <View style={styles.text2Wrapper}>
          <Text style={styles.text2}>Uang Kembali</Text>
        </View>
        <View style={styles.text3Wrapper}>
          <Text style={styles.text3}>Rp 11.600</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageWrapper: {
    marginTop: RFValue(20),
    justifyContent: 'center',
    alignItems: 'center',
  },
  text1Wrapper: {
    marginTop: RFValue(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  text2Wrapper: {
    marginTop: RFValue(8),
    justifyContent: 'center',
    alignItems: 'center',
  },
  text3Wrapper: {
    marginTop: RFValue(8),
    justifyContent: 'center',
    alignItems: 'center',
  },
  text1: {
    fontSize: RFValue(11),
    color: '#CBCBCB',
  },
  text2: {
    fontSize: RFValue(14),
    color: '#6A6A6A',
  },
  text3: {
    fontSize: RFValue(20),
    color: '#6A6A6A',
  },
});

export default PaymentSuccessUp;
