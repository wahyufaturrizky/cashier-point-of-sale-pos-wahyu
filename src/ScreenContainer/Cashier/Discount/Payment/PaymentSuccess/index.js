/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import PaymentSuccessUp from './PaymentSuccessUp';
import PaymentSuccessDown from './PaymentSuccessDown';
import CashierNavigator from '../../../CashierNavigator';

class PaymentSuccess extends Component {
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'row', height: '100%'}}>
        <View style={{flex: 45}}>
          <CashierNavigator
            runningOrderStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View style={{flex: 650}}>
          <View>
            <PaymentSuccessUp />
          </View>
          <View>
            <PaymentSuccessDown />
            <View>
              <TouchableOpacity
                style={{
                  width: '50%',
                  height: undefined,
                  aspectRatio: 10,
                  backgroundColor: '#FEBF11',
                  borderRadius: 6.6,
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                }}
                onPress={() => {
                  this.props.navigation.navigate('Transaction');
                }}>
                <Text style={{color: 'white', fontSize: RFValue(10)}}>
                  Done
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default PaymentSuccess;
