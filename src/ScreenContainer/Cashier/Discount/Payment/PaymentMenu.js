/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, TextInput, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {ScrollView} from 'react-native-gesture-handler';

export default class PaymentMenu extends Component {
  render() {
    return (
      <ScrollView
        showsVerticalScrollIndicator={true}
        style={{paddingHorizontal: '15%'}}>
        <View
          style={{
            height: RFValue(25),
            marginTop: RFValue(8),
            borderWidth: 1.1,
            borderRadius: 4.4,
            borderColor: '#E9E9E9',
            width: '100%',
            justifyContent: 'center',
          }}>
          <TextInput
            style={{alignSelf: 'center', fontSize: RFValue(10)}}
            placeholder="Tunai"
            keyboardType="numeric"
          />
        </View>
        <TouchableOpacity
          style={{
            height: RFValue(25),
            width: '100%',
            borderWidth: 1.1,
            borderRadius: 6.6,
            borderColor: '#E9E9E9',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#FEBF11',
            marginTop: RFValue(8),
          }}>
          <Text style={{color: 'white', fontSize: RFValue(10)}}>UANG PAS</Text>
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            marginTop: RFValue(8),
            height: RFValue(25),
          }}>
          <TouchableOpacity
            style={{
              height: RFValue(25),
              width: '100%',
              borderWidth: 1.1,
              borderRadius: 6.6,
              borderColor: '#E9E9E9',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: RFValue(8),
              backgroundColor: '#707070',
            }}
            onPress={this.props.discount}>
            <Text style={{color: 'white', fontSize: RFValue(10)}}>
              Discount Option
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            marginTop: RFValue(8),
            justifyContent: 'space-between',
            flexWrap: 'wrap',
          }}>
          <TouchableOpacity
            style={{
              width: '30%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../../../../assets/Cashier/Transaction/Payment/ovo.png')}
              resizeMode="contain"
              style={{width: '100%', height: undefined, aspectRatio: 2.45}}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '30%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../../../../assets/Cashier/Transaction/Payment/linkaja.png')}
              resizeMode="contain"
              style={{width: '100%', height: undefined, aspectRatio: 2.45}}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '30%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../../../../assets/Cashier/Transaction/Payment/dana.png')}
              resizeMode="contain"
              style={{width: '100%', height: undefined, aspectRatio: 2.45}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            marginTop: RFValue(50),
            height: RFValue(25),
          }}>
          <TouchableOpacity
            style={{
              height: '100%',
              width: '50%',
              borderWidth: 1.1,
              borderRadius: 5,
              borderColor: '#E9E9E9',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#707070',
            }}>
            <Text style={{fontSize: RFValue(10), color: 'white'}}>
              PRINT BILL
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              height: '100%',
              width: '50%',
              borderWidth: 1.1,
              borderRadius: 5,
              borderColor: '#E9E9E9',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#707070',
            }}
            onPress={this.props.switchModal}>
            <Text style={{fontSize: RFValue(10), color: 'white'}}>
              SPLIT METHOD
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            marginTop: RFValue(5),
            height: RFValue(25),
          }}>
          <TouchableOpacity
            style={{
              height: '100%',
              width: '100%',
              borderWidth: 1.1,
              borderRadius: 5,
              borderColor: '#E9E9E9',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#FEBF11',
            }}
            onPress={() => {
              this.props.navigation.navigate('PaymentSuccess');
            }}>
            <Text style={{fontSize: RFValue(10), color: 'white'}}>PAY</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
