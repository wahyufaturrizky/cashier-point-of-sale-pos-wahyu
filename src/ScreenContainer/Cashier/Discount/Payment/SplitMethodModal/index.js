import React, {Component} from 'react';
import {Text, View, Modal, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import ModalHeader from '../../../../View/Component/ModalHeader';

import SplitBill from './SplitBill';
import SplitPayment from './SplitPayment';

export default class DineInModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
      splitPaymentSelected: props.splitPaymentSelected,
      splitBillSelected: props.splitPaymentSelected,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
    if (nextProps.splitPaymentSelected && !this.props.splitPaymentSelected) {
      this.setState({
        splitPaymentSelected: nextProps.splitPaymentSelected,
      });
    } else {
      this.setState({
        splitPaymentSelected: nextProps.splitPaymentSelected,
      });
    }
    if (nextProps.splitBillSelected && !this.props.splitBillSelected) {
      this.setState({
        splitBillSelected: nextProps.splitBillSelected,
      });
    } else {
      this.setState({
        splitBillSelected: nextProps.splitBillSelected,
      });
    }
  }
  render() {
    return (
      <Modal visible={this.state.visible} transparent={true}>
        <View
          style={{
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#000000aa',
          }}>
          <View
            style={{
              height: '90%',
              width: '90%',
              backgroundColor: 'white',
              alignItems: 'center',
              borderRadius: RFValue(2),
            }}>
            <View
              style={{
                height: '10%',
                width: '100%',
                shadowOpacity: 1,
                backgroundColor: 'white',
                elevation: 1,
              }}>
              <ModalHeader
                title="SPLIT METHOD"
                close={this.props.switchModal}
              />
            </View>

            <View
              style={{
                height: '90%',
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                paddingHorizontal: '15%',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'space-between',
                }}>
                <SplitPayment
                  selected={this.state.splitPaymentSelected}
                  selection={this.props.toggleSplitPayment}
                />
                <SplitBill
                  selected={this.state.splitBillSelected}
                  selection={this.props.toggleBillPayment}
                />
              </View>
              <View
                style={{
                  position: 'absolute',
                  alignSelf: 'center',
                  width: 0.5,
                  height: '100%',
                  backgroundColor: '#D8D8D8',
                }}
              />
              {this.state.splitBillSelected ||
              this.state.splitPaymentSelected ? (
                <TouchableOpacity
                  style={{
                    height: '10%',
                    width: '100%',
                    borderRadius: RFValue(5),
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#FEBF11',
                    marginTop: RFValue(20),
                  }}
                  onPress={this.props.navigate}>
                  <Text style={{fontSize: RFValue(12), color: 'white'}}>
                    DONE
                  </Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={{
                    height: '10%',
                    width: '100%',
                    borderRadius: RFValue(5),
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#878787',
                    marginTop: RFValue(20),
                  }}
                  onPress="">
                  <Text style={{fontSize: RFValue(12), color: 'white'}}>
                    DONE
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
