/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {ScrollView} from 'react-native-gesture-handler';

import CashierNavigator from '../../CashierNavigator';
import PaymentMenu from './PaymentMenu';
import DiscountMenu from './DiscountMenu';
import TaxMenu from './TaxMenu';
import InvoiceList from '../../../View/Component/InvoiceList';
import InvoiceBill from '../../../View/Component/InvoiceBill';
import Cart from '../../../View/Component/Cart';

import SplitMethodModal from './SplitMethodModal';

export default class Payment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      splitMethodModalEnabled: false,
      rightPayment: 'menu',
      splitPaymentSelected: false,
      splitBillSelected: false,
    };
  }
  rightPayment = (options) => {
    switch (options) {
      case 'menu':
        return (
          <PaymentMenu
            switchModal={this.splitMethodModalSwitch}
            discount={this.switchDiscount}
            tax={this.switchTax}
            navigation={this.props.navigation}
          />
        );
      case 'discount':
        return <DiscountMenu done={this.switchMenu} />;
      case 'tax':
        return <TaxMenu done={this.switchMenu} />;
    }
  };
  switchMenu = () => {
    this.setState({rightPayment: 'menu'});
  };
  switchDiscount = () => {
    this.setState({rightPayment: 'discount'});
  };
  switchTax = () => {
    this.setState({rightPayment: 'tax'});
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          flexDirection: 'row',
          backgroundColor: '#F7F7F7',
        }}>
        <View style={{flex: 85}}>
          <CashierNavigator
            cartStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View
          style={{
            height: '100%',
            flex: 556,
            marginHorizontal: '1%',
            justifyContent: 'space-between',
          }}>
          <View style={{height: '15%', justifyContent: 'center'}}>
            <View
              style={{
                height: '80%',
                width: '100%',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderRadius: 1,
                flexDirection: 'row',
              }}>
              <View
                style={{
                  backgroundColor: 'white',
                  height: '100%',
                  width: '31%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: 1,
                  borderRadius: 5,
                  borderColor: '#DADADA',
                }}>
                <Text style={{fontSize: RFValue(10), color: '#CBCBCB'}}>
                  Order Type:
                </Text>
                <Text style={{fontSize: RFValue(15), color: '#4A4A4A'}}>
                  Dine In
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: 'white',
                  height: '100%',
                  width: '31%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: 1,
                  borderRadius: 5,
                  borderColor: '#DADADA',
                }}>
                <Text style={{fontSize: RFValue(10), color: '#CBCBCB'}}>
                  Customer Name:
                </Text>
                <Text style={{fontSize: RFValue(15), color: '#4A4A4A'}}>
                  Bilal
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: 'white',
                  height: '100%',
                  width: '31%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: 1,
                  borderRadius: 5,
                  borderColor: '#DADADA',
                }}>
                <Text style={{fontSize: RFValue(10), color: '#CBCBCB'}}>
                  Table Number:
                </Text>
                <Text style={{fontSize: RFValue(15), color: '#4A4A4A'}}>5</Text>
              </View>
            </View>
          </View>
          <View style={{height: '85%', justifyContent: 'space-between'}}>
            <View style={{height: '65%'}}>
              <ScrollView>
                <Cart isPayment={true} />
              </ScrollView>
            </View>
            <View
              style={{
                height: '30%',
                justifyContent: 'center',
                paddingHorizontal: 15,
              }}>
              <InvoiceList title="Sub Total" amount={20000} />
              <InvoiceList title="Tax / Service" amount={2000} />
              <InvoiceList title="Discount" amount={11000} />
              <InvoiceBill title="Total" amount={11100} />
            </View>
          </View>
        </View>
        <View
          style={{
            flex: 641,
            height: '100%',
            backgroundColor: 'white',
            paddingTop: 17.5,
          }}>
          <View
            style={{flex: 65, borderColor: '#C5C5C5', borderBottomWidth: 0.5}}>
            <View
              style={{
                flexDirection: 'row',
                flex: 70,
                justifyContent: 'space-between',
              }}>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingLeft: RFValue(15),
                }}
                onPress={() => {
                  this.props.navigation.goBack();
                }}>
                <Image
                  source={require('../../../../assets/General/back.png')}
                  resizeMode="contain"
                  style={{
                    width: undefined,
                    height: '100%',
                    aspectRatio: 1,
                    marginEnd: RFValue(3),
                  }}
                />
                <Text style={{fontSize: RFValue(10)}}>Back</Text>
              </TouchableOpacity>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={{fontSize: RFValue(18)}}>Payment</Text>
              </View>
              <View style={{width: '20%'}} />
            </View>
            <View
              style={{
                flex: 735,
                paddingHorizontal: RFValue(20),
                paddingTop: '10%',
              }}>
              <View
                style={{
                  height: '10%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: 25,
                }}>
                <Text style={{fontSize: RFValue(12), color: '#C5C5C5'}}>
                  TOTAL TAGIHAN
                </Text>
                <Text
                  style={{
                    fontSize: RFValue(15),
                    color: '#4A4A4A',
                    marginTop: 3,
                  }}>
                  Rp 50.000
                </Text>
              </View>

              {this.rightPayment(this.state.rightPayment)}
            </View>
          </View>
        </View>
        <SplitMethodModal
          visible={this.state.splitMethodModalEnabled}
          switchModal={this.splitMethodModalSwitch}
          navigate={this.splitMethodNavigate}
          splitPaymentSelected={this.state.splitPaymentSelected}
          splitBillSelected={this.state.splitBillSelected}
          toggleSplitPayment={this.toggleSplitPayment}
          toggleBillPayment={this.toggleBillPayment}
        />
      </View>
    );
  }
  splitMethodModalSwitch = () => {
    this.state.splitMethodModalEnabled
      ? this.setState({splitMethodModalEnabled: false})
      : this.setState({splitMethodModalEnabled: true});
  };
  splitMethodNavigate = () => {
    this.state.splitPaymentSelected
      ? this.props.navigation.navigate('SplitPayment') &&
        this.splitMethodModalSwitch()
      : this.props.navigation.navigate('SplitBill') &&
        this.splitMethodModalSwitch();
  };
  toggleSplitPayment = () => {
    this.state.splitPaymentSelected
      ? this.setState({splitPaymentSelected: false})
      : [
          this.state.splitBillSelected
            ? this.setState({
                splitPaymentSelected: true,
                splitBillSelected: false,
              })
            : this.setState({splitPaymentSelected: true}),
        ];
  };
  toggleBillPayment = () => {
    this.state.splitBillSelected
      ? this.setState({splitBillSelected: false})
      : [
          this.state.splitPaymentSelected
            ? this.setState({
                splitBillSelected: true,
                splitPaymentSelected: false,
              })
            : this.setState({splitBillSelected: true}),
        ];
  };
}
