/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class Floor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: false,
    };
  }
  toggleSelect = () => {
    this.setState({selected: !this.state.selected});
  };
  render() {
    return (
      <TouchableOpacity
        style={{
          width: '18%',
          marginHorizontal: '1%',
          marginBottom: RFValue(10),
          shadowOpacity: 1,
          elevation: 2,
          height: undefined,
          aspectRatio: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white',
          elevation: 2,
        }}
        onPress={this.toggleSelect}>
        {this.state.selected ? (
          <View
            style={{
              alignItems: 'center',
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              backgroundColor: '#FEBF11',
            }}>
            <Image
              source={require('../../../../assets/General/FloorUncolored.png')}
              style={{height: '60%', width: undefined, aspectRatio: 1}}
              resizeMode="contain"
            />
            <Text style={{fontSize: RFValue(8), marginTop: 8}}>Floor 1</Text>
            <Text style={{fontSize: RFValue(6)}}>15 Table</Text>
          </View>
        ) : (
          <View
            style={{
              alignItems: 'center',
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              backgroundColor: 'white',
            }}>
            <Image
              source={require('../../../../assets/General/FloorColored.png')}
              style={{height: '60%', width: undefined, aspectRatio: 1}}
              resizeMode="contain"
            />
            <Text style={{fontSize: RFValue(8), marginTop: 8}}>Floor 1</Text>
            <Text style={{fontSize: RFValue(6)}}>15 Table</Text>
          </View>
        )}
      </TouchableOpacity>
    );
  }
}
