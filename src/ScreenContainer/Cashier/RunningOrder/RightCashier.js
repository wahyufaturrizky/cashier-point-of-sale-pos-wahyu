/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, ScrollView, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import runningOderService from '../../../services/runningOrder.service';
import RunningOrderHotDetail from './RunningOrderHotDetail';

//import KitchenStatus from './KitchenStatus/KitchenStatus';
import EditOrder from './EditOrder';
import Cart from '../../View/Component/Cart';
import Button from './Button';
import CancelModal from './CancelModal';
import {connect} from 'react-redux';
import {set} from 'react-native-reanimated';

class RightCashier extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: '10:00',
      cancelModalVisible: false,
    };
  }

  cancelModalSwitch = () => {
    this.state.cancelModalVisible
      ? this.setState({cancelModalVisible: false})
      : this.setState({cancelModalVisible: true});
  };
  render() {
    return (
      <View>
        <View
          style={{
            height: '11%',
            backgroundColor: '#FFFFFF',
            marginBottom: '1.25%',
            justifyContent: 'center',
            paddingHorizontal: 23,
          }}>
          <View
            style={{
              height: '90%',
              borderRadius: 4,
              borderWidth: 0.5,
              borderColor: '#C5C5C5',
              paddingHorizontal: '3.14%',
              paddingVertical: 12,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View>
              <Text style={{fontSize: RFValue(7), color: '#878787'}}>
                KITCHEN STATUS
              </Text>
              {console.log(
                'this.props.itemSelected status = ',
                this.props.itemSelected,
              )}
              {this.props.itemSelected === '' ? (
                <Text style={{fontSize: RFValue(13), color: '#878787'}}>
                  NO ORDER SELECTED
                </Text>
              ) : (
                <Text style={{fontSize: RFValue(13), color: '#878787'}}>
                  No. 101 - TABLE 5
                </Text>
              )}
            </View>
            {this.props.itemSelected === '' ? (
              <View
                style={{
                  width: '23.5%',
                  height: undefined,
                  aspectRatio: 3,
                  borderRadius: 3.5,
                  backgroundColor: '#878787',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  paddingHorizontal: '2%',
                }}>
                {/* <Image
                  source={require('../../../assets/General/RunningOrder/clock.png')}
                  resizeMode="contain"
                  style={{width: undefined, height: '60%', aspectRatio: 1}}
                /> */}
                <Text style={{fontSize: RFValue(10), color: '#FFFFFF'}}>
                  00:00
                </Text>
              </View>
            ) : (
              <View
                style={{
                  width: '23.5%',
                  height: undefined,
                  aspectRatio: 3,
                  borderRadius: 3.5,
                  backgroundColor: '#FEBF11',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  paddingHorizontal: '2%',
                }}>
                {/* <Image
                  source={require('../../../assets/General/RunningOrder/clock.png')}
                  resizeMode="contain"
                  style={{width: undefined, height: '60%', aspectRatio: 1}}
                /> */}
                <Text style={{fontSize: RFValue(10), color: '#FFFFFF'}}>
                  {this.state.time}
                </Text>
              </View>
            )}
          </View>
        </View>
        <View
          style={{
            height: '87.75%',
            backgroundColor: '#FFFFFF',
            justifyContent: 'flex-end',
            alignItems: 'center',
            paddingBottom: '3.4%',
            paddingHorizontal: 23,
          }}>
          <View style={{height: '73%', width: '100%'}}>
            {/* <ScrollView>
              <Cart isRunningOrder={true} />
              {console.log(
                'this.props.listAllDataHotDetail test data = ',
                this.props.listAllDataHotDetail,
              )}
              {this.props.listAllDataHotDetail.map(
                (listAllDataHotDetails, index) => (
                  <RunningOrderHotDetail
                    qtyHotDetail={listAllDataHotDetails.qty}
                    statusHotDetail={listAllDataHotDetails.del_status}
                    namaHotDetail={listAllDataHotDetails.food_hold.name}
                    priceHotDetail={listAllDataHotDetails.food_hold.sale_price}
                    key={index}
                  />
                ),
              )}
            </ScrollView> */}
            {this.props.itemSelected === '' ? (
              <View
                style={{
                  height: '100%',
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text>No Transaction</Text>
              </View>
            ) : (
              <ScrollView>
                {/* <Cart isRunningOrder={true} /> */}
                {console.log(
                  'this.props.listAllDataHotDetail test data = ',
                  this.props.listAllDataHotDetail,
                )}
                {this.props.listAllDataHotDetail.map(
                  (listAllDataHotDetails, index) => (
                    <RunningOrderHotDetail
                      qtyHotDetail={listAllDataHotDetails.qty}
                      statusHotDetail={listAllDataHotDetails.del_status}
                      namaHotDetail={listAllDataHotDetails.food_hold.name}
                      priceHotDetail={
                        listAllDataHotDetails.food_hold.sale_price
                      }
                      key={index}
                    />
                  ),
                )}
              </ScrollView>
            )}
          </View>

          {this.props.itemSelected === '' ? (
            <Text>''</Text>
          ) : (
            <View
              style={{
                marginTop: '2.377%',
                backgroundColor: 'white',
                paddingHorizontal: 15,
                alignContent: 'center',
                height: '14.562%',
                marginBottom: '-2%',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  height: '100%',
                }}>
                <TouchableOpacity
                  style={{width: '39%'}}
                  // onPress={this.props.createRunningOrder}
                >
                  <View
                    style={{
                      flexDirection: 'row',
                      flex: 1,
                      backgroundColor: '#6F6F6F',
                      borderRadius: RFValue(5),
                      marginRight: 2,
                      marginBottom: 8,
                      shadowColor: '#DF2428',
                      shadowOffset: {width: 0, height: 2},
                      shadowOpacity: 100,
                      shadowRadius: 2.62,
                      elevation: 3,
                    }}>
                    <View
                      style={{
                        backgroundColor: '#6F6F6F',
                        paddingHorizontal: 10,
                        paddingVertical: 2,
                        justifyContent: 'center',
                        marginBottom: 6,
                        marginRight: 6,
                        borderRadius: 10,
                        flex: 1,
                      }}>
                      <Text
                        style={{
                          fontSize: RFValue(15),
                          textAlign: 'center',
                          color: '#FFFFFF',
                        }}>
                        Print Bill
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{width: '59%'}}
                  onPress={() => this.props.navigation.navigate('Payment')}>
                  <View
                    style={{
                      flexDirection: 'row',
                      flex: 1,
                      backgroundColor: '#FEBF11',
                      borderRadius: RFValue(5),
                      marginRight: 2,
                      marginBottom: 8,
                      shadowColor: '#DF2428',
                      shadowOffset: {width: 0, height: 2},
                      shadowOpacity: 100,
                      shadowRadius: 2.62,
                      elevation: 3,
                    }}>
                    <View
                      style={{
                        backgroundColor: '#FEBF11',
                        paddingHorizontal: 10,
                        paddingVertical: 2,
                        justifyContent: 'center',
                        marginBottom: 6,
                        marginRight: 6,
                        borderRadius: 10,
                        flex: 1,
                      }}>
                      <Text
                        style={{
                          fontSize: RFValue(15),
                          textAlign: 'center',
                          color: '#FFFFFF',
                        }}>
                        Pay
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          )}

          {/* [START KITHCEN STATUS] */}
          {/* <View style={{height: '25%', width: '100%'}}>
            <Button
              navigation={this.props.navigation}
              cancelModalSwitch={this.cancelModalSwitch}
            />
          </View> */}
          {/* [END KITHCEN STATUS] */}
        </View>
        <CancelModal
          visible={this.state.cancelModalVisible}
          cancelModalSwitch={this.cancelModalSwitch}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userToken: state.rightFoodTransaction.tokenUser,
    testRedux: state.rightFoodTransaction.testRedux,
  };
};

export default connect(mapStateToProps)(RightCashier);
