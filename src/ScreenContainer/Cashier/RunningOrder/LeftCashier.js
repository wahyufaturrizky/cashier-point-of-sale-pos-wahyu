import React, {Component} from 'react';
import {Text, View} from 'react-native';
import runningOderService from '../../../services/runningOrder.service';
import {connect} from 'react-redux';
import RunningOrderList from './RunningOrderList';

class LeftCashier extends Component {
  constructor(props) {
    super(props);
    this.retrieveAllRunningOrder = this.retrieveAllRunningOrder.bind(this);
    this.state = {
      runningOrderList: [],
    };
  }

  componentDidMount() {
    this.retrieveAllRunningOrder();
  }

  retrieveAllRunningOrder() {
    runningOderService
      .getAllRunningOderData(this.props.userToken)
      .then((response) => {
        this.setState({
          runningOrderList: response.data.data,
        });
        // console.log('get all running order data = ', response.data.data);
        // console.log('runningOrderListHotDetail message = ', response.data.message);
      })
      .catch((e) => {
        console.log('Error get all running order data = ', e.response);
      });
  }

  render() {
    const {runningOrderList} = this.state;
    return (
      <View style={{alignItems: 'center', paddingHorizontal: '1%'}}>
        {/* {console.log("runningOrderList apa si isinya = ", runningOrderList)} */}
        {runningOrderList.map((runningOrderLists, index) => (
          <RunningOrderList
            order_status={runningOrderLists.del_status}
            table={runningOrderLists.table_id}
            uuidRunningOrder={runningOrderLists.UUID}
            idRunningOrder={runningOrderLists.id}
            pressAction={this.props.selectItem}
            key={index}
          />
        ))}

        {/* <RunningOrderList
          paidStatus={true}
          table="Table 4"
          selectItem={this.props.selectItem}
        />
        <RunningOrderList
          paidStatus={false}
          table="Takeaway"
          selectItem={this.props.selectItem}
        />
        <RunningOrderList
          paidStatus={false}
          table="Delivery"
          selectItem={this.props.selectItem}
        /> */}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userToken: state.rightFoodTransaction.tokenUser,
  };
};

export default connect(mapStateToProps)(LeftCashier);
