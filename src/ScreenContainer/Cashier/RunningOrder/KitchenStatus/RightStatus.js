/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, ScrollView, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

//import KitchenStatus from './KitchenStatus/KitchenStatus';
//import EditOrder from './EditOrder';
import Cart from '../../../View/Component/Cart';
import Button2 from '../Button2';

export default class RightCashier extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: '10:00',
    };
  }
  render() {
    return (
      <View>
        <View
          style={{
            height: '11%',
            backgroundColor: '#FFFFFF',
            marginBottom: '1.25%',
            justifyContent: 'center',
            paddingHorizontal: 23,
          }}>
          <View
            style={{
              height: '90%',
              borderRadius: 4,
              borderWidth: 0.5,
              borderColor: '#C5C5C5',
              paddingHorizontal: '3.14%',
              paddingVertical: 12,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View>
              <Text style={{fontSize: RFValue(7), color: '#878787'}}>
                KITCHEN STATUS
              </Text>
              {this.props.itemSelected === undefined ? (
                <Text style={{fontSize: RFValue(13), color: '#878787'}}>
                  NO ORDER SELECTED
                </Text>
              ) : (
                <Text style={{fontSize: RFValue(13), color: '#878787'}}>
                  No. 101 - TABLE 5
                </Text>
              )}
            </View>
            {this.props.itemSelected === undefined ? (
              <View
                style={{
                  width: '23.5%',
                  height: undefined,
                  aspectRatio: 3,
                  borderRadius: 3.5,
                  backgroundColor: '#878787',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  paddingHorizontal: '2%',
                }}>
                <Image
                  source={require('../../../../assets/General/RunningOrder/clock.png')}
                  resizeMode="contain"
                  style={{width: undefined, height: '60%', aspectRatio: 1}}
                />
                <Text style={{fontSize: RFValue(10), color: '#FFFFFF'}}>
                  00:00
                </Text>
              </View>
            ) : (
              <View
                style={{
                  width: '23.5%',
                  height: undefined,
                  aspectRatio: 3,
                  borderRadius: 3.5,
                  backgroundColor: '#FEBF11',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  paddingHorizontal: '2%',
                }}>
                <Image
                  source={require('../../../../assets/General/RunningOrder/clock.png')}
                  resizeMode="contain"
                  style={{width: undefined, height: '60%', aspectRatio: 1}}
                />
                <Text style={{fontSize: RFValue(10), color: '#FFFFFF'}}>
                  {this.state.time}
                </Text>
              </View>
            )}
          </View>
        </View>
        <View
          style={{
            height: '87.75%',
            backgroundColor: '#FFFFFF',
            justifyContent: 'flex-end',
            alignItems: 'center',
            paddingBottom: '3.4%',
            paddingHorizontal: 23,
          }}>
          <View style={{height: '73%', width: '100%'}}>
            {this.props.itemSelected === undefined ? (
              <View
                style={{
                  height: '100%',
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text>No Transaction</Text>
              </View>
            ) : (
              <ScrollView>
                <Cart isRunningOrder={true} />
              </ScrollView>
            )}
          </View>
          <View style={{height: '5%', width: '100%'}}>
            <Button2 text="Close" navigation={this.props.navigation} />
          </View>
        </View>
      </View>
    );
  }
}
