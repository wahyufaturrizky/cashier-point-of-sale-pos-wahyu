/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import CashierNavigator from '../../CashierNavigator';
import LeftStatus from '../../../Cashier/RunningOrder/KitchenStatus/LeftStatus';
import RightStatus from '../../../Cashier/RunningOrder/KitchenStatus/RightStatus';
//import Button2 from '../Button2';

class KitchenStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemSelected: 1,
    };
  }
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'row', height: '100%'}}>
        <View style={{flex: 85}}>
          <CashierNavigator
            runningOrderStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View
          style={{
            flex: 642,
            backgroundColor: '#F7F7F7',
            borderWidth: 0.5,
            paddingHorizontal: '1%',
          }}>
          <ScrollView
            style={{width: '90%', height: '90%', marginVertical: '2%'}}>
            <LeftStatus />
          </ScrollView>
        </View>
        <View style={{flex: 555, backgroundColor: '#F7F7F7'}}>
          <RightStatus
            navigation={this.props.navigation}
            itemSelected={this.state.itemSelected}
          />
        </View>
      </View>
    );
  }
}

export default KitchenStatus;
