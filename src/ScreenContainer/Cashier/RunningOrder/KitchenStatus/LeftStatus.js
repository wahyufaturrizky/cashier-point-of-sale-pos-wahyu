import React, {Component} from 'react';
import {Text, View} from 'react-native';

import RunningOrderList from '../RunningOrderList';

export default class LeftStatus extends Component {
  render() {
    return (
      <View style={{alignItems: 'center', paddingHorizontal: '1%'}}>
        <RunningOrderList
          paidStatus={true}
          table="Table 4"
          selectItem={this.props.selectItem}
        />
        <RunningOrderList
          paidStatus={false}
          table="Takeaway"
          selectItem={this.props.selectItem}
        />
        <RunningOrderList
          paidStatus={false}
          table="Delivery"
          selectItem={this.props.selectItem}
        />
      </View>
    );
  }
}
