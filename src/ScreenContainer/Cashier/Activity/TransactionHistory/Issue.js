import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import {CheckBox} from 'react-native-elements';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {RFValue} from 'react-native-responsive-fontsize';

export default class Issue extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refund: false,
      waitLong: false,
      unavailable: false,
    };
  }
  render() {
    return (
      <View style={{height: '100%'}}>
        <Text style={{alignSelf: 'center', fontSize: RFValue(9)}}>
          {' '}
          CHOOSE AN ISSUE{' '}
        </Text>
        <View style={{height: '80%'}}>
          <View
            style={{
              flexDirection: 'row',
              height: '14%',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: RFValue(9)}}>Wrong Refund</Text>
            <CheckBox
              containerStyle={{
                height: '100%',
                width: undefined,
                aspectRatio: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              checkedIcon={
                <Image
                  resizeMode="contain"
                  style={{
                    width: undefined,
                    height: '75%',
                    aspectRatio: 1,
                    alignSelf: 'center',
                  }}
                  source={require('../../../../assets/General/checkTrue.png')}
                />
              }
              uncheckedIcon={
                <Image
                  resizeMode="contain"
                  style={{
                    width: undefined,
                    height: '75%',
                    aspectRatio: 1,
                    alignSelf: 'center',
                  }}
                  source={require('../../../../assets/General/checkFalse.png')}
                />
              }
              checked={this.state.refund}
              onPress={() => this.setState({refund: !this.state.refund})}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              height: '14%',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: RFValue(9)}}>Customer wait too long</Text>
            <CheckBox
              containerStyle={{
                height: '100%',
                width: undefined,
                aspectRatio: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              checkedIcon={
                <Image
                  resizeMode="contain"
                  style={{
                    width: undefined,
                    height: '75%',
                    aspectRatio: 1,
                    alignSelf: 'center',
                  }}
                  source={require('../../../../assets/General/checkTrue.png')}
                />
              }
              uncheckedIcon={
                <Image
                  resizeMode="contain"
                  style={{
                    width: undefined,
                    height: '75%',
                    aspectRatio: 1,
                    alignSelf: 'center',
                  }}
                  source={require('../../../../assets/General/checkFalse.png')}
                />
              }
              checked={this.state.waitLong}
              onPress={() => this.setState({waitLong: !this.state.waitLong})}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              height: '14%',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: RFValue(9)}}>Menu is unavailable</Text>
            <CheckBox
              containerStyle={{
                height: '100%',
                width: undefined,
                aspectRatio: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              checkedIcon={
                <Image
                  resizeMode="contain"
                  style={{
                    width: undefined,
                    height: '75%',
                    aspectRatio: 1,
                    alignSelf: 'center',
                  }}
                  source={require('../../../../assets/General/checkTrue.png')}
                />
              }
              uncheckedIcon={
                <Image
                  resizeMode="contain"
                  style={{
                    width: undefined,
                    height: '75%',
                    aspectRatio: 1,
                    alignSelf: 'center',
                  }}
                  source={require('../../../../assets/General/checkFalse.png')}
                />
              }
              checked={this.state.unavailable}
              onPress={() =>
                this.setState({unavailable: !this.state.unavailable})
              }
            />
          </View>
          <View
            style={{
              height: '40%',
              width: '100%',
              borderRadius: RFValue(3),
              borderWidth: 0.5,
              padding: '2%',
              borderColor: '#C5C5C5',
            }}>
            <View style={{flexDirection: 'row', height: '20%'}}>
              <View
                style={{
                  marginRight: '5%',
                  marginLeft: 5,
                  height: '100%',
                  width: undefined,
                  justifyContent: 'center',
                  alignItems: 'center',
                  aspectRatio: 1,
                }}>
                <Image
                  source={require('../../../../assets/General/edit.png')}
                  resizeMode="contain"
                  style={{width: undefined, height: '75%', aspectRatio: 1}}
                />
              </View>
              <Text style={{fontSize: RFValue(9)}}>Other Reason</Text>
            </View>
            <TextInput
              style={{fontSize: RFValue(9)}}
              placeholder="Notes"
              autoCorrect={false}
              multiline={true}
            />
          </View>
        </View>

        <View
          style={{
            width: '100%',
            height: '10%',
            marginBottom: '2.5%',
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            style={{
              height: '100%',
              backgroundColor: '#878787',
              justifyContent: 'center',
              borderRadius: RFValue(5),
              flexDirection: 'row',
              paddingHorizontal: '3%',
              alignItems: 'center',
            }}
            onPress={this.props.changeRightComponent}>
            <Text
              style={{
                color: 'white',
                fontSize: RFValue(10),
                marginHorizontal: '15%',
              }}>
              CANCEL
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              height: '100%',
              backgroundColor: '#FEBF11',
              justifyContent: 'center',
              borderRadius: RFValue(5),
              flexDirection: 'row',
              paddingHorizontal: '3%',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: 'white',
                fontSize: RFValue(10),
                marginHorizontal: '18%',
              }}>
              REPORT
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
