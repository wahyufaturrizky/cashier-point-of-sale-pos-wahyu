import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import NumberFormat from 'react-number-format';

export default class TransactionItem extends Component {
  render() {
    return (
      <View style={{width: '100%', height: undefined, aspectRatio: 8}}>
        <TouchableOpacity
          style={{
            height: '90%',
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: 'white',
          }}
          onPress={() => this.props.selectItem(1)}>
          <View
            style={{
              height: '100%',
              width: undefined,
              aspectRatio: 6,
              flexDirection: 'row',
            }}>
            <View
              style={{
                height: '100%',
                width: undefined,
                aspectRatio: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={require('../../../../assets/Cashier/Activity/money.png')}
                resizeMode="contain"
                style={{height: '50%', width: undefined, aspectRatio: 1}}
              />
            </View>
            <View
              style={{
                height: '100%',
                width: undefined,
                aspectRatio: 4,
                justifyContent: 'center',
              }}>
              <NumberFormat
                value={this.props.amount}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp '}
                renderText={(value) => (
                  <Text style={{fontSize: RFValue(8)}}>{value}</Text>
                )}
              />
              <Text style={{fontSize: RFValue(7), color: '#CBCBCB'}}>
                {this.props.item}
              </Text>
            </View>
          </View>
          <View
            style={{
              height: '100%',
              width: undefined,
              aspectRatio: 2.5,
              justifyContent: 'center',
              alignItems: 'flex-end',
              paddingRight: RFValue(10),
            }}>
            <Text style={{fontSize: RFValue(8)}}>{this.props.time}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
