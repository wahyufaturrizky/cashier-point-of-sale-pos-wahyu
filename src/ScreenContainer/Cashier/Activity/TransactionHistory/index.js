import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView} from 'react-native';
import CashierNavigator from '../../CashierNavigator';
import Details from './Details';
import TransactionList from './TransactionList';
import Issue from './Issue';
import {RFValue} from 'react-native-responsive-fontsize';

import CashierMiniHeader from '../../CashierMiniHeader';

export default class TransactionHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transactionItem: null,
      rightComponent: 'details',
    };
  }
  selectItem = (index) => {
    this.state.transactionItem == null
      ? this.setState({transactionItem: index})
      : this.setState({transactionItem: null});
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          flexDirection: 'row',
          backgroundColor: '#F7F7F7',
        }}>
        <View style={{flex: 85}}>
          <CashierNavigator
            activityStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View
          style={{
            flex: 500,
            alignItems: 'center',
            justifyContent: 'center',
            padding: '1%',
          }}>
          <ScrollView style={{height: '100%', width: '100%'}}>
            <View style={{alignItems: 'center'}}>
              <TransactionList
                date="TODAY / Mon, March 9th 2020"
                selectItem={this.selectItem}
              />
              <TransactionList
                date="Yesterday / Sun, March 8th 2020"
                selectItem={this.selectItem}
              />
            </View>
          </ScrollView>
        </View>
        <View style={{flex: 695, backgroundColor: 'white'}}>
          <View
            style={{
              flex: 65,
              paddingVertical: 10,
              borderBottomWidth: 0.5,
              borderColor: '#C5C5C5',
            }}>
            <CashierMiniHeader
              title="TRANSACTION HISTORY"
              navigation={this.props.navigation}
            />
          </View>
          <View style={{flex: 735, padding: RFValue(10)}}>
            {this.rightComponent()}
            {console.log(this.state.rightComponent)}
          </View>
        </View>
      </View>
    );
  }
  rightComponent = () => {
    switch (this.state.transactionItem != null) {
      case true:
        switch (this.state.rightComponent) {
          case 'details':
            return <Details changeRightComponent={this.changeRightComponent} />;
          case 'issue':
            return <Issue changeRightComponent={this.changeRightComponent} />;
        }
      case false:
        return (
          <View
            style={{
              height: '100%',
              alignItems: 'center',
              bottom: RFValue(50),
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: RFValue(11)}}>No Transaction Selected</Text>
          </View>
        );
    }
  };
  changeRightComponent = () => {
    switch (this.state.rightComponent) {
      case 'details':
        return this.setState({rightComponent: 'issue'});
      case 'issue':
        return this.setState({rightComponent: 'details'});
    }
  };
}
