import React, {Component} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import CashierMiniHeader from '../../CashierMiniHeader';
import CashierNavigator from '../../CashierNavigator';
import ShiftDetails from './ShiftDetails';
import ShiftItem from './ShiftItem';

export default class ShiftHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shiftItem: null,
    };
  }
  selectItem = (index) => {
    this.state.shiftItem == null
      ? this.setState({shiftItem: index})
      : this.setState({shiftItem: null});
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          flexDirection: 'row',
          backgroundColor: '#F7F7F7',
        }}>
        <View style={{flex: 85}}>
          <CashierNavigator
            activityStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View style={{flex: 500, backgroundColor: '#F5F5F5'}}>
          <ScrollView
            style={{height: '100%', width: '100%', padding: RFValue(10)}}>
            <View style={{alignItems: 'center'}}>
              <ShiftItem
                shiftEnded={false}
                date="Friday / March, 6th 2020"
                time="11:45 PM"
                selectItem={this.selectItem}
              />
            </View>
          </ScrollView>
        </View>
        <View style={{flex: 695, backgroundColor: 'white'}}>
          <View
            style={{
              flex: 65,
              paddingVertical: 10,
              borderBottomWidth: 0.5,
              borderColor: '#C5C5C5',
            }}>
            <CashierMiniHeader
              title="SHIFT HISTORY"
              navigation={this.props.navigation}
            />
          </View>
          {/* Shift Information Start */}
          <View style={{flex: 735}}>
            {this.state.shiftItem != null ? (
              <ShiftDetails />
            ) : (
              <View
                style={{
                  height: '100%',
                  alignItems: 'center',
                  bottom: RFValue(50),
                  justifyContent: 'center',
                }}>
                <Text style={{fontSize: RFValue(11)}}>
                  No Transaction Selected
                </Text>
              </View>
            )}
          </View>
          {/* Shift Information End */}
        </View>
      </View>
    );
  }
}
