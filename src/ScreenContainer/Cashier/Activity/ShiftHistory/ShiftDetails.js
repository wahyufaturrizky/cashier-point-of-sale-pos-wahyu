import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class ShiftDetails extends Component {
  render() {
    return (
      <View style={{paddingHorizontal: '5%', paddingTop: '5%'}}>
        <View style={styles.textContainer}>
          <Text style={styles.titleStyles}>Name</Text>
          <Text style={styles.infoStyles}>Nuwiya Amal</Text>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.titleStyles}>Start Shift</Text>
          <Text style={styles.infoStyles}>
            Monday, March 2nd 2020 at 02:02 PM
          </Text>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.titleStyles}>End Shift</Text>
          <Text style={styles.infoStyles}>-</Text>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.titleStyles}>Cash Register</Text>
          <Text style={styles.infoStyles}>Rp 500.000</Text>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.titleStyles}>Closing Cash Register</Text>
          <Text style={styles.infoStyles}>Rp 5.000.000</Text>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.titleStyles}>Sold Items</Text>
          <Text style={styles.infoStyles}>351</Text>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.titleStyles}>Refunded Items</Text>
          <Text style={styles.infoStyles}>10</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textContainer: {
    height: '10%',
    marginVertical: 1,
    borderBottomWidth: 0.5,
    borderTopWidth: 0.5,
    borderColor: '#EAEAEA',
    justifyContent: 'space-between',
    paddingLeft: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleStyles: {
    color: '#FEBF11',
    fontSize: RFValue(9),
  },
  infoStyles: {
    fontSize: RFValue(9),
    alignSelf: 'center',
  },
});
