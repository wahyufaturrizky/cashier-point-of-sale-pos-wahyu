/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, ScrollView, TouchableOpacity} from 'react-native';
import CashierNavigator from '../../CashierNavigator';
import CashDetail from './CashDetail';
import {RFValue} from 'react-native-responsive-fontsize';

import ClosingCashItem from './ClosingCashItem';
import CashierMiniHeader from '../../CashierMiniHeader';

export default class ClosingCash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      closingCashItem: null,
    };
  }
  selectItem = (index) => {
    this.state.closingCashItem == null
      ? this.setState({closingCashItem: index})
      : this.setState({closingCashItem: null});
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          flexDirection: 'row',
          backgroundColor: '#F7F7F7',
        }}>
        <View style={{flex: 85}}>
          <CashierNavigator
            activityStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View style={{flex: 500}}>
          <ScrollView
            style={{height: '100%', width: '100%', padding: RFValue(10)}}>
            <View style={{alignItems: 'center'}}>
              <ClosingCashItem
                registerOpened={true}
                date="TODAY / Mon, March 9th 2020"
                time="11:45 PM"
                selectItem={this.selectItem}
              />
              <ClosingCashItem
                registerOpened={false}
                date="Yesterday / Sun, March 8th 2020"
                time="11:45 PM"
                selectItem={this.selectItem}
              />
            </View>
          </ScrollView>
        </View>
        <View style={{flex: 695, backgroundColor: 'white'}}>
          <View
            style={{
              flex: 65,
              paddingVertical: 10,
              borderBottomWidth: 0.5,
              borderColor: '#C5C5C5',
            }}>
            <CashierMiniHeader
              title="CLOSING CASH REGISTER"
              navigation={this.props.navigation}
            />
          </View>
          <View style={{flex: 735, padding: RFValue(10)}}>
            {this.state.closingCashItem == 1 ? (
              <CashDetail />
            ) : (
              <View
                style={{
                  height: '100%',
                  alignItems: 'center',
                  bottom: RFValue(50),
                  justifyContent: 'center',
                }}>
                <Text style={{fontSize: RFValue(11)}}>
                  No Transaction Selected
                </Text>
              </View>
            )}
          </View>
        </View>
      </View>
    );
  }
}
