/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class ClosingCashItem extends Component {
  render() {
    return (
      <View style={{width: '100%', height: undefined, aspectRatio: 8}}>
        <TouchableOpacity
          style={{
            height: '90%',
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: 'white',
          }}
          onPress={() => this.props.selectItem(1)}>
          <View
            style={{
              height: '100%',
              width: undefined,
              aspectRatio: 6,
              flexDirection: 'row',
            }}>
            <View
              style={{
                height: '100%',
                width: undefined,
                aspectRatio: 2,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {this.props.registerOpened ? (
                <Text style={{fontSize: RFValue(9), color: '#FEBF11'}}>
                  Open
                </Text>
              ) : (
                <Text style={{fontSize: RFValue(9), color: '#C5C5C5'}}>
                  Close
                </Text>
              )}
            </View>
            <View
              style={{
                height: '100%',
                width: undefined,
                aspectRatio: 4.85,
                justifyContent: 'center',
              }}>
              <Text style={{fontSize: RFValue(7), color: '#CBCBCB'}}>Date</Text>
              <Text style={{fontSize: RFValue(8)}}>{this.props.date}</Text>
            </View>
          </View>
          <View
            style={{
              height: '100%',
              width: undefined,
              aspectRatio: 2,
              justifyContent: 'center',
              alignItems: 'flex-end',
              paddingRight: RFValue(10),
            }}>
            <Text style={{fontSize: RFValue(7), color: '#CBCBCB'}}>Clock</Text>
            <Text style={{fontSize: RFValue(8)}}>{this.props.time}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
