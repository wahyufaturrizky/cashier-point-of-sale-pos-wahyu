/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, TextInput, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {connect} from 'react-redux';
import CashRegisterService from '../../services/cashRegister.service';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cashRegister: null,
    };
  }

  onSubmitCashRegister = () => {
    CashRegisterService.getCashRegister(
      this.state.cashRegister,
      this.props.userToken,
    )
      .then((response) => {
        this.setState({
          loginStatus: response.data,
        });
        console.log('cash register = ', response.data);
      })
      .catch((e) => {
        console.log('error cash register = ', e.response);
      });

    this.props.navigation.navigate('Transaction');
  };

  handleChange = (value, name) => {
    console.log('Yang diketik di', name, ' =', value);
    this.setState({
      [name]: value,
    });
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'white',
            paddingHorizontal: RFValue(150),
          }}>
          <View style={{flex: 5}} />
          <View style={{flex: 2, alignItems: 'center'}}>
            <Text style={{fontSize: RFValue(12)}}> INSERT CASH REGISTER </Text>
          </View>
          <View
            style={{
              shadowColor: '#707070',
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'center',
              height: RFValue(30),
              width: '100%',
            }}>
            {this.state.cashRegister > 0 ? (
              <Text style={{fontSize: RFValue(12)}}>Rp.</Text>
            ) : (
              <View />
            )}
            <TextInput
              placeholder="Input Cash Register"
              style={{
                fontSize: RFValue(12),
                color: 'black',
                alignSelf: 'center',
                paddingVertical: 0,
              }}
              placeholderTextColor={'#C6CED9'}
              keyboardType="number-pad"
              onChangeText={(value) => this.handleChange(value, 'cashRegister')}
            />
          </View>
          {parseInt(this.state.cashRegister) > 0 ? (
            <View style={{width: '100%'}}>
              <View
                style={{
                  width: '100%',
                  borderBottomWidth: 1,
                  borderBottomColor: '#FEBF11',
                }}
              />
              <TouchableOpacity
                style={{
                  width: '100%',
                  height: RFValue(30),
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: RFValue(20),
                  borderRadius: RFValue(5),
                  backgroundColor: '#FEBF11',
                }}
                // onPress={() => this.props.navigation.navigate('Transaction')}
                onPress={this.onSubmitCashRegister}>
                <Text style={{color: 'white', fontSize: RFValue(10)}}>
                  START
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View style={{width: '100%'}}>
              <View
                style={{
                  width: '100%',
                  borderBottomWidth: 1,
                  borderBottomColor: '#707070',
                }}
              />
              <TouchableOpacity
                style={{
                  width: '100%',
                  height: RFValue(30),
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: RFValue(20),
                  borderRadius: RFValue(5),
                  backgroundColor: '#878787',
                }}>
                <Text style={{color: 'white', fontSize: RFValue(10)}}>
                  START
                  {console.log(
                    'userToken yang masuk d cash register = ',
                    this.props.userToken,
                  )}
                </Text>
              </TouchableOpacity>
            </View>
          )}
          <View style={{flex: 5}} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userToken: state.rightFoodTransaction.tokenUser,
  };
};

export default connect(mapStateToProps)(Register);
