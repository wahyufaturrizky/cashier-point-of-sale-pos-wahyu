/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class CashierMiniHeader extends Component {
  render() {
    return (
      <View
        style={{
          flexDirection: 'row',
          flex: 1,
          paddingHorizontal: '2%',
          justifyContent: 'space-between',
        }}>
        <TouchableOpacity
          style={{
            width: '11%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
          onPress={() => this.props.navigation.goBack()}>
          <Image
            source={require('../../assets/General/back.png')}
            resizeMode="contain"
            style={{height: '80%', width: undefined, aspectRatio: 1}}
          />
          <Text style={{fontSize: RFValue(7)}}>Back</Text>
        </TouchableOpacity>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={{fontSize: RFValue(8)}}>{this.props.title}</Text>
        </View>
        <View style={{width: '11%'}} />
      </View>
    );
  }
}
