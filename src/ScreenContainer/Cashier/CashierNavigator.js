/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class CashierNavigator extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'space-between',
          alignItems: 'center',
          backgroundColor: 'white',
          shadowOpacity: 1,
          shadowColor: '#C5C5C5',
          elevation: 20,
        }}>
        <TouchableOpacity
          style={styles.touchableStyleTransaction}
          onPress={() => this.props.navigation.navigate('Transaction')}>
          <View style={styles.imageContainer}>
            {this.props.cartStatus ? (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconTransactionNew.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Sales</Text>

                {/* [START] Vertical Text */}
                {/* <Text style={styles.textSelected}>S</Text>
                <Text style={styles.textSelected}>A</Text>
                <Text style={styles.textSelected}>L</Text>
                <Text style={styles.textSelected}>E</Text>
                <Text style={styles.textSelected}>S</Text> */}
                {/* [END] Vertical Text */}
              </View>
            ) : (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconTransactionNew.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Sales</Text>

                {/* [START] Vertical Text */}
                {/* <Text style={styles.textSelected}>S</Text>
                <Text style={styles.textSelected}>A</Text>
                <Text style={styles.textSelected}>L</Text>
                <Text style={styles.textSelected}>E</Text>
                <Text style={styles.textSelected}>S</Text> */}
                {/* [END] Vertical Text */}
              </View>
            )}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.touchableStyleDiscount}
          onPress={() => this.props.navigation.navigate('Discount')}>
          <View style={styles.imageContainer}>
            {this.props.discountStatus ? (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconDiscountNew.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Discount</Text>

                {/* [START] Vertical Text */}
                {/* <Text style={styles.textSelected}>D</Text>
                <Text style={styles.textSelected}>I</Text>
                <Text style={styles.textSelected}>S</Text>
                <Text style={styles.textSelected}>C</Text>
                <Text style={styles.textSelected}>O</Text>
                <Text style={styles.textSelected}>U</Text>
                <Text style={styles.textSelected}>N</Text>
                <Text style={styles.textSelected}>T</Text> */}
                {/* [END] Vertical Text */}
              </View>
            ) : (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconDiscountNew.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Discount</Text>
                {/* [START] Vertical Text */}
                {/* <Text style={styles.textSelected}>D</Text>
                <Text style={styles.textSelected}>I</Text>
                <Text style={styles.textSelected}>S</Text>
                <Text style={styles.textSelected}>C</Text>
                <Text style={styles.textSelected}>O</Text>
                <Text style={styles.textSelected}>U</Text>
                <Text style={styles.textSelected}>N</Text>
                <Text style={styles.textSelected}>T</Text> */}
                {/* [END] Vertical Text */}
              </View>
            )}
          </View>
        </TouchableOpacity>

        {/* [START Remove Activity] */}
        {/* <TouchableOpacity
          style={styles.touchableStyle}
          onPress={() => this.props.navigation.navigate('Activity')}>
          <View style={styles.imageContainer}>
            {this.props.activityStatus ? (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconActivitySelected.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Activity</Text>
              </View>
            ) : (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconActivityUnselected.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textUnselected}>Activity</Text>
              </View>
            )}
          </View>
        </TouchableOpacity> */}
        {/* [END Remove Activity] */}

        <TouchableOpacity
          style={styles.touchableStyleRunningOrder}
          onPress={() => this.props.navigation.navigate('RunningOrder')}>
          <View style={styles.imageContainer}>
            {this.props.runningOrderStatus ? (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconRunningOrderNew.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Running Order</Text>
              </View>
            ) : (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconRunningOrderNew.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Running Order</Text>
              </View>
            )}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.touchableStyleHistory}
          onPress={() => this.props.navigation.navigate('TransactionHistory')}>
          <View style={styles.imageContainer}>
            {this.props.settingStatus ? (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconSalesHistoryNew.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Sales History</Text>

                {/* <Text style={styles.textSelected}>H</Text>
                <Text style={styles.textSelected}>I</Text>
                <Text style={styles.textSelected}>S</Text>
                <Text style={styles.textSelected}>T</Text>
                <Text style={styles.textSelected}>O</Text>
                <Text style={styles.textSelected}>R</Text>
                <Text style={styles.textSelected}>Y</Text> */}
              </View>
            ) : (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconSalesHistoryNew.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Sales History</Text>
              </View>
            )}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.touchableStyleSettings}
          onPress={() => this.props.navigation.navigate('Setting')}>
          <View style={styles.imageContainer}>
            {this.props.settingStatus ? (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconSettingNew.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Settings</Text>
              </View>
            ) : (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconSettingNew.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Settings</Text>

                {/* <Text style={styles.textSelected}>S</Text>
                <Text style={styles.textSelected}>E</Text>
                <Text style={styles.textSelected}>T</Text>
                <Text style={styles.textSelected}>T</Text>
                <Text style={styles.textSelected}>I</Text>
                <Text style={styles.textSelected}>N</Text>
                <Text style={styles.textSelected}>G</Text> */}
              </View>
            )}
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  touchableStyleTransaction: {
    width: '100%',
    height: '20%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFC012',
  },
  touchableStyleDiscount: {
    width: '100%',
    height: '20%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00CD3D',
  },
  touchableStyleRunningOrder: {
    width: '100%',
    height: '20%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FA9B9B',
  },
  touchableStyleHistory: {
    width: '100%',
    height: '20%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#77C6FF',
  },
  touchableStyleSettings: {
    width: '100%',
    height: '20%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#CAA4F1',
  },
  imageContainer: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textSelected: {
    fontSize: RFValue(10),
    alignSelf: 'center',
    fontWeight: 'bold',
    color: '#4A4A4A',
    justifyContent: 'center',
    textAlign: 'center',
  },
  textUnselected: {
    fontSize: RFValue(10),
    alignSelf: 'center',
    fontWeight: 'bold',
    color: '#4A4A4A',
    justifyContent: 'center',
  },
});
