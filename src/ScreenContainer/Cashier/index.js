import Transaction from './Transaction';
import Discount from './Discount';
import Payment from './Transaction/Payment';
import SplitBill from './Transaction/SplitBill';
import SplitPayment from './Transaction/SplitPayment';
import Activity from './Activity';
import Register from './Register';
import TransactionHistory from './Activity/TransactionHistory';
import ClosingCash from './Activity/ClosingCash';
import ShiftHistory from './Activity/ShiftHistory';
import KitchenStatus from './RunningOrder/KitchenStatus';
import PaymentSuccess from './Transaction/Payment/PaymentSuccess';

export {
  Register,
  Transaction,
  Discount,
  KitchenStatus,
  Payment,
  SplitBill,
  SplitPayment,
  Activity,
  TransactionHistory,
  ClosingCash,
  ShiftHistory,
  PaymentSuccess,
};
