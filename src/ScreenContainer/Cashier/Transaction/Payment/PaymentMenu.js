/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {ScrollView} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getAllDataUangTunai} from '../../../../redux/actions/transaction/rightFoodTransactionAction';
import RunningPrintService from '../../../../services/runningPrint.service';
import EscPos from '@leesiongchan/react-native-esc-pos';

// const designinVoice = `
//         WARUNG KUKURUYUK
//         Jl. Ancol Timur XIV No 1
//         Tel: 0822 7458 6011
//         Tax Registration No: 123
//         Invoice No: A 000035
// [ ] Date : ${this.state.data_invoice_pay.date}
// [ ] Sales Associate : Admin
// [ ] Customer : ${this.state.data_invoice_pay.customer}
// [ ] SN Item Qty
//         ${this.state.data_invoice_pay.details[0].menu_name}    x ${this.state.data_invoice_pay.details[0].qty}
// [ ] Sub Total :
// [ ] Disc Amt (%) :
// [ ] Service/Delivery Charge :
// [ ] Tax :
// [ ] Grand Total :
// [ ] Cash :
// [ ] Change Amount :

// Terimakasih telah Berkunjung
// `;

class PaymentMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uang_tunai: null,
      value_qr_code: 'wahyu',
      print_invoice_status: '',
      data_invoice_pay: '',
      ip_address_printing_invoice_pay: 'null',
    };
  }

  createPrintingInvoicePay = () => {
    console.log(
      'this.props.all_data_id_order_printing di Pay = ',
      this.props.all_data_id_order_printing,
    );
    RunningPrintService.createPrintingInvoicePay(
      this.props.all_data_id_order_printing,
      this.props.userToken,
    )
      .then((response) => {
        console.log('response createPrintingInvoicePay = ', response);
        this.setState({
          print_invoice_status: response.data.message,
          data_invoice_pay: response.data.data,
          ip_address_printing_invoice_pay: response.data.data.ip_address,
        });

        this.printingProcessInvoicePay();
      })
      .catch((e) => {
        console.log('Error createPrintingInvoicePay = ', e.response);
      });

    this.props.navigation.navigate('PaymentSuccess');
  };

  printingProcessInvoicePay = async () => {
    console.log(
      'this.state.ip_address_success = ',
      this.state.ip_address_printing_invoice_pay,
      'dan this.state.ip_address_data_invoice_dua = ',
      this.state.data_invoice_pay,
    );

    try {
      EscPos.setConfig({type: 'network'});

      await EscPos.connect(this.state.ip_address_printing_invoice_pay, 9100);
      EscPos.setPrintingSize(EscPos.PRINTING_SIZE_80_MM);
      EscPos.setTextDensity(8);
      // await EscPos.printDesign(this.state.data_invoice_pay);
      await EscPos.printDesign(
        'WARUNG KUKURUYUK Jl. Ancol Timur XIV No 1 Tel: 0822 7458 6011 Tax Registration No: 123 Invoice No: A 000035 [ ] Date : ',
        this.state.data_invoice_pay.date,
        '[ ] Sales Associate : Admin [ ] Customer : ',
        this.state.data_invoice_pay.customer,
        '[ ] SN Item Qty',
        this.state.data_invoice_pay.details[0].menu_name,
        'x',
        this.state.data_invoice_pay.details[0].qty,
        '[ ] Sub Total : [ ] Disc Amt (%) : [ ] Service/Delivery Charge : [ ] Tax : [ ] Grand Total : [ ] Cash : [ ] Change Amount : Terimakasih telah Berkunjung',
      );
      await EscPos.cutFull();
      await EscPos.beep();
      //await EscPos.kickDrawerPin();
      await EscPos.disconnect();
    } catch (error) {
      console.error(error);
    }
  };

  handleChange = (value, name) => {
    console.log('yangr diketik di', name, 'adalah = ', value);
    this.setState(
      {
        [name]: value,
      },
      () => this.props.getAllDataUangTunai(this.state.uang_tunai),
    );
  };
  render() {
    return (
      <ScrollView
        showsVerticalScrollIndicator={true}
        style={{paddingHorizontal: '15%'}}>
        <View
          style={{
            height: RFValue(25),
            marginTop: RFValue(8),
            borderWidth: 1.1,
            borderRadius: 4.4,
            borderColor: '#E9E9E9',
            width: '100%',
            justifyContent: 'center',
          }}>
          <TextInput
            style={{alignSelf: 'center', fontSize: RFValue(10)}}
            placeholder="Tunai"
            keyboardType="numeric"
            onChangeText={(value) => this.handleChange(value, 'uang_tunai')}
          />
        </View>
        <TouchableOpacity
          style={{
            height: RFValue(25),
            width: '100%',
            borderWidth: 1.1,
            borderRadius: 6.6,
            borderColor: '#E9E9E9',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#FEBF11',
            marginTop: RFValue(8),
          }}>
          <Text style={{color: 'white', fontSize: RFValue(10)}}>UANG PAS</Text>
        </TouchableOpacity>
        {/* [START] Discount Option*/}
        {/* <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            marginTop: RFValue(8),
            height: RFValue(25),
          }}>
          <TouchableOpacity
            style={{
              height: RFValue(25),
              width: '100%',
              borderWidth: 1.1,
              borderRadius: 6.6,
              borderColor: '#E9E9E9',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: RFValue(8),
              backgroundColor: '#707070',
            }}
            onPress={this.props.discount}>
            <Text style={{color: 'white', fontSize: RFValue(10)}}>
              Discount Option
            </Text>
          </TouchableOpacity>
        </View> */}
        {/* [END] Discount Option*/}
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            marginTop: RFValue(8),
            justifyContent: 'space-between',
            flexWrap: 'wrap',
          }}>
          <TouchableOpacity
            style={{
              width: '30%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#EB5757',
              borderRadius: RFValue(5),
            }}>
            {/* <Image
              source={require('../../../../assets/Cashier/Transaction/Payment/ovo.png')}
              resizeMode="contain"
              style={{width: '100%', height: undefined, aspectRatio: 2.45}}
            /> */}
            <Text
              style={{
                color: 'white',
                fontSize: RFValue(8),
                width: '100%',
                height: undefined,
                aspectRatio: 2.45,
                textAlign: 'center',
                textAlignVertical: 'center',
              }}>
              OVO
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '30%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#6D91F0',
              borderRadius: RFValue(5),
            }}>
            {/* <Image
              source={require('../../../../assets/Cashier/Transaction/Payment/linkaja.png')}
              resizeMode="contain"
              style={{width: '100%', height: undefined, aspectRatio: 2.45}}
            /> */}
            <Text
              style={{
                color: 'white',
                fontSize: RFValue(8),
                width: '100%',
                height: undefined,
                aspectRatio: 2.45,
                textAlign: 'center',
                textAlignVertical: 'center',
              }}>
              LINK AJA
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '30%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#33C15D',
              borderRadius: RFValue(5),
            }}>
            {/* <Image
              source={require('../../../../assets/Cashier/Transaction/Payment/dana.png')}
              resizeMode="contain"
              style={{width: '100%', height: undefined, aspectRatio: 2.45}}
            /> */}
            <Text
              style={{
                color: 'white',
                fontSize: RFValue(8),
                width: '100%',
                height: undefined,
                aspectRatio: 2.45,
                textAlign: 'center',
                textAlignVertical: 'center',
              }}>
              DANA
            </Text>
          </TouchableOpacity>
        </View>

        {/* [START] QRCODE */}
        {/* <View style={styles.containerQrCode}>
          <WebView source={{ uri: 'https://reactnative.dev/' }}/>
          <QRCode
            value={this.state.value_qr_code}
            size={200}
            bgColor='purple'
            fgColor='white'/>
        </View> */}
        {/* [END] QRCODE */}

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            marginTop: RFValue(50),
            height: RFValue(25),
          }}>
          <TouchableOpacity
            style={{
              height: '100%',
              width: '50%',
              borderWidth: 1.1,
              borderRadius: 5,
              borderColor: '#E9E9E9',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#707070',
            }}>
            <Text style={{fontSize: RFValue(10), color: 'white'}}>Split</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              height: '100%',
              width: '50%',
              borderWidth: 1.1,
              borderRadius: 5,
              borderColor: '#E9E9E9',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#FEBF11',
            }}
            // onPress={() => this.props.navigation.navigate('PaymentSuccess')}
            onPress={this.createPrintingInvoicePay}>
            <Text style={{fontSize: RFValue(10), color: 'white'}}>Pay</Text>
          </TouchableOpacity>
        </View>
        {/* [START] PAY FULL WIDTH */}
        {/* <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            marginTop: RFValue(5),
            height: RFValue(25),
          }}>
          <TouchableOpacity
            style={{
              height: '100%',
              width: '100%',
              borderWidth: 1.1,
              borderRadius: 5,
              borderColor: '#E9E9E9',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#FEBF11',
            }}
            onPress={() => {
              this.props.navigation.navigate('PaymentSuccess');
            }}>
            <Text style={{fontSize: RFValue(10), color: 'white'}}>PAY</Text>
          </TouchableOpacity>
        </View> */}
        {/* [END] PAY FULL WIDTH */}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  containerQrCode: {
    flex: 1,
    margin: 10,
    alignItems: 'center',
    paddingTop: 40,
    backgroundColor: 'pink',
    justifyContent: 'center',
  },
});

const mapStateToProps = (state) => {
  return {
    testRedux: state.rightFoodTransaction.testRedux,
    userToken: state.rightFoodTransaction.tokenUser,
    all_data_id_order_printing:
      state.rightFoodTransaction.all_data_id_order_printing,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({getAllDataUangTunai}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PaymentMenu);
