/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import PaymentSuccessUp from './PaymentSuccessUp';
import PaymentSuccessDown from './PaymentSuccessDown';
import CashierNavigator from '../../../CashierNavigator';
import {connect} from 'react-redux';
import RunningOrderService from '../../../../../services/runningOrderData.service';
import RunningPrintService from '../../../../../services/runningPrint.service';
import EscPos from '@leesiongchan/react-native-esc-pos';

// const designInvoiceDua = `
//         WARUNG KUKURUYUK
//         Jl. Ancol Timur XIV No 1
//         Tel: 0822 7458 6011
//         Tax Registration No: 123
//         Invoice No: A 000035
// [ ] Date : ${this.state.ip_address_data_invoice_dua.date}
// [ ] Sales Associate : Admin
// [ ] Customer : ${this.state.ip_address_data_invoice_dua.customer}
// [ ] SN Item Qty
//         ${this.state.ip_address_data_invoice_dua.details[0].menu_name}    x ${this.state.ip_address_data_invoice_dua.details[0].qty}
// [ ] Sub Total :
// [ ] Disc Amt (%) :
// [ ] Service/Delivery Charge :
// [ ] Tax :
// [ ] Grand Total :

// Terimakasih telah Berkunjung
// `;

class PaymentSuccess extends Component {
  constructor(props) {
    super(props);
    this.state = {
      printing_process_get_image: '',
      ip_address_success: '',
      ip_address_data_invoice_dua: '',
    };
  }

  createRunningOrder = () => {
    RunningOrderService.createRunningOrderData(
      this.props.allTransactionMenu.type_order,
      this.props.allTransactionMenu.floor_id,
      this.props.allTransactionMenu.customer_id,
      this.props.allTransactionMenu.numberTable,
      this.props.allTransactionMenu.foodMenuData,
      this.props.userToken,
    )
      .then((response) => {
        this.setState({
          RunningOrderDataStatus: response.data.message,
          RunningOrderData: response.data.data,
        });
        console.log(
          'createRunningOrderData Status Done = ',
          response.data.message,
        );
        console.log('createRunningOrderData Done = ', response.data.data);

        RunningPrintService.createPrintingInvoiceDuaDone(
          this.props.all_data_id_order_printing,
          this.props.userToken,
        )
          .then((response) => {
            this.setState({
              ip_address_data_invoice_dua: response.data.data,
              ip_address_success: response.data.data.ip_address,
            });

            this.printingProcessInvoiceDua();
          })
          .catch((e) => {
            console.log('Error create createRunningOrderData = ', e.response);
          });
      })
      .catch((e) => {
        console.log('Error create createRunningOrderData Done = ', e.response);
      });

    this.props.navigation.navigate('Transaction');
  };

  printingProcessInvoiceDua = async () => {
    console.log(
      'this.state.ip_address_data_invoice_dua = ',
      this.state.ip_address_success,
      'dan this.state.ip_address_data_invoice_dua = ',
      this.state.ip_address_data_invoice_dua,
    );

    try {
      EscPos.setConfig({type: 'network'});

      await EscPos.connect(this.state.ip_address_success, 9100);
      EscPos.setPrintingSize(EscPos.PRINTING_SIZE_80_MM);
      EscPos.setTextDensity(8);
      // await EscPos.printDesign(this.state.ip_address_data_invoice_dua);
      await EscPos.printDesign(
        'WARUNG KUKURUYUK Jl. Ancol Timur XIV No 1 Tel: 0822 7458 6011 Tax Registration No: 123 Invoice No: A 000035 [ ] Date : ',
        this.state.ip_address_data_invoice_dua.date,
        '[ ] Sales Associate : Admin [ ] Customer : ',
        this.state.ip_address_data_invoice_dua.customer,
        '[ ] SN Item Qty',
        this.state.ip_address_data_invoice_dua.details[0].menu_name,
        'x',
        this.state.ip_address_data_invoice_dua.details[0].qty,
        '[ ] Sub Total : [ ] Disc Amt (%) : [ ] Service/Delivery Charge : [ ] Tax : [ ] Grand Total : [ ] Terimakasih telah Berkunjung',
      );
      await EscPos.cutFull();
      await EscPos.beep();
      //await EscPos.kickDrawerPin();
      await EscPos.disconnect();
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    return (
      <View style={{flex: 1, flexDirection: 'row', height: '100%'}}>
        <View style={{flex: 45}}>
          <CashierNavigator
            runningOrderStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View style={{flex: 650}}>
          <View>
            <PaymentSuccessUp
              uangKembali={this.props.dataUangTunai - this.props.subTotal}
            />
          </View>
          <View>
            <PaymentSuccessDown />
            <View>
              <TouchableOpacity
                style={{
                  width: '50%',
                  height: undefined,
                  aspectRatio: 10,
                  backgroundColor: '#FEBF11',
                  borderRadius: 6.6,
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                }}
                onPress={this.createRunningOrder}>
                <Text style={{color: 'white', fontSize: RFValue(10)}}>
                  Done
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dataUangTunai: state.rightFoodTransaction.dataUangTunai,
    subTotal: state.rightFoodTransaction.subTotal,
    allTransactionMenu: state.rightFoodTransaction.allTransactionMenu,
    all_data_discount: state.rightFoodTransaction.all_data_discount,
    all_data_id_order_printing:
      state.rightFoodTransaction.all_data_id_order_printing,
  };
};
export default connect(mapStateToProps)(PaymentSuccess);
