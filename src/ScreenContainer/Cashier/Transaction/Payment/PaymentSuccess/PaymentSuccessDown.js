/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

class PaymentSuccessDown extends Component {
  render() {
    return (
      <View>
        <View style={styles.textInputWrapper}>
          <TextInput placeholder="e-mail" />
        </View>
        <View style={styles.touchableWrapper}>
          {/* <TouchableOpacity style={{flexDirection: 'row'}}>
            <View style={styles.imageWrapper}>
              <Image
                source={require('../../../../../assets/General/RunningOrder/Icon_Mail_1.png')}
              />
            </View>
            <View style={styles.text1Wrapper}>
              <Text style={styles.text1}>Kirim struk ke e-mail</Text>
            </View>
            <View style={{justifyContent: 'center'}}>
              <Image
                source={require('../../../../../assets/General/RunningOrder/Icon_Mail_2.png')}
              />
            </View>
          </TouchableOpacity> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  touchableWrapper: {
    height: '24%',
    width: '35%',
    //flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    // borderWidth: 2,
    // borderColor: '#5DADE2',
    borderRadius: RFValue(2),
    marginTop: RFValue(10),
    //backgroundColor: 'yellow',
  },
  imageWrapper: {
    height: '100%',
    width: '10%',
    //paddingRight: RFValue(30),
    marginRight: RFValue(30),
    //backgroundColor: 'orange',
    alignItems: 'center',
  },
  textInputWrapper: {
    height: '24%',
    width: '35%',
    marginTop: RFValue(10),
    //backgroundColor: 'green',
    alignItems: 'center',
    alignSelf: 'center',
    borderWidth: 2,
    borderColor: '#D5D8DC',
    borderRadius: RFValue(2),
  },
  text1Wrapper: {
    //backgroundColor: 'red',
    alignItems: 'center',
    marginRight: RFValue(20),
  },
  text1: {
    fontSize: RFValue(9),
    color: '#6A6A6A',
  },
});

export default PaymentSuccessDown;
