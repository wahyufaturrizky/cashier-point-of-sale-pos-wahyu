import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {ScrollView} from 'react-native-gesture-handler';

export default class TaxMenu extends Component {
  render() {
    return (
      <View style={{justifyContent: 'space-between', height: '65%'}}>
        <View style={{height: '80%', justifyContent: 'space-around'}}>
          <ScrollView style={{height: '100%', width: '100%'}}>
            <View
              style={{
                height: '100%',
                width: '100%',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'space-between',
              }}>
              <TouchableOpacity
                style={{
                  height: RFValue(30),
                  width: '48%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 5,
                  backgroundColor: '#878787',
                  marginBottom: '2%',
                }}>
                <Text style={{color: 'white', fontSize: RFValue(9)}}>
                  ppn 10%
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
        <TouchableOpacity
          style={{
            height: '15%',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 6.6,
            backgroundColor: '#FEBF11',
          }}
          onPress={this.props.done}>
          <Text style={{color: 'white', fontSize: RFValue(10)}}>DONE</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
