/* eslint-disable radix */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {
  Text,
  View,
  Modal,
  TextInput,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import ModalHeader from '../../../View/Component/ModalHeader';
import TableService from '../../../../services/table.service';
import AsyncStorage from '@react-native-community/async-storage';
// import DineInModal from '../DineInModal';

export default class CustomerDetailDineInModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
      // onloading: 0,
      dineInModalVisible: false,
      customerDetailDineInModalVisible: false,
    };
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
  }

  render() {
    const {tables} = this.state;
    return (
      <View>
        <Modal visible={this.state.visible} transparent={true}>
          <View
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#000000aa',
            }}>
            <View
              style={{
                height: '90%',
                width: '90%',
                backgroundColor: 'white',
                alignItems: 'center',
                borderRadius: RFValue(2),
              }}>
              <View
                style={{
                  height: '10%',
                  width: '100%',
                  shadowOpacity: 1,
                  backgroundColor: 'white',
                  elevation: 1,
                }}>
                <ModalHeader
                  title="CUSTOMER DETAIL - DINE IN"
                  close={this.props.customerDetailDineInModalSwitch}
                  // save={() => { this.onTable(this.state)}}
                  // save={this.props.dineInModalSwitch}
                  // save={this.props.handleSubmitDataCustomer}
                />
              </View>

              <View style={{height: '90%', width: '100%'}}>
                <ScrollView>
                  <View
                    style={{
                      width: '100%',
                      alignItems: 'center',
                      marginVertical: RFValue(15),
                    }}>
                    {/* [START Customer Name] */}
                    {/* <View
                      style={{
                        height: RFValue(75),
                        width: '70%',
                        alignItems: 'center',
                      }}>
                      <Text style={{fontSize: RFValue(16)}}>Customer Name</Text>
                      <TextInput
                        style={{
                          height: RFValue(35),
                          width: '100%',
                          marginTop: RFValue(10),
                          backgroundColor: 'white',
                          borderWidth: 0.5,
                          borderRadius: 3,
                          paddingHorizontal: '7%',
                          paddingVertical: 0,
                          fontSize: RFValue(12),
                        }}
                        placeholder="Input Name"
                      />
                    </View> */}
                    {/* [END Customer Name] */}

                    {/* [START Customer Detail] */}
                    <View
                      style={{
                        width: '70%',
                        // alignItems: 'center'
                      }}>
                      <Text style={{fontSize: RFValue(14), color: '#4A4A4A'}}>
                        Customer Data
                      </Text>
                      <TextInput
                        style={styles.inputStyle}
                        placeholder="Input Name"
                        placeholderTextColor={'#C6CED9'}
                        onChangeText={(text) =>
                          this.props.handleChange(text, 'input_name_customer')
                        }
                      />
                      <TextInput
                        style={styles.inputStyle}
                        placeholder="Address"
                        placeholderTextColor={'#C6CED9'}
                        onChangeText={(text) =>
                          this.props.handleChange(text, 'address_customer')
                        }
                      />
                      <TextInput
                        style={styles.inputStyle}
                        placeholder="Phone Number"
                        placeholderTextColor={'#C6CED9'}
                        onChangeText={(text) =>
                          this.props.handleChange(text, 'phone_number_customer')
                        }
                        keyboardType="phone-pad"
                      />
                      <TextInput
                        style={styles.inputStyle}
                        placeholder="Email"
                        placeholderTextColor={'#C6CED9'}
                        onChangeText={(text) =>
                          this.props.handleChange(text, 'email_customer')
                        }
                        keyboardType="email-address"
                      />
                    </View>
                    {/* [END Customer Detail] */}
                    <TouchableOpacity
                      style={{
                        width: '70%',
                        height: undefined,
                        aspectRatio: 11,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        marginTop: RFValue(20),
                        borderRadius: RFValue(3),
                        backgroundColor: '#FEBF11',
                      }}
                      onPress={this.props.handleSubmitDataCustomer}>
                      <Text style={{color: 'white'}}>Save</Text>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputStyle: {
    height: RFValue(35),
    width: '100%',
    marginTop: RFValue(10),
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderColor: '#C6CED9',
    borderRadius: 3,
    paddingHorizontal: '2%',
    paddingVertical: 0,
    fontSize: RFValue(9),
  },
});
