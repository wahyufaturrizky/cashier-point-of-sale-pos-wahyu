/* eslint-disable radix */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {
  Text,
  View,
  Modal,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import ModalHeader from '../../../View/Component/ModalHeader';
import TableService from '../../../../services/table.service';
import FloorService from '../../../../services/floor.service';

import Table from './Table';
import Floor from './Floor';
import {connect} from 'react-redux';

class DineInModal extends Component {
  constructor(props) {
    super(props);
    this.retrieveTable = this.retrieveTable.bind(this);
    this.retrieveFloor = this.retrieveFloor.bind(this);
    this.state = {
      visible: props.visible,
      tables: [],
      floors: [],
    };
  }
  componentDidMount() {
    this.retrieveTable();
    this.retrieveFloor();
    this.props.userToken;
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
  }

  retrieveTable() {
    TableService.getTable(this.props.userToken)
      .then((response) => {
        console.log('respone retrieveTable = ', response);
        this.setState({
          tables: response.data.data,
        });
      })
      .catch((e) => {
        console.log('Error respone retrieveTable = ', e);
      });
  }

  retrieveFloor() {
    FloorService.getFloor(this.props.userToken)
      .then((response) => {
        console.log('respone retrieveFloor = ', response);
        this.setState({
          floors: response.data.data,
        });
      })
      .catch((e) => {
        console.log('Error respone retrieveFloor = ', e);
      });
  }

  render() {
    const {tables, floors} = this.state;
    return (
      <Modal visible={this.state.visible} transparent={true}>
        <View
          style={{
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#000000aa',
          }}>
          <View
            style={{
              height: '90%',
              width: '90%',
              backgroundColor: 'white',
              alignItems: 'center',
              borderRadius: RFValue(2),
            }}>
            <View
              style={{
                height: '10%',
                width: '100%',
                shadowOpacity: 1,
                backgroundColor: 'white',
                elevation: 1,
              }}>
              <ModalHeader
                title="DINE IN"
                close={this.props.dineInModalSwitch}
                // save={this.props.closeAndDineIn}
              />
            </View>

            <View style={{height: '90%', width: '100%'}}>
              <ScrollView>
                <View
                  style={{
                    width: '100%',
                    alignItems: 'center',
                    marginVertical: RFValue(15),
                  }}>
                  {/* [START Customer Name] */}
                  {/* <View
                    style={{
                      height: RFValue(75),
                      width: '70%',
                      alignItems: 'center',
                    }}>
                    <Text style={{fontSize: RFValue(16)}}>Customer Name</Text>
                    <TextInput
                      style={{
                        height: RFValue(35),
                        width: '100%',
                        marginTop: RFValue(10),
                        backgroundColor: 'white',
                        borderWidth: 0.5,
                        borderRadius: 3,
                        paddingHorizontal: '7%',
                        paddingVertical: 0,
                        fontSize: RFValue(12),
                      }}
                      placeholder="Input Name"
                    />
                  </View> */}
                  {/* [END Customer Name] */}

                  {/* [START Select Floor] */}
                  <View
                    style={{width: '100%', width: '70%', alignItems: 'center'}}>
                    <Text style={{fontSize: RFValue(13)}}>SELECT FLOOR</Text>
                    <View
                      style={{
                        alignItems: 'center',
                        width: '100%',
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                      }}>
                      {console.log('floors masu ga = ', floors)}
                      {floors.map((floor, index) => (
                        <Floor
                          pressActionFloor={this.props.toggleSelectFloorDineIn}
                          name={floor.name}
                          idFloor={floor.UUID}
                          tableCapacity={parseInt(floor.table_capacity)}
                          key={index}
                        />
                      ))}
                      {/* <Floor />
                      <Floor />
                      <Floor />
                      <Floor />
                      <Floor /> */}
                    </View>
                  </View>
                  {/* [END Select Floor] */}

                  {/* [START Select table] */}
                  <View
                    style={{
                      width: '100%',
                      width: '70%',
                      alignItems: 'center',
                      marginTop: 24,
                    }}>
                    <Text style={{fontSize: RFValue(13)}}>SELECT TABLE</Text>
                    <View
                      style={{
                        alignItems: 'center',
                        width: '100%',
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                      }}>
                      {console.log(
                        'tables by Floor masuk ga = ',
                        this.props.tablesbyFloor,
                      )}
                      {this.props.tablesbyFloor.map((table, index) => (
                        <Table
                          pressActionTable={this.props.toggleSelectTableDineIn}
                          name={table.name}
                          idTable={table.UUID}
                          numberTable={table.id}
                          sitCapacity={parseInt(table.sit_capacity)}
                          key={index}
                        />
                      ))}
                      {console.log('tables masuk ga = ', tables)}
                      {tables.map((table, index) => (
                        <Table
                          pressActionTable={this.props.toggleSelectTableDineIn}
                          name={table.name}
                          idTable={table.UUID}
                          numberTable={table.id}
                          sitCapacity={parseInt(table.sit_capacity)}
                          key={index}
                        />
                      ))}
                      {/*<Table />
                      <Table />
                      <Table />
                      <Table />
                      <Table />
                      <Table />*/}
                    </View>
                  </View>
                  {/* [END Select table] */}
                </View>
              </ScrollView>

              <TouchableOpacity
                style={{
                  width: '70%',
                  height: undefined,
                  aspectRatio: 11,
                  alignItems: 'center',
                  alignSelf: 'center',
                  justifyContent: 'center',
                  marginVertical: RFValue(20),
                  borderRadius: RFValue(3),
                  backgroundColor: '#FEBF11',
                }}
                onPress={this.props.closeAndDineIn}>
                <Text style={{color: 'white'}}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userToken: state.rightFoodTransaction.tokenUser,
  };
};

export default connect(mapStateToProps)(DineInModal);
