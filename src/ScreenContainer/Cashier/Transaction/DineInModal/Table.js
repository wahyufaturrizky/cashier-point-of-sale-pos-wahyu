/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: false,
    };
  }

  render() {
    return (
      <TouchableOpacity
        style={{
          width: '18%',
          marginHorizontal: '1%',
          marginBottom: RFValue(10),
          shadowOpacity: 1,
          elevation: 2,
          height: undefined,
          aspectRatio: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white',
          elevation: 2,
        }}
        onPress={() =>
          this.setState(
            {selected: !this.state.selected},
            this.props.pressActionTable(
              this.props.idTable,
              this.props.numberTable,
            ),
          )
        }>
        {this.state.selected ? (
          <View
            style={{
              alignItems: 'center',
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              borderColor: '#6D91F0',
              borderWidth: 2,
              borderRadius: RFValue(10),
              // backgroundColor: '#FEBF11',
            }}>
            {/* <Image
              source={require('../../../../assets/General/TableUncolored.png')}
              style={{height: '60%', width: undefined, aspectRatio: 1}}
              resizeMode="contain"
            /> */}
            <Text style={{fontSize: RFValue(8), color: '#6D91F0'}}>
              {this.props.name}
            </Text>
            <Text style={{fontSize: RFValue(6), color: '#6D91F0'}}>
              {this.props.sitCapacity}
            </Text>
          </View>
        ) : (
          <View
            style={{
              alignItems: 'center',
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              backgroundColor: '#6D91F0',
              borderRadius: RFValue(10),
            }}>
            {/* <Image
              source={require('../../../../assets/General/TableColored.png')}
              style={{height: '60%', width: undefined, aspectRatio: 1}}
              resizeMode="contain"
            /> */}
            <Text style={{fontSize: RFValue(8), color: 'white'}}>
              {this.props.name}
            </Text>
            <Text style={{fontSize: RFValue(6), color: 'white'}}>
              {this.props.sitCapacity}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  }
}
