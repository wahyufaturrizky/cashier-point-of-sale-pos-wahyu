/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import NumberFormat from 'react-number-format';

export default class ModifierList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: false,
    };
  }
  render() {
    return (
      <View
        style={{
          width: '25%',
          height: undefined,
          aspectRatio: 2.5,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          style={{height: RFValue(70), width: '90%'}}
          onPress={() =>
            this.setState(
              {selected: !this.state.selected},
              this.props.pressActionModifier(
                this.props.name,
                this.props.value,
                this.props.uuidModifierMenuFood,
              ),
            )
          }>
          {this.state.selected ? (
            <View
              style={{
                height: '90%',
                backgroundColor: 'white',
                borderRadius: RFValue(3),
                borderColor: '#FE724C',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                // elevation: 3,
              }}>
              <Text
                style={{
                  fontSize: RFValue(10),
                  color: '#FE724C',
                  textAlign: 'center',
                }}>
                {this.props.name}
              </Text>
              <NumberFormat
                value={this.props.value}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp. '}
                renderText={(value) => (
                  <Text
                    style={{
                      color: '#FE724C',
                      fontSize: RFValue(8),
                      marginTop: RFValue(8),
                    }}>
                    {value}
                  </Text>
                )}
              />
            </View>
          ) : (
            <View
              style={{
                height: '90%',
                backgroundColor: '#FE724C',
                borderRadius: RFValue(3),
                justifyContent: 'center',
                alignItems: 'center',
                elevation: 3,
              }}>
              <Text
                style={{
                  fontSize: RFValue(10),
                  textAlign: 'center',
                  color: 'white',
                }}>
                {this.props.name}
              </Text>
              <NumberFormat
                value={this.props.value}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp. '}
                renderText={(value) => (
                  <Text
                    style={{
                      fontSize: RFValue(8),
                      color: '#fff',
                      marginTop: RFValue(8),
                    }}>
                    {value}
                  </Text>
                )}
              />
            </View>
          )}
        </TouchableOpacity>
      </View>
    );
  }
}
