/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import FoodMenuService from '../../../../services/foodMenu.service';

import DiscountItemList from './DiscountItemList';

export default class DiscountTime extends Component {
  constructor(props) {
    super(props);
    this.retrieveMenuDiscountFoodById = this.retrieveMenuDiscountFoodById.bind(
      this,
    );

    this.state = {
      visible: false,
      foodDiscounts: [],
      foodMenus: [],
      currentFoodMenu: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    this.retrieveMenuDiscountFoodById();
  }

  retrieveMenuDiscountFoodById() {
    // console.log("this.props.uuidFoodMenu = ", this.props.uuidFoodMenu)
    FoodMenuService.get(this.props.uuidFoodMenu)
      .then((response) => {
        this.setState({
          foodDiscounts: response.data.data.food_discount,
        });
        console.log(
          'Check Item Modal Response food_discount = ',
          response.data.data.food_discount,
        );
      })
      .catch((e) => {
        console.log('Check kalau food_discount error = ', e);
      });
  }

  render() {
    const {foodDiscounts} = this.state;
    return (
      <View
        style={{
          width: '100%',
          alignItems: 'center',
          marginBottom: RFValue(20),
          paddingHorizontal: '10%',
        }}>
        <Text style={{fontSize: RFValue(12), marginBottom: RFValue(5)}}>
          {' '}
          SELECT DISCOUNT{' '}
        </Text>
        <View
          style={{
            width: '100%',
            backgroundColor: 'white',
            flexDirection: 'row',
            flexWrap: 'wrap',
          }}>
          {foodDiscounts.map((foodDiscountsList, index) => (
            <DiscountItemList
              pressActionDiscount={this.props.pressActionDiscount}
              name={foodDiscountsList.discount.discount_name}
              value={foodDiscountsList.discount.amount}
              value={foodDiscountsList.discount.amount}
              uuidDiscountMenu={foodDiscountsList.UUID}
            />
          ))}
          {/* <DiscountItemList name="Disc Pelajar" value={30} />
          <DiscountItemList name="Disc Mahasiswa" value={20} />
          <DiscountItemList name="Disc Pelajar" value={30} />
          <DiscountItemList name="Disc Mahasiswa" value={20} /> */}
        </View>
      </View>
    );
  }
}
