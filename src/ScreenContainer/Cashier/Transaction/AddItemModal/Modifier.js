/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import ModifierList from './ModifierList';
import FoodMenuService from '../../../../services/foodMenu.service';

import {connect} from 'react-redux';

class Modifier extends Component {
  constructor(props) {
    super(props);
    this.retrieveMenuFoodById = this.retrieveMenuFoodById.bind(this);

    this.state = {
      visible: false,
      foodModifiers: [],
      currentFoodMenu: null,
      currentIndex: -1,
    };
  }

  componentWillMount() {
    console.log('componentWillMount');
    this.retrieveMenuFoodById();
  }

  componentDidMount() {
    console.log('componentDidMount');
    this.retrieveMenuFoodById();
  }

  componentDidUpdate(prevProps, PrevState) {
    console.log('componentDidUpdate');
    const {foodModifiers} = this.state;
    if (foodModifiers !== PrevState.foodModifiers) {
      this.retrieveMenuFoodById();
    }
  }

  retrieveMenuFoodById() {
    console.log(
      'this.state.token retrieveMenuFoodById = ',
      this.props.userToken,
    );
    FoodMenuService.get(this.props.uuidFoodMenu, this.props.userToken)
      .then((response) => {
        console.log('response Item Modal Response food_modifier = ', response);
        cosnno;
        this.setState({
          foodModifiers: response.data.data.food_modifier,
        });
      })
      .catch((e) => {
        console.log('Check response kalau Modifier error = ', e.request);
        console.log('Check request kalau Modifier error = ', e.request);
        console.log('Check message kalau Modifier error = ', e.message);
      });
  }

  render() {
    const {foodModifiers} = this.state;
    return (
      <View
        style={{
          width: '100%',
          alignItems: 'center',
          marginTop: RFValue(20),
          marginBottom: RFValue(20),
          paddingHorizontal: '10%',
        }}>
        <Text style={{fontSize: RFValue(12), marginBottom: RFValue(18)}}>
          {' '}
          SELECT MODIFIER{' '}
        </Text>
        <View
          style={{
            width: '100%',
            backgroundColor: 'white',
            flexDirection: 'row',
            flexWrap: 'wrap',
          }}>
          {foodModifiers.map((foodModifiersList, index) => (
            <ModifierList
              pressActionModifier={this.props.pressActionModifier}
              name={foodModifiersList.modifier.name}
              value={foodModifiersList.modifier.price}
              uuidModifierMenuFood={foodModifiersList.UUID}
              key={index}
            />
          ))}
          {/* <ModifierList name="telur" value="5.000" />
          <ModifierList name="sambal" value="5.000" />
          <ModifierList name="ayam" value="10.000" />
          <ModifierList name="sayur" value="5.000" /> */}
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // userToken: state.rightFoodTransaction.tokenUser,
  };
};

export default connect(mapStateToProps)(Modifier);
