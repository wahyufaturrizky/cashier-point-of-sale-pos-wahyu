/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  Modal,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import ModalHeader from '../../../View/Component/ModalHeader';
import FoodMenuService from '../../../../services/foodMenu.service';
import MenuCategoryItem from '../MenuCategoryItem';
import {connect} from 'react-redux';

class CategoryModal extends Component {
  constructor(props) {
    super(props);
    this.retrieveCategory = this.retrieveCategory.bind(this);
    this.state = {
      visible: props.visible,
      categories: [],
      currentIndex: -1,
    };
  }

  componentDidMount() {
    this.retrieveCategory();
    this.props.userToken;
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
  }

  retrieveCategory() {
    FoodMenuService.getCategory(this.props.userToken)
      .then((response) => {
        this.setState({
          categories: response.data.data,
        });
        console.log(
          'response ini semua kategory Menunya = ',
          response.data.data,
        );
      })
      .catch((e) => {
        console.log('Error response ini semua kategory Menunya = ', e.response);
      });
  }

  render() {
    const {categories, currentIndex} = this.state;
    return (
      <Modal visible={this.state.visible} transparent={true}>
        <View
          style={{
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#000000aa',
          }}>
          <View
            style={{
              height: '90%',
              width: '90%',
              backgroundColor: 'white',
              alignItems: 'center',
              borderRadius: RFValue(2),
            }}>
            <View
              style={{
                height: '10%',
                width: '100%',
                shadowOpacity: 1,
                backgroundColor: 'white',
                elevation: 1,
              }}>
              <ModalHeader
                title="CATEGORY"
                close={this.props.categoryModalSwitch}
                // save={this.props.categoryModalSwitch}
              />
            </View>

            <View style={{height: '90%', width: '100%'}}>
              <View
                style={{
                  height: '100%',
                  width: '100%',
                  alignItems: 'center',
                  marginVertical: RFValue(15),
                  paddingBottom: '10%',
                }}>
                <View
                  style={{
                    height: '15%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={{fontSize: RFValue(15)}}>SELECT ORDER TYPE</Text>
                </View>

                <ScrollView>
                  <View
                    style={{
                      width: '100%',
                      marginVertical: '2%',
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                    }}>
                    {/* {console.log('ini foodMenus', foodMenus[0])}
                    {console.log('foodsMenuFilter', foodsMenuFilter)} */}
                    {categories.map((category, index) => (
                      <MenuCategoryItem
                        // onPress={(event)=> {
                        // {console.log('MenuCategoryItem', categories)}
                        // uuid=categories.UUID
                        // {console.log('uuid_MenuCategoryItem',uuid)}
                        // const {navigate} = this.props.navigation;
                        // navigate('UUID', {uuid: uuid});
                        // }}
                        className={
                          'list-group-item' +
                          (index === currentIndex ? 'active' : '')
                        }
                        foodCategory={category.category_name}
                        pressActionCategory={this.props.categoryModalSwitch}
                        uuidCategoryMenu={category.UUID}
                        onClick={() =>
                          this.props.setActiveFoodMenu(category, index)
                        }
                        key={index}
                      />
                    ))}
                  </View>
                </ScrollView>

                {/* <View
                  style={{
                    height: '60%',
                    width: '75%',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                  }}>
                  categories.map((category,index)=>())
                  <TouchableOpacity
                    style={{
                      width: '25%',
                      height: undefined,
                      aspectRatio: 1,
                      borderRadius: 10,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                      shadowOpacity: 1,
                      elevation: 3,
                    }}>
                    <Image
                      source={require('../../../../assets/General/mealIcon.png')}
                      resizeMode="contain"
                      style={{
                        width: '50%',
                        height: undefined,
                        aspectRatio: 1,
                        marginBottom: '10%',
                      }}
                    />
                    <Text style={{fontSize: RFValue(15)}}>MEAL</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: '25%',
                      height: undefined,
                      aspectRatio: 1,
                      borderRadius: 10,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                      shadowOpacity: 1,
                      elevation: 3,
                    }}>
                    <Image
                      source={require('../../../../assets/General/mealIcon.png')}
                      resizeMode="contain"
                      style={{
                        width: '50%',
                        height: undefined,
                        aspectRatio: 1,
                        marginBottom: '10%',
                      }}
                    />
                    <Text style={{fontSize: RFValue(15)}}>BEVERAGE</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: '25%',
                      height: undefined,
                      aspectRatio: 1,
                      borderRadius: 10,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                      shadowOpacity: 1,
                      elevation: 3,
                    }}>
                    <Image
                      source={require('../../../../assets/General/mealIcon.png')}
                      resizeMode="contain"
                      style={{
                        width: '50%',
                        height: undefined,
                        aspectRatio: 1,
                        marginBottom: '10%',
                      }}
                    />
                    <Text style={{fontSize: RFValue(15)}}>SNACK</Text>
                  </TouchableOpacity>
                </View> */}
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userToken: state.rightFoodTransaction.tokenUser,
  };
};

export default connect(mapStateToProps)(CategoryModal);
