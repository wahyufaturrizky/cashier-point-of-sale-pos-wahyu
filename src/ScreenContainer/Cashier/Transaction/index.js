/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Text, View, TextInput, TouchableOpacity} from 'react-native';
import CashierNavigator from '../../Cashier/CashierNavigator';
import LeftTransaction from './LeftTransaction';
import RightTransaction from './RightTransaction';
import CustomerDetailDineInModal from './CustomerDetailDineInModal';
import DineInModal from './DineInModal';
import TakeawayModal from './TakeawayModal';
import DeliveryModal from './DeliveryModal';
import CategoryModal from './CategoryModal';
import AddItemModal from './AddItemModal';
import CustomerService from '../../../services/customerData.service';
import RunningOrderService from '../../../services/runningOrderData.service';
import RunningPrintService from '../../../services/runningPrint.service';
import FoodMenuService from '../../../services/foodMenu.service';
import TableService from '../../../services/table.service';

import {
  getAllDataTransaction,
  getAllDatasubTotal,
  getAllDataDiscount,
  getAllDataCustomer,
  getAllDataOrderIdPrinting,
  // getAllCategoryMenuId
} from '../../../redux/actions/transaction/rightFoodTransactionAction';
import {bindActionCreators} from 'redux';
import EscPos from '@leesiongchan/react-native-esc-pos';

// const designKot = `
//         KOT
// [ ] Order No : ${this.state.data_kot.table_id}
// [ ] Date : ${this.state.data_kot.date}
// [ ] Customer : ${this.state.data_kot.customer}
// [ ] Order Type :
// [ ] Waiter :
// [ ] SN Item Qty
//         ${this.state.data_kot.details[0].menu_name}    x ${this.state.data_kot.details[0].qty}
// `;

class Transaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customerDetailDineInModalVisible: false,
      dineInModalVisible: false,
      takeawayModalVisible: false,
      deliveryModalVisible: false,
      addItemModalVisible: false,
      isCustomerDetailDineIn: false,
      isTakeaway: false,
      isDelivery: false,
      isDineIn: false,
      categoryModalVisible: false,
      uuidCategoryMenu: null,
      foodItem: null,
      priceItem: null,
      input_name_customer: '',
      phone_number_customer: '',
      email_customer: '',
      address_customer: '',
      data_customer_detail: '',
      selected: false,
      tableId: null,
      floorId: null,
      numberTable: null,
      allTransactionMenu: {},
      foodMenuData: [],
      allAddModifierMenu: [],
      allAddDiscountMenu: [],
      quantityMenuFood: 1,
      subTotal: null,
      subTotalModifier: null,
      RunningOrderDataStatus: '',
      customerDetailDineInStatus: '',
      customerDetailTakeAwayStatus: '',
      customerDetailDineInData: '',
      customerDetailTakeAwayData: '',
      RunningOrderDatauuid: '',
      data_customer_take_away: '',
      input_name_customer_takeaway: '',
      address_customer_takeaway: '',
      phone_number_customer_takeaway: '',
      email_customer_takeaway: '',
      data_customer_delivery: '',
      input_name_customer_delivery: '',
      address_customer_delivery: '',
      phone_number_customer_delivery: '',
      email_customer_delivery: '',
      uuidFoodMenu: null,
      modifierFoodMenu: null,
      type_order: null,
      name_discount: null,
      amount_discount: null,
      uuid_discount: null,
      customerDetailDineInDataId: null,
      foodMenuId: null,
      foodMenusFilter: [],
      hot_detail_id_running_order: null,
      data_calculate_sales_order: '',
      data_calculate_sales_order_detail: [],
      print_kot_status: '',
      data_kot: '',
      ip_address_kot_sucess: '',
      tablesbyFloor: [],
    };
    // this.child = React.createRef()
  }

  async componentDidMount() {
    this.props.userToken;
  }

  toggleSelectTableDineIn = (idTable, numberTable) => {
    this.setState(
      {
        selected: !this.state.selected,
        tableId: idTable,
        numberTable: numberTable,
      },
      () =>
        console.log(
          'Meja yang aku pilih = ',
          this.state.numberTable,
          'and ID table = ',
          this.state.tableId,
        ),
    );
  };

  toggleSelectFloorDineIn = (idFloor) => {
    this.setState(
      {
        selected: !this.state.selected,
        floorId: idFloor,
      },
      () => console.log('and ID table = ', this.state.floorId),
    );

    TableService.getTableByFloor(this.props.userToken, idFloor)
      .then((response) => {
        console.log('respone retrieveTable by Floor = ', response);
        this.setState({
          tablesbyFloor: response.data.data.floor_table,
        });
      })
      .catch((e) => {
        console.log('Error respone retrieveTable by Floor = ', e);
      });
  };

  handleChange = (text, name) => {
    console.log('Yang diketik di', name, ' =', text);
    this.setState({
      [name]: text,
    });
  };

  handleSubmitDataCustomer = () => {
    const DataCustomerDetail = {
      name: this.state.input_name_customer,
      phone: this.state.phone_number_customer,
      email: this.state.email_customer,
      address: this.state.address_customer,
    };
    this.setState(
      {
        data_customer_detail: DataCustomerDetail,
      },
      () => this.props.getAllDataCustomer(this.state.data_customer_detail),
    );

    // const getAllDataCustomerDetail = this.state.data_customer_detail;
    // console.log("createCustomer data coba check = ", this.state.input_name_customer, this.state.phone_number_customer, this.state.email_customer, this.state.address_customer)

    CustomerService.createCustomer(
      this.state.input_name_customer,
      this.state.phone_number_customer,
      this.state.email_customer,
      this.state.address_customer,
      this.props.userToken,
    )
      .then((response) => {
        this.setState({
          customerDetailDineInStatus: response.data.message,
          customerDetailDineInData: response.data.data,
          customerDetailDineInDataId: response.data.data.id,
        });
        console.log('customerDetailDineInStatus = ', response.data.message);
        console.log('customerDetailDineInData = ', response.data.data);
      })
      .catch((e) => {
        console.log('Error create customer = ', e.response);
      });

    this.dineInModalSwitch();
  };

  handleSubmitDataCustomerTakeAway = () => {
    const DataCustomerTakeAway = {
      input_name_customer_takeaway: this.state.input_name_customer_takeaway,
      address_customer_takeaway: this.state.address_customer_takeaway,
      phone_number_customer_takeaway: this.state.phone_number_customer_takeaway,
      email_customer_takeaway: this.state.email_customer_takeaway,
    };
    this.setState(
      {
        data_customer_take_away: DataCustomerTakeAway,
      },
      () => this.props.getAllDataCustomer(this.state.data_customer_take_away),
    );

    CustomerService.createCustomer(
      this.state.input_name_customer,
      this.state.phone_number_customer,
      this.state.email_customer,
      this.state.address_customer,
      this.props.userToken,
    )
      .then((response) => {
        this.setState({
          customerDetailTakeAwayStatus: response.data.message,
          customerDetailTakeAwayData: response.data.data,
        });
        console.log('customerDetailTakeAwayStatus = ', response.data.message);
        console.log('customerDetailTakeAwayData = ', response.data.data);
      })
      .catch((e) => {
        console.log('Error create customer = ', e.response);
      });

    this.closeAndTakeaway();
  };

  handleSubmitDataCustomerDelivery = () => {
    const DataCustomerDelivery = {
      input_name_customer_delivery: this.state.input_name_customer_delivery,
      address_customer_delivery: this.state.address_customer_delivery,
      phone_number_customer_delivery: this.state.phone_number_customer_delivery,
      email_customer_delivery: this.state.email_customer_delivery,
    };
    this.setState(
      {
        data_customer_delivery: DataCustomerDelivery,
      },
      () => this.props.getAllDataCustomer(this.state.data_customer_delivery),
    );

    CustomerService.createCustomer(
      this.state.input_name_customer,
      this.state.phone_number_customer,
      this.state.email_customer,
      this.state.address_customer,
      this.props.userToken,
    )
      .then((response) => {
        this.setState({
          customerDetailDeliveryStatus: response.data.message,
          customerDetailDeliveryData: response.data.data,
        });
        console.log('customerDetailDeliveryStatus = ', response.data.message);
        console.log('customerDetailDeliveryData = ', response.data.data);
      })
      .catch((e) => {
        console.log('Error create customer = ', e.response);
      });

    this.closeAndDelivery();
  };

  createRunningOrder = () => {
    RunningOrderService.createRunningOrderData(
      this.state.type_order,
      this.state.floorId,
      this.state.customerDetailDineInDataId,
      this.state.numberTable,
      this.state.foodMenuData,
      this.props.userToken,
    )
      .then((response) => {
        console.log('createRunningOrderData Status = ', response);
        this.setState(
          {
            RunningOrderDataStatus: response.data.message,
            RunningOrderDatauuid: response.data.data.running_order_id,
            hot_detail_id_running_order: response.data.data.hold_id,
          },
          () =>
            this.props.getAllDataOrderIdPrinting(
              this.state.RunningOrderDatauuid,
            ),
        );
        console.log(
          'createRunningOrderData = ',
          this.state.RunningOrderDatauuid,
        );

        // [START] createPrintBill
        RunningPrintService.createPrintBill(
          this.state.hot_detail_id_running_order,
          this.props.userToken,
          this.state.discount_id,
        )
          .then((response) => {
            console.log(
              'response Success get calculate sales order = ',
              response,
            );
            this.setState({
              data_calculate_sales_order: response.data.data,
              data_calculate_sales_order_detail: response.data.data.details,
            });

            // [START] create Printing KOT
            RunningPrintService.createPrintKot(
              this.state.RunningOrderDatauuid,
              this.props.userToken,
            )
              .then((response) => {
                console.log('response kot = ', response);
                this.setState(
                  {
                    print_kot_status: response.data.message,
                    data_kot: response.data.data,
                    ip_address_kot_sucess: response.data.data.ip_address,
                  },
                  () => this.printingProcessKotOrder(),
                );
              })
              .catch((e) => {
                console.log('Error data_kot = ', e.response);
              });

            // [END] create Printing KOT
          })
          .catch((e) => {
            console.log('Error get calculate sales order = ', e.response);
          });
      })
      .catch((e) => {
        console.log('Error create createRunningOrderData = ', e.response);
      });
  };

  printingProcessKotOrder = async () => {
    const {ip_address_kot_sucess, data_kot, table_id, date} = this.state;
    console.log(
      'this.state.ip_address_kot_sucess = ',
      ip_address_kot_sucess,
      'dan this.state.data_kot = ',
      data_kot,
    );

    const designDataKTO = ``;

    try {
      EscPos.setConfig({type: 'network'});

      await EscPos.connect(this.state.ip_address_kot_sucess, 9100);
      console.log(
        'EscPos.connect = ',
        await EscPos.connect(this.state.ip_address_kot_sucess, 9100),
      );
      EscPos.setPrintingSize(EscPos.PRINTING_SIZE_80_MM);
      EscPos.setTextDensity(8);
      // await EscPos.printDesign(this.state.data_kot);
      await EscPos.printDesign(
        'KOT [ ] Order No :',
        data_kot.table_id,
        '[ ] Date :',
        data_kot.date,
        '[ ] Customer :',
        data_kot.customer,
        '[ ] Order Type : [ ] Waiter :  [ ] SN Item Qty',
        data_kot.details[0].menu_name,
        'x',
        data_kot.details[0].qty,
      );
      await EscPos.cutFull();
      console.log('EscPos.cutFull KOT = ', await EscPos.cutFull());
      await EscPos.beep();
      console.log('EscPos.beep() KOT = ', await EscPos.beep());
      //await EscPos.kickDrawerPin();
      await EscPos.disconnect();
      console.log('EscPos.disconnect KOT = ', await EscPos.disconnect());
    } catch (error) {
      console.error('Eror Printer KOT = ', error);
    }
  };

  render() {
    console.log('woi redux = ', this.props);
    return (
      <View style={{flex: 1, flexDirection: 'row'}}>
        <View
          style={{
            flex: 85,
            borderColor: '#DADADA',
            shadowOpacity: 0.5,
            shadowRadius: 5,
            borderRight: 1,
          }}>
          <CashierNavigator
            cartStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View style={{flex: 640}}>
          <LeftTransaction
            // ref={this.child}
            foodMenusFilter={this.state.foodMenusFilter}
            categoryModalSwitch={this.categoryModalSwitch}
            addItemModalSwitch={this.addItemModalSwitch}
          />
        </View>
        <View style={{flex: 555}}>
          <RightTransaction
            navigation={this.props.navigation}
            customerDetailDineInModalSwitch={
              this.customerDetailDineInModalSwitch
            }
            isDineIn={this.state.isDineIn}
            // isCustomerDetailDineIn={this.state.isCustomerDetailDineIn}
            takeawayModalSwitch={this.takeawayModalSwitch}
            isTakeaway={this.state.isTakeaway}
            deliveryModalSwitch={this.deliveryModalSwitch}
            isDelivery={this.state.isDelivery}
            allTransactionMenu={this.state.allTransactionMenu}
            subTotal={this.state.subTotal}
            subTotalModifier={this.state.subTotalModifier}
            amountDiscount={this.state.amount_discount}
            createRunningOrder={this.createRunningOrder}
          />
        </View>
        <CustomerDetailDineInModal
          visible={this.state.customerDetailDineInModalVisible}
          customerDetailDineInModalSwitch={this.customerDetailDineInModalSwitch}
          toggleCustomerDetailDineIn={this.toggleCustomerDetailDineIn}
          closeAndCustomerDetailDineIn={this.closeAndCustomerDetailDineIn}
          dineInModalSwitch={this.dineInModalSwitch}
          handleChange={this.handleChange}
          handleSubmitDataCustomer={this.handleSubmitDataCustomer}
          state={this.state}
        />
        {/* [START DINE IN MODAL] */}
        <DineInModal
          tablesbyFloor={this.state.tablesbyFloor}
          visible={this.state.dineInModalVisible}
          dineInModalSwitch={this.dineInModalSwitch}
          toggleDineIn={this.toggleDineIn}
          toggleSelectTableDineIn={this.toggleSelectTableDineIn}
          toggleSelectFloorDineIn={this.toggleSelectFloorDineIn}
          closeAndDineIn={this.closeAndDineIn}
        />
        {/* [END DINE IN MODAL] */}
        <TakeawayModal
          visible={this.state.takeawayModalVisible}
          takeawayModalSwitch={this.takeawayModalSwitch}
          toggleTakeaway={this.toggleTakeaway}
          handleChange={this.handleChange}
          // closeAndTakeaway={this.closeAndTakeaway}
          handleSubmitDataCustomerTakeAway={
            this.handleSubmitDataCustomerTakeAway
          }
        />
        <DeliveryModal
          visible={this.state.deliveryModalVisible}
          deliveryModalSwitch={this.deliveryModalSwitch}
          toggleDelivery={this.toggleDelivery}
          handleChange={this.handleChange}
          // closeAndDelivery={this.closeAndDelivery}
          handleSubmitDataCustomerDelivery={
            this.handleSubmitDataCustomerDelivery
          }
        />
        <CategoryModal
          visible={this.state.categoryModalVisible}
          categoryModalSwitch={this.categoryModalSwitch}
        />
        <AddItemModal
          userToken={this.props.userToken}
          visible={this.state.addItemModalVisible}
          addItemModalSwitch={this.addItemModalSwitch}
          foodItem={this.state.foodItem}
          addItemMenuOnTransaction={this.addItemMenuOnTransaction}
          addQuantityMenuFood={this.addQuantityMenuFood}
          addModifierMenuFood={this.addModifierMenuFood}
          addDiscountMenuFood={this.addDiscountMenuFood}
          uuidFoodMenu={this.state.uuidFoodMenu}
        />
      </View>
    );
  }
  customerDetailDineInModalSwitch = () => {
    this.state.customerDetailDineInModalVisible
      ? this.setState({customerDetailDineInModalVisible: false})
      : this.setState({customerDetailDineInModalVisible: true});
  };
  toggleCustomerDetailDineIn = () => {
    this.state.isCustomerDetailDineIn
      ? this.setState({isCustomerDetailDineIn: false})
      : this.setState({isCustomerDetailDineIn: true});
  };
  closeAndCustomerDetailDineIn = () => {
    this.customerDetailDineInModalSwitch();
    this.toggleCustomerDetailDineIn();
    this.state.isCustomerDetailDineIn = !this.state.isTakeaway
      ? null
      : this.toggleTakeaway();
    this.state.isCustomerDetailDineIn = !this.state.isDelivery
      ? null
      : this.toggleDelivery();
    this.state.isCustomerDetailDineIn = !this.state.isDineIn
      ? null
      : this.toggleDineIn();
  };

  takeawayModalSwitch = () => {
    this.state.takeawayModalVisible
      ? this.setState({takeawayModalVisible: false})
      : this.setState({takeawayModalVisible: true});
  };
  toggleTakeaway = () => {
    this.state.isTakeaway
      ? this.setState({isTakeaway: false})
      : this.setState({isTakeaway: true});
  };
  closeAndTakeaway = () => {
    console.log('this.state.isTakeaway =', this.state.isTakeaway);
    this.state.isTakeaway
      ? this.setState({isTakeaway: false, type_order: 2})
      : this.setState({isTakeaway: true, type_order: 2});

    this.takeawayModalSwitch();
    this.toggleTakeaway();
    this.state.isTakeaway = !this.state.isCustomerDetailDineIn
      ? null
      : this.toggleCustomerDetailDineIn();
    this.state.isTakeaway = !this.state.isDelivery
      ? null
      : this.toggleDelivery();
  };
  deliveryModalSwitch = () => {
    this.state.deliveryModalVisible
      ? this.setState({deliveryModalVisible: false})
      : this.setState({deliveryModalVisible: true});
  };
  toggleDelivery = () => {
    this.state.isDelivery
      ? this.setState({isDelivery: false})
      : this.setState({isDelivery: true});
  };
  closeAndDelivery = () => {
    console.log('this.state.isDelivery =', this.state.isDelivery);
    this.state.isDelivery
      ? this.setState({isDelivery: false, type_order: 3})
      : this.setState({isDelivery: true, type_order: 3});

    this.deliveryModalSwitch();
    this.toggleDelivery();
    this.state.isDelivery = !this.state.isCustomerDetailDineIn
      ? null
      : this.toggleCustomerDetailDineIn();
    this.state.isDelivery = !this.state.isTakeaway
      ? null
      : this.toggleTakeaway();
  };

  categoryModalSwitch = (uuidCategoryMenu) => {
    console.log('check ada ga = ', uuidCategoryMenu);
    this.state.categoryModalVisible
      ? this.setState({categoryModalVisible: false, uuidCategoryMenu: null})
      : this.setState({
          categoryModalVisible: true,
          uuidCategoryMenu: uuidCategoryMenu,
        });

    FoodMenuService.getItemModal(uuidCategoryMenu, this.props.userToken)
      .then((response) => {
        this.setState({
          foodMenusFilter: response.data.data.food_category,
        });
        console.log(
          'Cek Semua Kategory Menu Bro = ',
          this.state.foodMenusFilter,
        );
      })
      .catch((e) => {
        console.log(e);
        console.log('Cek Kalo Kategory nya Error =', e);
      });
  };

  addItemModalSwitch = (foodItemName, priceItem, uuidFoodMenu, foodMenuId) => {
    console.log(
      'addItemModalSwitch check data = ',
      foodItemName,
      priceItem,
      uuidFoodMenu,
      foodMenuId,
    );
    this.state.addItemModalVisible
      ? this.setState({
          addItemModalVisible: false,
          foodItem: foodItemName,
          priceItem: priceItem,
          uuidFoodMenu: uuidFoodMenu,
          foodMenuId: foodMenuId,
        })
      : this.setState({
          addItemModalVisible: true,
          foodItem: foodItemName,
          priceItem: priceItem,
          uuidFoodMenu: uuidFoodMenu,
          foodMenuId: foodMenuId,
        });
  };

  addItemMenuOnTransaction = () => {
    console.log(
      'modifier_menu: this.state.allAddModifierMenu = ',
      this.state.allAddModifierMenu,
    );
    const foodMenuData = {
      // [Struktur yang lama]
      // uuid_food_menu: this.state.uuidFoodMenu,
      // food_name: this.state.foodItem,
      // quantity_food: this.state.quantityMenuFood,
      // price_food: this.state.priceItem * this.state.quantityMenuFood,
      // food_modifier: this.state.allAddModifierMenu,
      // food_discount: this.state.getAllDataDiscount,

      // [Struktur yang baru]
      id: this.state.foodMenuId,
      food_name: this.state.foodItem,
      quantity: this.state.quantityMenuFood,
      price_food: this.state.priceItem * this.state.quantityMenuFood,
      food_modifier: this.state.allAddModifierMenu,
      food_discount: this.state.getAllDataDiscount,
    };

    this.state.addItemModalVisible
      ? this.setState(
          {
            addItemModalVisible: false,
            foodMenuData: [...this.state.foodMenuData, foodMenuData],
          },
          // ()=> console.log("allTransactionMenu = ", this.state.allTransactionMenu)
          () => {
            const dataAllTransactionMenu = {
              type_order: this.state.type_order,
              table_id: this.state.numberTable,
              floor_id: this.state.floorId,
              customer_id: this.state.customerDetailDineInDataId,
              numberTable: this.state.numberTable,
              foodMenuData: this.state.foodMenuData,
            };
            this.setState({allTransactionMenu: dataAllTransactionMenu}, () => {
              console.log(
                'allTransactionMenu data = ',
                this.state.allTransactionMenu,
              );
            });
            // console.log("Broll Brooll check masuk Data Sanskuy = ", this.state.allTransactionMenu);
            this.props.getAllDataTransaction(dataAllTransactionMenu);
            let data = [...this.state.foodMenuData];
            // this.state.allTransactionMenu.map((testModifier, index) => (
            //     testModifier.modifier_menu.map((dataModifier, index) => (
            //         console.log("test data modifier = ", dataModifier.name_modifier)
            //     ))
            // ));
            // console.log("allTransactionMenu = ", this.state.allTransactionMenu),
            let subTotal = 0;
            for (let i = 0; i < data.length; i++) {
              subTotal += data[i].price_food;
            }
            this.setState(
              {
                subTotal: subTotal,
              },
              () => this.props.getAllDatasubTotal(this.state.subTotal),
            );
          },
        )
      : this.setState(
          {
            addItemModalVisible: true,
            foodMenuData: [...this.state.foodMenuData, foodMenuData],
          },
          () => {
            const dataAllTransactionMenu = {
              type_order: this.state.type_order,
              table_id: this.state.numberTable,
              floor_id: this.state.floorId,
              customer_id: this.state.customerDetailDineInDataId,
              numberTable: this.state.numberTable,
              foodMenuData: this.state.foodMenuData,
            };
            this.setState({allTransactionMenu: dataAllTransactionMenu}, () => {
              console.log(
                'allTransactionMenu data = ',
                this.state.allTransactionMenu,
              );
            });

            this.props.getAllDataTransaction(dataAllTransactionMenu);
            let data = [...this.state.foodMenuData];
            console.log('data masuk ga = ', data);
            // console.log("allTransactionMenu = ", this.state.allTransactionMenu),
            let subTotal = 0;
            for (let i = 0; i < data.length; i++) {
              subTotal += data[i].price_food;
            }
            console.log('subTotal = ', subTotal);
            this.setState(
              {
                subTotal: subTotal,
              },
              () => this.props.getAllDatasubTotal(this.state.subTotal),
            );
          },
        );
  };

  addQuantityMenuFood = (quantityMenuFood) => {
    this.setState(
      {
        quantityMenuFood: quantityMenuFood,
      },
      () => console.log('quantityMenuFood = ', this.state.quantityMenuFood),
    );
  };

  addModifierMenuFood = (
    modifierMenuNameFood,
    modifierMenuPriceFood,
    modifierMenuuuidFood,
  ) => {
    const getAllDataModifier = {
      name_modifier: modifierMenuNameFood,
      price_modifier: modifierMenuPriceFood,
      uuid_modifier: modifierMenuuuidFood,
    };

    this.setState(
      {
        allAddModifierMenu: [
          ...this.state.allAddModifierMenu,
          getAllDataModifier,
        ],
      },

      () =>
        console.log(
          'modifierMenuFood yuk cek Sanssskuy = ',
          this.state.allAddModifierMenu,
        ),
    );

    let dataModfier = [...this.state.allAddModifierMenu];
    let subTotalModifier = this.state.subTotalModifier;
    for (let i = 0; i < dataModfier.length; i++) {
      subTotalModifier += dataModfier[i].price_modifier;
    }
    console.log('subTotalModifier = ', subTotalModifier);
    this.setState({
      subTotalModifier: subTotalModifier,
    });
  };

  addDiscountMenuFood = (
    discountMenuNameFood,
    discountMenuPriceFood,
    discountuuidFood,
  ) => {
    this.setState(
      {
        name_discount: discountMenuNameFood,
        amount_discount: discountMenuPriceFood,
        uuid_discount: discountuuidFood,
      },
      () => this.props.getAllDataDiscount(this.state.amount_discount),
    );

    const getAllDataDiscount = {
      name_discount: this.state.name_discount,
      amount_discount: this.state.amount_discount,
      uuid_discount: this.state.uuid_discount,
    };

    this.setState(
      {
        allAddDiscountMenu: [
          ...this.state.allAddDiscountMenu,
          getAllDataDiscount,
        ],
      },

      () =>
        console.log(
          'allAddDiscountMenu All Data sanskuy = ',
          this.state.allAddDiscountMenu,
        ),
    );
  };

  // [START] Fungsi untuk DINE IN Modal

  dineInModalSwitch = () => {
    this.state.dineInModalVisible
      ? this.setState({dineInModalVisible: false})
      : this.setState({dineInModalVisible: true}) &&
        this.state.customerDetailDineInModalVisible
      ? this.setState({customerDetailDineInModalVisible: true})
      : this.setState({customerDetailDineInModalVisible: false});
  };

  toggleDineIn = () => {
    this.state.isDineIn
      ? this.setState({isDineIn: false})
      : this.setState({isDineIn: true});
  };

  closeAndDineIn = () => {
    console.log('this.state.isDineIn =', this.state.isDineIn);
    this.state.isDineIn
      ? this.setState({isDineIn: false, type_order: 1})
      : this.setState({isDineIn: true, type_order: 1});

    this.dineInModalSwitch();
    this.toggleDineIn();
    this.state.isDineIn = !this.state.isTakeaway ? null : this.toggleTakeaway();
    this.state.isDineIn = !this.state.isDelivery ? null : this.toggleDelivery();
    this.state.isDineIn = !this.state.isCustomerDetailDineIn
      ? null
      : this.toggleCustomerDetailDineIn();
  };

  // [END] Fungsi untuk DINE IN Modal
}

const mapStateToProps = (state) => {
  return {
    testRedux: state.rightFoodTransaction.testRedux,
    userToken: state.rightFoodTransaction.tokenUser,
    all_data_id_order_printing:
      state.rightFoodTransaction.all_data_id_order_printing,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getAllDataTransaction,
      getAllDatasubTotal,
      getAllDataDiscount,
      getAllDataCustomer,
      getAllDataOrderIdPrinting,
      // getAllCategoryMenuId
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(Transaction);
