import React, {Component} from 'react';
import {
  Text,
  View,
  Modal,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import ModalHeader from '../../../View/Component/ModalHeader';
import {ScrollView} from 'react-native-gesture-handler';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';

export default class DeliveryModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
    };
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
  }
  render() {
    let radio_props = [
      {label: 'Go Food', value: 0},
      {label: 'Grab Food', value: 1},
      {label: 'Lala Move', value: 3},
    ];
    return (
      <Modal visible={this.state.visible} transparent={true}>
        <View
          style={{
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#000000aa',
          }}>
          <View
            style={{
              height: '90%',
              width: '90%',
              backgroundColor: 'white',
              alignItems: 'center',
              borderRadius: RFValue(2),
            }}>
            <View
              style={{
                height: '10%',
                width: '100%',
                shadowOpacity: 1,
                backgroundColor: 'white',
                elevation: 1,
              }}>
              <ModalHeader
                title="CUSTOMER DETAIL - DELIVERY"
                close={this.props.deliveryModalSwitch}
                // save={this.props.closeAndDelivery}
                // save={this.props.handleSubmitDataCustomerDelivery}
              />
            </View>

            <View style={{height: '90%', width: '100%'}}>
              <ScrollView>
                <View
                  style={{
                    width: '100%',
                    alignItems: 'center',
                    marginVertical: RFValue(15),
                  }}>
                  <View
                    style={{
                      width: '70%',
                      // alignItems: 'center'
                    }}>
                    <Text
                      style={{
                        fontSize: RFValue(14),
                        color: '#4A4A4A',
                        marginBottom: RFValue(8),
                      }}>
                      Delivery System
                    </Text>
                    <RadioForm
                      radio_props={radio_props}
                      initial={0}
                      buttonColor={'#33C15D'}
                      animation
                      onPress={(value) => {
                        this.setState({value: value});
                      }}
                    />
                  </View>
                </View>
                <View
                  style={{
                    width: '100%',
                    alignItems: 'center',
                    marginVertical: RFValue(15),
                  }}>
                  <View
                    style={{
                      width: '70%',
                      // alignItems: 'center'
                    }}>
                    <Text style={{fontSize: RFValue(14), color: '#4A4A4A'}}>
                      Customer Data
                    </Text>
                    <TextInput
                      style={styles.inputStyle}
                      placeholder="Input Name"
                      onChangeText={(text) =>
                        this.props.handleChange(
                          text,
                          'input_name_customer_delivery',
                        )
                      }
                    />
                    <TextInput
                      style={styles.inputStyle}
                      placeholder="Address"
                      onChangeText={(text) =>
                        this.props.handleChange(
                          text,
                          'address_customer_delivery',
                        )
                      }
                    />
                    <TextInput
                      style={styles.inputStyle}
                      placeholder="Phone Number"
                      keyboardType="name-phone-pad"
                      onChangeText={(text) =>
                        this.props.handleChange(
                          text,
                          'phone_number_customer_delivery',
                        )
                      }
                    />
                    <TextInput
                      style={styles.inputStyle}
                      placeholder="Email"
                      keyboardType="email-address"
                      onChangeText={(text) =>
                        this.props.handleChange(text, 'email_customer_delivery')
                      }
                    />
                  </View>
                </View>
              </ScrollView>

              <TouchableOpacity
                style={{
                  width: '70%',
                  height: undefined,
                  aspectRatio: 11,
                  alignItems: 'center',
                  alignSelf: 'center',
                  justifyContent: 'center',
                  marginVertical: RFValue(20),
                  borderRadius: RFValue(3),
                  backgroundColor: '#FEBF11',
                }}
                onPress={this.props.closeAndDineIn}>
                <Text style={{color: 'white'}}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  inputStyle: {
    height: RFValue(35),
    width: '100%',
    marginTop: RFValue(10),
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderColor: '#C6CED9',
    borderRadius: 3,
    paddingHorizontal: '2%',
    paddingVertical: 0,
    fontSize: RFValue(9),
  },
});
