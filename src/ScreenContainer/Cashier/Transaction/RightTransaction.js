/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from 'react-native';
import RightTransactionHeader from './RightTransactionHeader';
import {RFValue} from 'react-native-responsive-fontsize';
import NumberFormat from 'react-number-format';

import InvoiceList from '../../View/Component/InvoiceList';
import InvoiceBill from '../../View/Component/InvoiceBill';
import InvoiceListTotal from '../../View/Component/InvoiceListTotal';
import DeleteMenuModal from './DeleteMenuModal';
import Cart from '../../View/Component/Cart';
import {connect} from 'react-redux';
import RunningPrintService from '../../../services/runningPrint.service';

class RightTransaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
      tables: [],
      input_name_customer: '',
      address_customer: '',
      phone_numer_customer: '',
      email_customer: '',
      test_masuk_ga: '',
      onloading: 0,
      deleteModalVisible: false,
      isDelete: false,
    };
  }

  deleteMenuModalSwitch = () => {
    this.state.deleteModalVisible
      ? this.setState({deleteModalVisible: false})
      : this.setState({deleteModalVisible: true});
  };

  toggleDeleteMenu = () => {
    this.state.isDelete
      ? this.setState({isDelete: false})
      : this.setState({isDelete: true});
  };

  closeAndDeleteMenu = () => {
    this.props.navigation.navigate('Transaction');
  };

  render() {
    return (
      <View style={{width: undefined, height: undefined}}>
        <View
          style={{
            paddingHorizontal: '2%',
            height: '13.8%',
            backgroundColor: 'white',
          }}>
          <RightTransactionHeader
            customerDetailDineInModalSwitch={
              this.props.customerDetailDineInModalSwitch
            }
            isDineIn={this.props.isDineIn}
            // isCustomerDetailDineIn={this.props.isCustomerDetailDineIn}
            takeawayModalSwitch={this.props.takeawayModalSwitch}
            isTakeaway={this.props.isTakeaway}
            deliveryModalSwitch={this.props.deliveryModalSwitch}
            isDelivery={this.props.isDelivery}
          />
        </View>

        <View
          style={{
            height: '86.14%',
            marginTop: '2.526%',
            backgroundColor: 'white',
          }}>
          <View style={{padding: RFValue(10), height: '53.492%'}}>
            <ScrollView>
              {/* <Cart addItemModal={this.addItemModalSwitch} /> */}
              {console.log(
                'this.props.allTransactionMenu.foodMenuData = ',
                this.props.allTransactionMenu.foodMenuData,
              )}
              {this.props.allTransactionMenu.foodMenuData &&
                this.props.allTransactionMenu.foodMenuData.map(
                  (allTransactionMenus, index) => (
                    <InvoiceList
                      quantity={allTransactionMenus.quantity_food}
                      title={allTransactionMenus.food_name}
                      amount={allTransactionMenus.price_food}
                      key={index}
                      deleteMenuModalSwitch={this.deleteMenuModalSwitch}
                    />
                  ),
                )}

              {/* <InvoiceList title={this.props.foodItem} amount={this.props.priceItem} /> */}
            </ScrollView>
          </View>
          <View
            style={{
              backgroundColor: 'white',
              justifyContent: 'space-between',
              height: '25.854%',
              paddingHorizontal: 15,
              paddingBottom: 5,
            }}>
            {/* <InvoiceList title={this.props.foodItem} amount={this.props.priceItem} /> */}
            {console.log(
              'this.props.subTotal + this.props.subTotalModifier = ',
              this.props.subTotal,
              this.props.subTotalModifier,
            )}
            <InvoiceListTotal
              title="Sub Total"
              amount={this.props.subTotal + this.props.subTotalModifier}
            />
            <InvoiceListTotal title="Tax / Service" amount={0} />
            {/* <InvoiceListTotal title="Discount" amount={this.props.amountDiscount} /> */}
            <InvoiceListTotal
              title="Discount"
              amount={this.props.all_data_discount.price_discount}
            />
            <InvoiceBill
              title="Total"
              amount={this.props.subTotal + this.props.subTotalModifier}
            />
          </View>

          <View
            style={{
              marginTop: '2.377%',
              backgroundColor: 'white',
              paddingHorizontal: 15,
              alignContent: 'center',
              height: '14.562%',
              marginBottom: '3.715%',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                height: '100%',
              }}>
              <TouchableOpacity
                style={{width: '39%'}}
                onPress={this.props.createRunningOrder}>
                <View
                  style={{
                    flexDirection: 'row',
                    flex: 1,
                    backgroundColor: '#6F6F6F',
                    borderRadius: RFValue(5),
                    marginRight: 2,
                    marginBottom: 8,
                    shadowColor: '#DF2428',
                    shadowOffset: {width: 0, height: 2},
                    shadowOpacity: 100,
                    shadowRadius: 2.62,
                    elevation: 3,
                  }}>
                  <View
                    style={{
                      backgroundColor: '#6F6F6F',
                      paddingHorizontal: 10,
                      paddingVertical: 2,
                      justifyContent: 'center',
                      marginBottom: 6,
                      marginRight: 6,
                      borderRadius: 10,
                      flex: 1,
                    }}>
                    <Text
                      style={{
                        fontSize: RFValue(15),
                        textAlign: 'center',
                        color: '#FFFFFF',
                      }}>
                      Order
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{width: '59%'}}
                onPress={() => this.props.navigation.navigate('Payment')}>
                <View
                  style={{
                    flexDirection: 'row',
                    flex: 1,
                    backgroundColor: '#FEBF11',
                    borderRadius: RFValue(5),
                    marginRight: 2,
                    marginBottom: 8,
                    shadowColor: '#DF2428',
                    shadowOffset: {width: 0, height: 2},
                    shadowOpacity: 100,
                    shadowRadius: 2.62,
                    elevation: 3,
                  }}>
                  <View
                    style={{
                      backgroundColor: '#FEBF11',
                      paddingHorizontal: 10,
                      paddingVertical: 2,
                      justifyContent: 'center',
                      marginBottom: 6,
                      marginRight: 6,
                      borderRadius: 10,
                      flex: 1,
                    }}>
                    <Text
                      style={{
                        fontSize: RFValue(15),
                        textAlign: 'center',
                        color: '#FFFFFF',
                      }}>
                      Pay
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <DeleteMenuModal
          visible={this.state.deleteModalVisible}
          deleteMenuModalSwitch={this.deleteMenuModalSwitch}
          toggleDeleteMenu={this.toggleDeleteMenu}
          closeAndDeleteMenu={this.closeAndDeleteMenu}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    testRedux: state.rightFoodTransaction.testRedux,
    all_data_discount: state.rightFoodTransaction.all_data_discount,
    userToken: state.rightFoodTransaction.tokenUser,
    all_data_id_order_printing:
      state.rightFoodTransaction.all_data_id_order_printing,
  };
};

export default connect(mapStateToProps)(RightTransaction);
