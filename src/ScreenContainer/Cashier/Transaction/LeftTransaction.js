/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {connect} from 'react-redux';
//import {getAllFoodMenu} from '../../../redux/actions/transaction/foodAllMenu';
import MenuItem from './MenuItem';
import MenuItemFilter from './MenuItemFilter';
import MenuItemSearch from './MenuItemSearch';
import AddItemModal from '../Transaction/AddItemModal/';
import FoodMenuService from '../../../services/foodMenu.service';
import setActiveFoodMenu from '../../View/Component/Cart/index';
import {Link} from 'react-router-dom';

class LeftTransaction extends Component {
  constructor(props) {
    super(props);
    this.retrieveFoodMenus = this.retrieveFoodMenus.bind(this);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);

    this.state = {
      visible: false,
      foodMenus: [],
      foodMenuSearch: [],
      currentFoodMenu: null,
      currentIndex: -1,
      searchFoodMenu: '',
    };
  }

  async componentDidMount() {
    this.retrieveFoodMenus();
  }

  // Search Menu Function
  onChangeSearchTitle(text) {
    const searchFoodMenu = text;
    // console.log("searchFoodMenu = ", searchFoodMenu);
    this.setState({
      searchFoodMenu: searchFoodMenu,
    });

    const dataSearchString = this.state.searchFoodMenu;
    // console.log("searchFoodMenu = ", dataSearchString);

    FoodMenuService.getFoodMenuSearch(dataSearchString, this.props.userToken)
      .then((response) => {
        this.setState({
          foodMenuSearch: response.data.data,
        });
        console.log('respone search menu', response.data.data);
      })
      .catch((e) => {
        console.log('Error respone search menu = ', e.response);
      });
  }

  retrieveFoodMenus() {
    FoodMenuService.getAll(this.props.userToken)
      .then((response) => {
        this.setState({
          foodMenus: response.data.data,
        });
        console.log('response get all food menu data =  ', response.data.data);
      })
      .catch((e) => {
        console.log('Error get all food menu data = ', e);
      });
  }

  render() {
    const {foodMenus, currentIndex, foodMenuSearch} = this.state;
    return (
      <View style={{flex: 1, backgroundColor: 'white', flexDirection: 'row'}}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#F7F7F7',
            paddingHorizontal: '3.9%',
          }}>
          <View
            style={{
              flexDirection: 'row',
              paddingBottom: 5,
              marginTop: 19,
              height: RFValue(40),
            }}>
            <View style={{width: '63.2%', marginRight: '3.2%'}}>
              <View
                style={{
                  backgroundColor: 'white',
                  flex: 1,
                  justifyContent: 'center',
                  borderRadius: 5,
                  shadowOpacity: 0.5,
                  shadowRadius: 5,
                  elevation: 3,
                }}>
                <TextInput
                  placeholder="Search..."
                  defaultValue={this.state.searchFoodMenu}
                  style={{fontSize: RFValue(10), justifyContent: 'center'}}
                  inputContainerStyle={{height: 30}}
                  onChangeText={this.onChangeSearchTitle}
                />
              </View>
            </View>
            <View
              style={{
                width: '33.5%',
                height: '100%',
                justifyContent: 'center',
                alignContent: 'center',
              }}>
              <TouchableOpacity
                style={{
                  height: '100%',
                  width: '100%',
                  backgroundColor: '#FEBF11',
                  shadowColor: '#000',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  borderRadius: RFValue(3),
                  elevation: 3,
                }}
                onPress={this.props.categoryModalSwitch}>
                <Text
                  style={{
                    alignSelf: 'center',
                    fontSize: RFValue(12),
                    color: '#FFF',
                  }}>
                  Category
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <Text
            style={{
              fontSize: RFValue(18),
              color: '#4A4A4A',
              marginVertical: RFValue(1),
            }}>
            Food Menu
          </Text>

          <ScrollView>
            <View
              style={{
                width: '100%',
                marginVertical: '2%',
                flexDirection: 'row',
                flexWrap: 'wrap',
              }}>
              {foodMenuSearch.map((foodMenuSearching, index) => (
                <MenuItemSearch
                  className={
                    'list-group-item' + (index === currentIndex ? 'active' : '')
                  }
                  pressAction={this.props.addItemModalSwitch}
                  foodName={foodMenuSearching.name}
                  price={foodMenuSearching.sale_price}
                  photo={foodMenuSearching.photo}
                  uuid={foodMenuSearching.UUID}
                  onClick={() =>
                    this.props.setActiveFoodMenu(foodMenuSearching, index)
                  }
                  key={index}
                />
              ))}
              {console.log(
                'list map this.props.foodMenusFilter = ',
                this.props.foodMenusFilter,
              )}
              {this.props.foodMenusFilter.map((foodMenuFilter, index) => (
                <MenuItemFilter
                  className={
                    'list-group-item' + (index === currentIndex ? 'active' : '')
                  }
                  pressAction={this.props.addItemModalSwitch}
                  foodName={foodMenuFilter.name}
                  price={foodMenuFilter.sale_price}
                  photo={foodMenuFilter.photo}
                  uuid={foodMenuFilter.UUID}
                  onClick={() =>
                    this.props.setActiveFoodMenu(foodMenuFilter, index)
                  }
                  key={index}
                />
              ))}
              {console.log('MenuItem = ', foodMenus)}
              {foodMenus.map((foodMenu, index) => (
                <MenuItem
                  className={
                    'list-group-item' + (index === currentIndex ? 'active' : '')
                  }
                  pressAction={this.props.addItemModalSwitch}
                  foodName={foodMenu.name}
                  price={foodMenu.sale_price}
                  photo={foodMenu.photo}
                  uuid={foodMenu.UUID}
                  foodMenuId={foodMenu.food_category.id}
                  stockFood={foodMenu.stock}
                  onClick={() => this.props.setActiveFoodMenu(foodMenu, index)}
                  key={index}
                />
              ))}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userToken: state.rightFoodTransaction.tokenUser,
    running_id: state.rightFoodTransaction.runningId,
    all_data_id_category: state.rightFoodTransaction.all_data_id_category,
  };
};

export default connect(mapStateToProps)(LeftTransaction);
