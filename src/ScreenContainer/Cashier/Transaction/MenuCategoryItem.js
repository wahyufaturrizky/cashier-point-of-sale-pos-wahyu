/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import NumberFormat from 'react-number-format';

export default class MenuCategoryItem extends Component {
  render() {
    return (
      <View
        style={{
          width: '33.33%',
          height: undefined,
          aspectRatio: 1.1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          style={{
            height: '70%',
            width: '70%',
            // aspectRatio: 1,
            borderRadius: RFValue(10),
            justifyContent: 'center',
            alignItems: 'center',
            shadowOpacity: 1,
            // elevation: 3,
            backgroundColor: '#FE724C',
          }}
          // onPress={(event) =>
          //   {console.log("Check UUID Per Category Menu =", this.props.uuidCategoryMenu)
          // }}
          onPress={() =>
            this.props.pressActionCategory(this.props.uuidCategoryMenu)
          }>
          {/* <Image 
            source={require('../../../assets/General/mealIcon.png')} resizeMode="contain" 
            style={{
              width: '50%',
              height: undefined,
              aspectRatio: 1,
              marginBottom: '10%',
            }} /> */}
          <Text
            style={{
              fontSize: RFValue(15),
              textAlign: 'center',
              color: 'white',
            }}>
            {this.props.foodCategory}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
